import base64, redis, arrow, datetime, json
from time import sleep

from Crypto.Cipher import AES
from Crypto import Random
from Crypto.Util import Counter

# locals
import config as Config

rc = redis.StrictRedis(
    host=Config.DefaultConfig.CACHE_HOST,
    port=Config.DefaultConfig.CACHE_PORT,
    db=Config.DefaultConfig.CACHE_DB,
    charset="utf-8",
    decode_responses=True,
)


class ServiceStatus(object):
    def __init__(self, name, checker, service_id, status, updated, message, version):
        self.name = name
        self.checker = checker
        self.service_id = service_id
        self.status = status
        self.message = message
        self.updated = updated
        self.version = version

    def to_json(self):
        return json.dumps(self, default=lambda o: o.__dict__)


def set_service_status(service_id=None, about=None):
    dts = arrow.get(datetime.datetime.now()).format("YYYY-MM-DD HH:mm:ss")
    ss = ServiceStatus(
        "deployer",
        "deployer",
        service_id,
        "success",
        dts,
        "ping",
        "{0} ({1})".format(about["version"], about["modified"]),
    )
    rc.set("service_status:{0}:{1}".format(ss.name, ss.service_id), ss.to_json())
    rc.expire(
        "service_status:{0}:{1}".format(ss.name, ss.service_id),
        Config.DefaultConfig.CACHE_DEPLOYER_SERVICE_STATS_TTL,
    )


# takes: plaintext
# returns: cipher (encrypted message)
def pyco_encrypt(message):
    obj = AES.new(
        Config.DefaultConfig.CRYPTO_KBT_KEY,
        AES.MODE_CFB,
        Config.DefaultConfig.CRYPTO_KBT_IV,
    )
    return base64.urlsafe_b64encode(obj.encrypt(message)).decode()


# takes: cipher (encrypted message)
# returns: plaintext (byte decoded)
def pyco_decrypt(cipher):
    obj2 = AES.new(
        Config.DefaultConfig.CRYPTO_KBT_KEY,
        AES.MODE_CFB,
        Config.DefaultConfig.CRYPTO_KBT_IV,
    )
    return obj2.decrypt(base64.urlsafe_b64decode(cipher)).decode()
    # return "Not Available"


def ping_service_status(service_id, about):
    # logger.info('NOTICE: ping service status background thread started...')
    while True:
        set_service_status(service_id=service_id, about=about)
        # note: this loop sleeps 5 seconds less than the TTL to ensure the TTL is reset before it expires.
        sleep(int(Config.DefaultConfig.CACHE_DEPLOYER_SERVICE_STATS_TTL) - 5)


# get deployment statuses (get json and return dict)
def get_deployment_statuses(db=None):
    return json.loads(
        json.dumps(
            [dict(r) for r in db.engine.execute("select * from deployment_status")]
        )
    )
