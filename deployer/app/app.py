import os, stat, sys, shutil, subprocess, arrow, datetime, logging, graypy, json, redis, uuid, pgpubsub
from collections import namedtuple

from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from sqlalchemy.ext.declarative import declarative_base

from subprocess import Popen, PIPE

from slackclient import SlackClient
from time import sleep
from threading import Thread

import kubernetes.client

# locals
from utils import *
from models import *
import config as Config

rundts = datetime.datetime.now()
did = None
queue_item = None
result = None

about = {
    "name": "deployer",
    "version": "1.3.0",
    "modified": "2018-08-14",
    "created": "2018-06-18",
}
service_id = str(uuid.uuid4())[:8]
deployment_statuses = None


class didFilter(logging.Filter):
    # on each log msg.
    def filter(self, record):
        global result
        record.did = did
        return True


# ~ db = create_engine(Config.DefaultConfig.SQLALCHEMY_DATABASE_URI, echo=True)
db = create_engine(Config.DefaultConfig.SQLALCHEMY_DATABASE_URI)
Session = sessionmaker(db)
session = Session()
base.metadata.create_all(db)
pubsub = pgpubsub.connect(
    host=Config.DefaultConfig.DB_SERVICE,
    database=Config.DefaultConfig.DB_NAME,
    user=Config.DefaultConfig.DB_USER,
    password=Config.DefaultConfig.DB_PASS,
)

logger_base = logging.getLogger("main_logger")
logger_base.setLevel(logging.getLevelName(Config.DefaultConfig.APP_LOGLEVEL))
graylog_handler = graypy.GELFHandler(
    host=Config.DefaultConfig.GRAYLOG_HOST, port=Config.DefaultConfig.GRAYLOG_PORT
)
console_handler = logging.StreamHandler()
if "graylog" in Config.DefaultConfig.LOG_TO:
    logger_base.addHandler(graylog_handler)
    logger_base.addFilter(didFilter())
if "console" in Config.DefaultConfig.LOG_TO:
    logger_base.addHandler(console_handler)
logger = logging.LoggerAdapter(
    logging.getLogger("main_logger"),
    {
        "application_name": Config.DefaultConfig.GRAYLOG_APPNAME,
        "application_env": Config.DefaultConfig.GRAYLOG_APPENV,
    },
)

logger.info(
    "{0} - v.{1} ({2}) - {3}".format(
        about["name"], about["version"], about["modified"], service_id
    )
)
logger.info(
    "logs are being sent to: {0}".format(
        ", ".join(str(i) for i in Config.DefaultConfig.LOG_TO)
    )
)


# notify via slack.
def notify_via_slack(subject, message):
    slack_token = Config.DefaultConfig.slack["api-token"]
    try:
        sc = SlackClient(slack_token)
        resp = sc.api_call(
            "chat.postMessage",
            channel=Config.DefaultConfig.slack["channel"],
            text=subject,
            icon_emoji=":eyes",
            as_user="false",
            attachments=[{"text": message, "mrkdwn_in": ["text"]}],
            username="buildbot",
        )
    except Exception as e:
        logger.error(
            "ERROR: Issue sending via slack, is your api key set/valid?\nDetails: {0}".format(
                e
            )
        )
        sys.exit(500)


# update item status by id
#  note: if no row is updated (such would be the case if two deployers are running and one picks the even up
#        before the other) this function returns False, otherwise it returns true.
def update_request_item_status(id=None, status_id=None):
    logger.debug("update request item status")
    # ~ sql = "update deployment set deployment_status_id=(select id from deployment_status where name='{0}') where id={1}".format(id,status)
    sql = "update deployment set deployment_status_id={2} where id={0} AND deployment_status_id={1}".format(
        id, ds_request["id"], status_id
    )
    logger.debug("executing sql of: {0}".format(sql))
    result = db.engine.execute(sql)
    # logger.debug('update rowcount: {0}'.format(result.rowcount))
    return True if result.rowcount > 0 else False


def update_item_status(id=None, status_id=None, status_message=None):
    sql = None
    if status_message and len(status_message) > 0:
        # ~ sql = "update deployment set deployment_status_id=(select id from deployment_status where name='{0}'), deployment_status_message='{2}' where id={1}".format(id, status, status_message)
        sql = "update deployment set deployment_status_id={1}, deployment_status_message='{2}' where id={0}".format(
            id, status_id, status_message
        )
    else:
        # ~ sql = "update deployment set deployment_status_id=(select id from deployment_status where name='{0}') where id={1}".format(id,status)
        sql = "update deployment set deployment_status_id={2} where id={0}".format(
            id, status_id
        )
    logger.debug("executing sql of: {0}".format(sql))
    result = db.engine.execute(sql)
    # logger.debug('update rowcount: {0}'.format(result.rowcount))
    return True if result.rowcount > 0 else False


def get_deploy_start_message(build_request_payload, repo_branch):
    return """
• App: {0} / {1}
• Build: {2} (clean: {3})
• Push: {4} (latest: {5})
• VC Tag: {6} (tag: {7})
""".format(
        build_request_payload["APP_NAME"],
        repo_branch,
        build_request_payload["RUN_BUILD"],
        build_request_payload["CLEAN_SOURCE"],
        build_request_payload["PUSH_IMAGE_ENABLED"],
        build_request_payload["TAG_IMAGE_LATEST"],
        build_request_payload["VC_TAG_ENABLED"],
        build_request_payload["VC_TAG"]
        if "VC_TAG" in build_request_payload
        else "not specified",
    )


def get_deploy_complete_message(result):
    return """
• App: {0} / {1}
• Result: {3}
• Img: {10} (size: {11})
• Build: {2} (in {12}s)
• Push: {4} (latest: {5})
• Tag: {6} (latest: {7})
• VC Tag: {8} (tag: {9})
  """.format(
        result["component"],
        result["vc_ref"],
        result["build_success"],
        result["build_result_msg"],
        result["image_push_performed"],
        result["image_push_latest_performed"],
        result["image_tag_performed"],
        result["image_tag_latest_performed"],
        result["vc_tag_performed"],
        result["image_set_id"],
        result["image_url"],
        result["image_size"],
        result["build_time"],
    )


def deploy_item(item=None):
    logger.debug("request created by user id: {0}".format(item.created_by))
    user = session.query(User).filter(User.id == item.created_by).first()
    logger.debug(" - associated user account: {0}".format(user))
    kbt = pyco_decrypt(user.kube_bearer_token)
    configuration = kubernetes.client.Configuration()
    configuration.host = Config.DefaultConfig.KUBE_CLUSTER_URL
    configuration.verify_ssl = False
    configuration.api_key = {"authorization": "Bearer " + kbt}
    logger.debug("Kube Configuration:\n{0}".format(configuration.__dict__))
    patch = [
        {
            "op": "replace",
            "path": "/spec/template/spec/containers/0/image",
            "value": item.image_url,
        }
    ]
    logger.info(
        "Patching deployment:\n  - namespace: {0}\n  - application: {1}\n  - patch: {2}".format(
            item.namespace, item.app_name, patch
        )
    )
    api_instance = kubernetes.client.AppsV1beta1Api(
        kubernetes.client.ApiClient(configuration)
    )
    try:
        api_instance.patch_namespaced_deployment(
            name=item.app_name, namespace=item.namespace, body=patch
        )
        logger.debug("patch namespaced deployment response: {0}".format(api_response))
        # NOTICE: see comment below about watch being disabled.
        # @TODO we need to fix the watch or make a workaround as this logic is needed.
        update_item_status(
            id=item.id,
            status_id=ds_success["id"],
            status_message="patch submitted successfully (note: watch of deploy currently disabled - you must manually watch for completion)",
        )
    except ApiException as e:
        logger.error("APIException: {0}".format(e))
        error_code = str(e)[1:4]
        if error_code == "403":
            logger.error("403 Error Deploying Build")
            update_item_status(
                item=item,
                status="failure",
                status_message="Unauthorized: you are not authorized to deploy '{0}' to '{1}'".format(
                    item.app_name, item.namespace
                ),
            )
        elif error_code == "404":
            logger.error("404 Error Deploying Build")
            update_item_status(
                item=item,
                status="failure",
                status_message="Not Found: it appears '{0}' is not setup in the '{1}' namespace, please notify devops to have the app setup manually".format(
                    item.app_name, item.namespace
                ),
            )
        else:
            logger.error("Other Error Deploying Build:\n{0}".format(e))
            update_item_status(
                item=item,
                status="failure",
                status_message="Other Error Deploying Build:\n{0}".format(e),
            )
    except Exception as e:
        logger.error("Other Error Occurred: {0}".format(e))
        update_item_status(
            item=item,
            status="failure",
            status_message="Other Error Occurred:\n{0}".format(e),
        )
    logger.debug("patch applied")
    #
    #  NOTICE: as of 2018-06-27 the watch is disabled as it throws an error:
    #   "Response is not chunked. Header 'transfer-encoding: chunked' is missing."
    #
    # ~ logger.debug("patch applied, watching deployment for status")
    # ~ v1a = client.AppsV1Api()
    # ~ w = watch.Watch()
    # ~ for event in w.stream(
    # ~ v1a.list_namespaced_deployment, item.namespace, timeout_seconds=28, watch=True
    # ~ ):
    # ~ logger.debug(
    # ~ "Deployment: {0}\n  - replicas: {1} (ready: {2})\n  - unavailable: {3}\n  - updated: {4}".format(
    # ~ event["object"].metadata.name,
    # ~ event["object"].status.replicas,
    # ~ event["object"].status.ready_replicas,
    # ~ event["object"].status.unavailable_replicas,
    # ~ event["object"].status.updated_replicas,
    # ~ )
    # ~ )
    # set deployment status to 'failure' now, if success it will be updated via the watch.
    # ~ update_item_status(
    # ~ id=item.id,
    # ~ status_id=ds_failure["id"],
    # ~ status_message="Timeout: deployment timed out, check logs for more info.",
    # ~ )
    # stop if deploy all done.
    #   note: unavailable will be None when all are available
    # ~ if not event["object"].status.unavailable_replicas:
    # ~ logger.debug("All replicas are available, stopping watch...")
    # ~ w.stop()
    # ~ update_item_status(
    # ~ id=item.id,
    # ~ status_id=ds_success["id"],
    # ~ status_message="deployed, available instances: {0}".format(
    # ~ event["object"].status.ready_replicas
    # ~ ),
    # ~ )


def process_item(item=None):
    global did
    did = item.id
    deploy_item(item=item)


def long_poll():
    logger.info(
        "starting long poll service (quietly), run interval: {0}s".format(
            Config.DefaultConfig.DEPLOYER_CHECK_SLEEP_AMOUNT
        )
    )
    while True:
        cursor = (
            session.query(Deployment)
            .join(Deployment.deployment_status)
            .filter(
                Deployment.deployment_status.property.mapper.class_.name == "request"
            )
            .order_by(Deployment.id)
        )
        result = cursor.first()
        if cursor.count() > 0:
            while result is not None:
                logger.info(
                    "NOTICE: long poll process found a record to process, record id: {0}".format(
                        result.id
                    )
                )
                # mark item as being processed
                if update_request_item_status(
                    id=result.id, status_id=ds_deploying["id"]
                ):
                    process_item(item=result)
                    # check for more items to process.
                    result = cursor.first()
                else:
                    logger.debug("another process is processing this item, skipping")
        sleep(Config.DefaultConfig.DEPLOYER_CHECK_SLEEP_AMOUNT)


# start a background thread that keeps updating the service status.
#   note: thread runs indefinitely.
pss_thread = Thread(target=ping_service_status, args=(service_id, about))
pss_thread.start()
lp_thread = Thread(target=long_poll, args=())
lp_thread.start()

deployment_statuses = get_deployment_statuses(db=db)
ds_request = [x for x in deployment_statuses if x["name"] == "request"][0]
ds_deploying = [x for x in deployment_statuses if x["name"] == "deploying"][0]
ds_failure = [x for x in deployment_statuses if x["name"] == "failure"][0]
ds_success = [x for x in deployment_statuses if x["name"] == "success"][0]

pubsub.listen(Config.DefaultConfig.NOTIFY_SERVICE_CHANNEL)
logger.info(
    "listening for deployment requests on channel: {0}".format(
        Config.DefaultConfig.NOTIFY_SERVICE_CHANNEL
    )
)
for e in pubsub.events():
    # NOTICE: we have the id already so no need to select anything; HOWEVER, we do want to update
    #   the record on id and status, this way if another deployer has picked it up before this one does
    #   it will get no records found on the update.
    logger.debug("received notify of deployment request: {0}".format(e.payload))
    event_data = json.loads(e.payload)
    result = namedtuple("Deployment", event_data.keys())(**event_data)
    # check to ensure item is a 'request', if so process it.
    if result.deployment_status_id == ds_request["id"]:
        # update record to reflect it is being deployed.
        # if set item status to 'deploying' fails it means something else has grabbed the record to process it.
        if update_request_item_status(id=result.id, status_id=ds_deploying["id"]):
            # deploy the request (this also sets the record deployment status accordingly)
            process_item(item=result)
            logger.debug("deployment request processing complete")
        else:
            logger.debug("another process is processing this item, skipping")
    else:
        logger.debug("no rds to ds_request match, pass on processing...")
