#
# NOTICE: models are maintained via Flask-Migrate in the wui app.
#

from sqlalchemy import (
    Column,
    String,
    Integer,
    Boolean,
    Text,
    ForeignKey,
    JSON,
    TIMESTAMP,
    DateTime,
)
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship

import datetime

base = declarative_base()

# ~ class BuildTemplate(base):
# ~ __tablename__ = 'build_template'
# ~ id = Column(Integer, primary_key=True)
# ~ name = Column(String(50), nullable=False, unique=True)
# ~ description = Column(String(500), nullable=False)
# ~ template = Column(Text, nullable=False)
# ~ def __str__(self):
# ~ return '{0}'.format(self.name)


# ~ class AppConfig(base):
# ~ __tablename__ = 'component_run_default'
# ~ id = Column(Integer, primary_key=True)
# ~ component = Column(String)
# ~ data = Column(JSON, default=None, nullable=True)


class User(base):
    __tablename__ = "user"
    id = Column(Integer, primary_key=True)
    name = Column(String(100), nullable=False)
    username = Column(String(33), nullable=False, unique=True)
    nickname = Column(String(5), nullable=True, unique=True)
    email = Column(String(255), nullable=False, unique=True)
    active = Column(Boolean(), default=True)
    confirmed_at = Column(DateTime())
    note = Column(Text, nullable=True)
    kube_bearer_token = Column(String(255), nullable=True)

    def __str__(self):
        return "user name={0}, username={1}, kbt (encrypted)={2}".format(
            self.name, self.username, self.kube_bearer_token
        )


class DeploymentStatus(base):
    __tablename__ = "deployment_status"
    id = Column(Integer, primary_key=True)
    name = Column(String)
    desc = Column(String)

    def __str__(self):
        return "{0}".format(self.name)


class Deployment(base):
    __tablename__ = "deployment"
    id = Column(Integer, primary_key=True)
    deployment_status_id = Column(
        Integer, ForeignKey("deployment_status.id"), nullable=False
    )
    deployment_status = relationship(
        DeploymentStatus, foreign_keys="Deployment.deployment_status_id"
    )
    deployment_status_message = Column(String(2000), nullable=True)
    namespace = Column(String(50), nullable=False)
    app_name = Column(String(50), nullable=False)
    image_url = Column(String(255), nullable=False)
    approver = Column(String(50), nullable=False)
    note = Column(Text, nullable=True)
    created_by = Column(Integer, ForeignKey("user.id"), nullable=False)
    creator = relationship("User", foreign_keys=[created_by])
    created_at = Column(TIMESTAMP, default=datetime.datetime.utcnow, nullable=False)


# ~ class QueueStatus(base):
# ~ __tablename__ = 'queue_status'
# ~ id = Column(Integer, primary_key=True)
# ~ name = Column(String)
# ~ desc = Column(String)
# ~ def __str__(self):
# ~ return '{0} ({1})'.format(self.name, self.desc)


# ~ class Queue(base):
# ~ __tablename__ = 'queue'
# ~ id = Column(Integer, primary_key=True)
# ~ queue_status_id = Column(Integer, ForeignKey("queue_status.id"), nullable=False)
# ~ data = Column(JSON, nullable=False)
# ~ result = Column(JSON, nullable=True)
# ~ created_by = Column(Integer, nullable=False)
# ~ created_at = Column(TIMESTAMP, nullable=False, default=datetime.datetime.utcnow)
# ~ queue_status = relationship(QueueStatus, foreign_keys='Queue.queue_status_id')
