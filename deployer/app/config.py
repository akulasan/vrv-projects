#!/usr/bin/env python

import os
from distutils.util import strtobool


def env_strtobool(val):
    if val in ["true", "'true'"]:
        return True
    return False


class BaseConfig(object):
    # APPLICATION
    CRYPTO_KBT_IV = b"D\xca\xc9\xa8\x1a\xcb\xdb\xf1\x89H\xf9\x85\xb6\xe4\xb0\xdd"
    CRYPTO_KBT_KEY = "abcdefghijklmnop"
    KUBE_CLUSTER_URL = os.environ["KUBE_CLUSTER_URL"]
    # amount of time to sleep between queue checks in seconds.
    #  notice: the queue processor will continue to run (with no sleeps) if there are more records in the queue.
    #          this sleep amount comes into play when all queue records are processed.
    DEPLOYER_CHECK_SLEEP_AMOUNT = int(os.environ["DEPLOYER_CHECK_SLEEP_AMOUNT"])
    NOTIFY_SERVICE_CHANNEL = os.environ["DEPLOYER_NOTIFY_SERVICE_CHANNEL"]

    # CACHE
    CACHE_HOST = os.environ["CACHE_HOST"]
    CACHE_PORT = os.environ["CACHE_PORT"]
    CACHE_DB = os.environ["CACHE_DB"]
    CACHE_DEPLOYER_SERVICE_STATS_TTL = os.environ["CACHE_DEPLOYER_SERVICE_STATS_TTL"]

    # DATABASE
    DB_NAME = os.environ["DB_NAME"]
    DB_USER = os.environ["DB_USER"]
    DB_PASS = os.environ["DB_PASS"]
    DB_SERVICE = os.environ["DB_SERVICE"]
    DB_PORT = os.environ["DB_PORT"]
    SQLALCHEMY_DATABASE_URI = "postgresql://{0}:{1}@{2}:{3}/{4}".format(
        DB_USER, DB_PASS, DB_SERVICE, DB_PORT, DB_NAME
    )
    SQLALCHEMY_TRACK_MODIFICATIONS = False

    # LOGGING
    LOG_TO = os.environ["LOG_TO"].split(",")
    APP_LOGLEVEL = os.environ["APP_LOGLEVEL"]
    GRAYLOG_HOST = os.environ["GRAYLOG_HOST"]
    GRAYLOG_PORT = int(os.environ["GRAYLOG_PORT"])
    GRAYLOG_APPNAME = "ab-deployer"
    GRAYLOG_APPENV = os.environ["DEPLOY_ENV"]

    # NOTIFICATION
    slack = {
        "channel": os.environ["SLACK_CHANNEL"],  # channel to post to
        "msg_subject": os.environ["SLACK_MSG_SUBJECT"],  # message subject
        "api-token": os.environ["SLACK_API_TOKEN"],  # api token for auth
    }


class DefaultConfig(BaseConfig):
    pass
