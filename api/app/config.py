#!/usr/bin/env python

import os
from distutils.util import strtobool


def env_strtobool(val):
    if val in ["true", "'true'"]:
        return True
    return False


class BaseConfig(object):
    DEPLOY_ENV = os.environ["DEPLOY_ENV"]

    # CACHE
    CACHE_HOST = os.environ["CACHE_HOST"]
    CACHE_PORT = os.environ["CACHE_PORT"]
    CACHE_DB = os.environ["CACHE_DB"]
    CACHE_API_SERVICE_STATS_TTL = os.environ["CACHE_API_SERVICE_STATS_TTL"]

    # database.
    DB_NAME = os.environ["DB_NAME"]
    DB_USER = os.environ["DB_USER"]
    DB_PASS = os.environ["DB_PASS"]
    DB_SERVICE = os.environ["DB_SERVICE"]
    DB_PORT = os.environ["DB_PORT"]
    SQLALCHEMY_DATABASE_URI = "postgresql://{0}:{1}@{2}:{3}/{4}".format(
        DB_USER, DB_PASS, DB_SERVICE, DB_PORT, DB_NAME
    )
    SQLALCHEMY_TRACK_MODIFICATIONS = False

    # APPLICATION
    API_BASE_URL = os.environ["API_BASE_URL"]
    # Build Deploy Queue Check Sleep Amount
    BD_QC_SLEEP_AMOUNT = int(os.environ["BD_QC_SLEEP_AMOUNT"])

    # LOGGING
    LOG_TO = os.environ["LOG_TO"].split(",")
    APP_LOGLEVEL = os.environ["APP_LOGLEVEL"]
    GRAYLOG_HOST = os.environ["GRAYLOG_HOST"]
    GRAYLOG_PORT = int(os.environ["GRAYLOG_PORT"])
    GRAYLOG_APPNAME = "ab-api"
    GRAYLOG_APPENV = os.environ["DEPLOY_ENV"]


class DefaultConfig(BaseConfig):
    pass
