import uuid, datetime, arrow
from time import sleep
from threading import Thread

from apistar import App, Route, exceptions
from apistar_jwt.token import JWT, JWTUser
from apistar_jwt.decorators import anonymous_allowed, authentication_required

from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

# locals
import config as Config
from models import *
from utils import *

about = {
    "name": "ab-api",
    "version": "1.2.0",
    "modified": "2018-08-14",
    "created": "2018-05-16",
}
service_id = str(uuid.uuid4())[:8]

logger.info(
    "{0} - v.{1} ({2}) - {3}".format(
        about["name"], about["version"], about["modified"], service_id
    )
)
logger.info(
    "logs are being sent to: {0}".format(
        ", ".join(str(i) for i in Config.DefaultConfig.LOG_TO)
    )
)

db = create_engine(Config.DefaultConfig.SQLALCHEMY_DATABASE_URI)
Session = sessionmaker(db)
session = Session()


@authentication_required
def welcome(user: JWTUser) -> dict:
    tok_exp = arrow.get(user.token["exp"]).format("YYYY-MM-DD HH:mm:ss")
    message = "Welcome {0} ({1}), your login expires at {2}".format(
        user.username, user.id, tok_exp
    )
    return {"message": message}


def login(data: UserData, jwt: JWT) -> dict:
    cursor = session.query(ApiAccount).filter(ApiAccount.username == data.username)
    user = cursor.first()
    if not user or not user.verify_password(data.password):
        raise exceptions.Forbidden("Incorrect username or password.")
    iat = datetime.datetime.utcnow()
    exp = datetime.datetime.utcnow() + datetime.timedelta(minutes=60)
    payload = {
        "id": user.id,
        "username": user.username,
        "associated_user_id": user.associated_user_id,
        "iat": iat,
        "exp": exp,  # ends in 60 minutes
    }
    token = jwt.encode(payload)
    if token is None:
        # encoding failed, handle error
        raise exceptions.BadRequest()
    return {"token": token}


def index() -> dict:
    return {
        "message": "{0} (v.{1})".format(about["name"], about["version"]),
        "tip": "see /docs/ for api documentation",
    }


@authentication_required
def submit_build(data: BuildRequest, user: JWTUser) -> dict:
    payload = {
        "APP_NAME": data.app_name,
        "REPO_BRANCH": data.src_branch,
        "TAG_IMAGE_LATEST": data.tag_image_latest,
        "PUSH_IMAGE_ENABLED": data.push_image_enabled,
        "VC_TAG_ENABLED": data.vc_tag_enabled,
    }
    if "src_branch" in data and len(data.src_branch) > 0:
        payload["RUN_BUILD"] = data.src_branch
    if "src_repo" in data and len(data.src_repo) > 0:
        payload["SRC_REPO"] = data.src_repo
    if "vc_tag" in data and len(data.vc_tag) > 0:
        payload["VC_TAG"] = data.vc_tag
    logger.debug("creating build request as follows: {0}".format(payload))
    q = Queue()
    q.queue_status_id = session.query(QueueStatus).filter_by(name="request").one().id
    q.data = payload
    q.created_by = user.token["associated_user_id"]
    q.note = data.note if "note" in data and len(data.note) > 0 else "created via api"
    q.requester = data.requester
    session.add(q)
    session.commit()
    session.refresh(q)
    return {"message": "build request submitted", "qid": q.id}


@authentication_required
def submit_deploy(data: DeployRequest, user: JWTUser) -> dict:
    d = Deployment()
    d.deployment_status_id = (
        session.query(DeploymentStatus).filter_by(name="request").one().id
    )
    d.image_url = data.image_url
    d.note = data.note
    d.created_by = user.token["associated_user_id"]
    d.app_name = data.app_name
    d.namespace = data.namespace
    d.approver = data.approver
    session.add(d)
    session.commit()
    session.refresh(d)
    return {"message": "deploy request submitted", "did": d.id}


def build_poll(qid, data, user):
    # @TODO we should probably update service_status to show service is 'waiting' for build to finish...
    item = None
    # loop until build finishes
    while not item:
        item = (
            session.query(Queue)
            .join(Queue.queue_status)
            .filter(Queue.id == qid)
            .filter(Queue.queue_status.property.mapper.class_.name == "success")
            .first()
        )
        logger.debug(
            "- build not yet finished, sleeping for {0}s...".format(
                Config.DefaultConfig.BD_QC_SLEEP_AMOUNT
            )
        )
        sleep(Config.DefaultConfig.BD_QC_SLEEP_AMOUNT)
    # build is finished, submit deploy request
    payload = {
        "app_name": data.app_name,
        "namespace": data.namespace,
        "image_url": item.result["image_url"],
        "approver": "{0} (requester)".format(data.requester),
    }
    if "note" in data and len(data.note) > 0:
        payload["note"] = data.note
    else:
        payload["note"] = "created via api"
    dr = DeployRequest(payload)
    submit_deploy(dr, user)


@authentication_required
def submit_build_and_deploy(data: ExtendedBuildRequest, user: JWTUser) -> dict:
    rsp = submit_build(data, user)
    qid = rsp["qid"]
    # start a new thread a poll for build complete there so we can return qid to requester.
    thread = Thread(target=build_poll, args=(qid, data, user))
    thread.start()
    return {"message": "build and deploy request submitted", "qid": qid}


routes = [
    Route(
        "{0}/".format(Config.DefaultConfig.API_BASE_URL), method="GET", handler=welcome
    ),
    Route(
        "{0}/login".format(Config.DefaultConfig.API_BASE_URL),
        method="POST",
        handler=login,
    ),
    Route(
        "{0}/index".format(Config.DefaultConfig.API_BASE_URL),
        method="GET",
        handler=index,
    ),
    Route(
        "{0}/build/submit".format(Config.DefaultConfig.API_BASE_URL),
        method="POST",
        handler=submit_build,
    ),
    Route(
        "{0}/deploy/submit".format(Config.DefaultConfig.API_BASE_URL),
        method="POST",
        handler=submit_deploy,
    ),
    Route(
        "{0}/build-and-deploy/submit".format(Config.DefaultConfig.API_BASE_URL),
        method="POST",
        handler=submit_build_and_deploy,
    ),
]

components = [
    JWT({"JWT_SECRET": "BZz4bHXYQD?g9YN2UksRn7*r3P(eo]P,Rt8NCWKs6VP34qmTL#8f&ruD^TtG"})
]

# start a background thread that keeps updating the service status.
#   note: thread runs indefinitely.
thread = Thread(target=ping_service_status, args=(service_id, about))
thread.start()

app = App(routes=routes, components=components)

# ~ if __name__ == '__main__':
# ~ app.serve('0.0.0.0', 5000, debug=True, use_debugger=True, use_reloader=True)
