import datetime, arrow, json
from apistar import types, validators
from passlib.apps import custom_app_context as pwd_context
from sqlalchemy import (
    Column,
    String,
    Integer,
    Boolean,
    Text,
    ForeignKey,
    JSON,
    TIMESTAMP,
    DateTime,
)
from sqlalchemy.orm import relationship
from sqlalchemy.ext.declarative import declarative_base

###
### Database Models
###

base = declarative_base()


class User(base):
    __tablename__ = "user"
    id = Column(Integer, primary_key=True)
    name = Column(String(100), nullable=False)
    username = Column(String(33), nullable=False, unique=True)
    nickname = Column(String(5), nullable=True, unique=True)
    email = Column(String(255), nullable=False, unique=True)
    active = Column(Boolean(), default=True)
    confirmed_at = Column(DateTime())
    note = Column(Text, nullable=True)

    def __str__(self):
        return "%s" % (self.username)


class ApiAccount(base):
    __tablename__ = "api_account"
    id = Column(Integer, primary_key=True)
    username = Column(String(33), nullable=False, unique=True)
    password = Column(String(255), nullable=False)
    active = Column(Boolean(), default=True, nullable=False)
    note = Column(Text, nullable=True)
    associated_user_id = Column(Integer, ForeignKey("user.id"), nullable=False)
    associated_user = relationship(User, foreign_keys=[associated_user_id])

    def hash_password(password):
        return pwd_context.encrypt(password)

    def verify_password(self, password):
        return pwd_context.verify(password, self.password)


class DeploymentStatus(base):
    __tablename__ = "deployment_status"
    id = Column(Integer, primary_key=True)
    name = Column(String)
    desc = Column(String)

    def __str__(self):
        return "{0}".format(self.name)


class Deployment(base):
    __tablename__ = "deployment"
    id = Column(Integer, primary_key=True)
    deployment_status_id = Column(
        Integer, ForeignKey("deployment_status.id"), nullable=False
    )
    deployment_status = relationship(
        DeploymentStatus, foreign_keys="Deployment.deployment_status_id"
    )
    deployment_status_message = Column(String(2000), nullable=True)
    namespace = Column(String(50), nullable=False)
    app_name = Column(String(50), nullable=False)
    image_url = Column(String(255), nullable=False)
    approver = Column(String(50), nullable=False)
    note = Column(Text, nullable=True)
    created_by = Column(Integer, ForeignKey("user.id"), nullable=False)
    creator = relationship("User", foreign_keys=[created_by])
    created_at = Column(TIMESTAMP, default=datetime.datetime.utcnow, nullable=False)


class QueueStatus(base):
    __tablename__ = "queue_status"
    id = Column(Integer, primary_key=True)
    name = Column(String)
    desc = Column(String)

    def __str__(self):
        return "{0} ({1})".format(self.name, self.desc)


class Queue(base):
    __tablename__ = "queue"
    id = Column(Integer, primary_key=True)
    queue_status_id = Column(Integer, ForeignKey("queue_status.id"), nullable=False)
    queue_status = relationship(QueueStatus, foreign_keys="Queue.queue_status_id")
    data = Column(JSON, nullable=False)
    result = Column(JSON, nullable=True)
    created_by = Column(Integer, nullable=False)
    created_at = Column(TIMESTAMP, nullable=False, default=datetime.datetime.utcnow)
    note = Column(Text, nullable=True)
    requester = Column(String(50), nullable=False)


###
### API Models
###


class BuildRequest(types.Type):
    app_name = validators.String(
        max_length=100,
        min_length=3,
        title="App",
        description="name of app to build (must be a valid app)",
    )
    # build_dir = validators.String(max_length=100, min_length=3, title='Build Dir', description='build dir')
    # clean_source = validators.Boolean(default=False, title='Clean Source', description='clean build source before build')
    # image_repo = validators.String(max_length=100, min_length=3, title='Image Repo', description='repo to push build image to')
    # prj_name = validators.String(max_length=100, min_length=3, title='Project', description='project name the the application belongs to')
    push_image_enabled = validators.Boolean(
        default=True,
        title="Push Image Enabled",
        description="push the built image to image repository",
    )
    src_branch = validators.String(
        max_length=100,
        min_length=3,
        title="Repo Branch",
        description="source branch to use for build",
    )
    run_build = validators.Boolean(
        default=True, title="Run Build", description="perform the build"
    )
    # src_name = validators.String(max_length=100, min_length=3, title='Source', description='app source')
    src_repo = validators.String(
        max_length=500,
        default="",
        allow_null=True,
        min_length=0,
        title="Source Repo",
        description="repo to pull source code from",
    )
    tag_image_latest = validators.Boolean(
        default=False,
        title="Tag Image Latest",
        description="tag the built image with the 'latest' tag",
    )
    vc_tag = validators.String(
        max_length=100,
        default="",
        allow_null=True,
        title="Source Tag",
        description="tag to apply to source (leave do not supply/leave emtpy to use autogenerated (RUNSTAMP) value)",
    )
    vc_tag_enabled = validators.Boolean(
        default=True, title="Tag Source", description="tag source repo with vc_tag"
    )
    requester = validators.String(
        max_length=50,
        min_length=1,
        title="Requester",
        description="the person/entity requesting the build",
    )
    note = validators.String(
        max_length=2000,
        default="",
        allow_null=True,
        title="Note",
        description="a note about the build (v1.0.0 build, build auth changes, etc.)",
    )


# this class is used by the submit_build_and_deploy.
class ExtendedBuildRequest(BuildRequest):
    namespace = validators.String(
        max_length=100,
        min_length=3,
        title="Namespace",
        description="namespace to deploy to",
    )


class DeployRequest(types.Type):
    app_name = validators.String(
        max_length=100,
        min_length=3,
        title="App",
        description="name of app to deploy (must be a valid app)",
    )
    namespace = validators.String(
        max_length=100,
        min_length=3,
        title="Namespace",
        description="namespace to deploy to",
    )
    image_url = validators.String(
        max_length=1000,
        min_length=3,
        title="Image URL",
        description="url of image to be deployed",
    )
    approver = validators.String(
        max_length=50,
        min_length=1,
        title="Approver",
        description="the person/entity who approved the deployment",
    )
    note = validators.String(
        max_length=4000,
        min_length=0,
        title="Note",
        description="notes regarding the deployment",
    )


class SecretData(types.Type):
    name = validators.String(
        max_length=20,
        title="Secret Name",
        description="name of the secret or something like that",
    )
    info = validators.String(default="nothing supplied", title="just some info")
    rating = validators.Integer(
        minimum=1, maximum=5, title="your rating", allow_null=True
    )
    in_stock = validators.Boolean(default=False)
    size = validators.String(enum=["small", "medium", "large"])


class UserData(types.Type):
    username = validators.String()
    password = validators.String()
