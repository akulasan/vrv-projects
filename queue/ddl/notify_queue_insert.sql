-- Notify when record was inserted into 'queue' table
CREATE OR REPLACE FUNCTION notify_queue_inserted()
  RETURNS trigger AS $$
DECLARE
BEGIN
  PERFORM pg_notify(
    CAST('queue_inserted' AS text),
    row_to_json(NEW)::text);
  RETURN NEW;
END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER notify_queue_inserted
  AFTER INSERT ON queue
  FOR EACH ROW
  EXECUTE PROCEDURE notify_queue_inserted();
