-- Notify when record was inserted into 'deployment' table
CREATE OR REPLACE FUNCTION notify_deployment_inserted()
  RETURNS trigger AS $$
DECLARE
BEGIN
  PERFORM pg_notify(
    CAST('deployment_inserted' AS text),
    row_to_json(NEW)::text);
  RETURN NEW;
END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER notify_deployment_inserted
  AFTER INSERT ON deployment
  FOR EACH ROW
  EXECUTE PROCEDURE notify_deployment_inserted();
