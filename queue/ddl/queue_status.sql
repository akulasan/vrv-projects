
-- statuses for the queue table.
CREATE TABLE queue_status (
  id          serial PRIMARY KEY,
  item_name   varchar(15) NOT NULL,
  item_desc   varchar(100) NOT NULL
);
