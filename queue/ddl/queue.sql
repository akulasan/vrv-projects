
-- the queue table stores build requests.
CREATE TABLE queue (
  id                serial PRIMARY KEY,
  queue_status_id   integer references queue_status(id) NOT NULL,
  data              json NOT NULL,
  created_by        varchar(100) NOT NULL,
  created_at        timestamp without time zone default (now() at time zone 'utc') NOT NULL
);
