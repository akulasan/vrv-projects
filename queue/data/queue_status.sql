
INSERT INTO queue_status ("name", "desc")
VALUES
('request','build requested'),
('vc_checkout','build performing checkout'),
('building','build building'),
('success','build completed'),
('failure','build failed')
;
