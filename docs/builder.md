# Builder

The builder picks up requests from the Queue and processes them (e.g. perform initial vc checkout if needed and then call the build script). The builder processes one record at a time sequentially. This means that if there are two build requests, the builder will build the oldest request (FIFO) and then proceed with the next to oldest request and so on until all builds are performed.

Builder is designed to have multiple builders running at a time. If there are two builders running, two builds can run simultaneously, three builders can run three builds simultaneously, etc.

After a builder finishes processing all requests in the queue, the builder sleeps for a given amount of time and then checks the queue again for new requests.

The application has the following general project types:

- standard (e.g. ab, sqlpad, nas): these are standarded apps that are dockerized themselves)
- wrapped (e.g. iig-docker, ncia-docker): these are stand-a-lone apps that are 'wrapped' by a docker project for building

What the builder does (for wrapped project):
- build gets build request
- things are setup (enrichment, set paths, etc.)
- if prj dir does not exist it is created as: `/opt/sources/${PRJ_NAME}`
- git clone performed
  - git clone -b repo_branch clone_opts src_repo src_name
    - example for iig-cs-app: git clone -b master git@bitbucket.org:nortondev/contentservice.git app
- build started (e.g. build-ab executed)
  - performs git clone of wrapper


## High-Level Overview
- check Queue
  - if => 1 item to process, begin processing
  - if < 1 item to process, sleep


## Item Processing Overview
- notify of build start
- perform vc_checkout
  - set item's queue status to vc_checkout
  - checkout
- perform build
  - update item's queue status to building
  - build (execute build-ab script)
  - update item's queue status to complete (success/failure)
- notify of build complete


## Misc Info

- initially we use python:3.6-alpine container for python components, this version used alpine 3.4 which has docker v1.11.2-r1 which does not include dockerd. rather than use this version we manually pull down docker from alpine v3.6 which has docker v 17.05.0-40. also note that alpine edge has docker v18.03.0-40 but lacks dependencies so we opt for v3.6 version SEE 'temp: install docker from apline v3.6 repo' below for more details
  - to do this we commented out docker in apk install and added the following after apk installs:
    ```
    \
    # temp: install docker from apline v3.6 repo \
    #&& wget http://dl-3.alpinelinux.org/alpine/v3.6/community/x86_64/docker-17.05.0-r0.apk \
    #&& apk add --allow-untrusted docker-17.05.0-r0.apk \
    #&& rm docker-17.05.0-r0.apk \
    \
    ```


## Overview of Build Request Params

- APP_NAME: name of the Application/Component
  - the application/component name
  - must match setup info within autobuild (e.g. Component Default - Component)
  - value used as the name of the image pushed to the IMAGE_REPO
- PRJ_NAME: name of Project
  - this is the high level 'project' name which will be created in the SOURCES_DIR and contain all code and logic for building this component
- SRC_NAME: name of 'source' dir within app
  - this is the name of the source code directory
  - this directory must contain the build-ab script and the Dockerfile
- BUILD_DIR: the build directory (directory within the SRC_NAME directory which has the build-ab and Dockerfile)
- SRC_REPO: the 'Clone URL' of the source
- REPO_BRANCH: the branch in the SRC_REPO to use
- CLEAN_SOURCE: clean (delete) the source before building?
- TAG_IMAGE_LATEST: tag the image with the 'latest' tag
- IMAGE_REPO: the repository to push the built image to
- PUSH_IMAGE_ENABLED: push the built image to the IMAGE_REPO?
- VC_TAG_ENABLED: tag the version control code?


## Build Project Setup Overview

This section shows how a build project is set up given the app/prj paramenters.

- everything is set up relative to ${SOURCES_DIR} which is typically located at `/opt/sources`
- the general project structure is as follows:
```
/opt/sources/
|-- example-basic-html
|   -- example-html-app
|       -- html-app
```
  - `example-basic-html` is the build project root and is set by the `PRJ_NAME` variable; this generally (but does not have to) matches the applications git repo project name.
  - `example-html-app` is the build source and is set by the `SRC_NAME` variable
  - `html-app` is the app name and is set by the `APP_NAME` variable


----
content-service-app (from iig-docker)
/opt/sources/
|-- content-service-app    <== Dockerfile is here
|   -- app                 <== app source code is here

----

