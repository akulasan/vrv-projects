# Overview

ab is comprised of a set of components, all python applications

## Components

- builder:
  - standard python app
  - pulls records from the queue and performs the build docker build, if push image is specified, the resulting docker image is pushed to PCR
  - each instance of builder can perform 1 build at a time
- wui (wooey)
  - flask python app
  - web interface that allows submitting records to the queue, performing deploys and other ab tasks
- queue
  - database (postgres) queue of builds and deploys
- api
  - apistar python app
  - exposes builder logic via api so you can do all of what wui does via an api
  - was built for evan to use with gitlab builds but they changed how they wanted to do things so its currently not used
- deployer
  - standard python app
  - currently not used
  - the purpose of this component was to have wui submit a request for deployment to queue and deployer would pick it up and do the deploy; however there were issues with it so it is not used, currently when a deploy request is submitted, wui issues the call directly to k8s
- maint
  - a simple alpine container that performs db backups via cron job
