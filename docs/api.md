# API Documentation

## Notices

- you can view the api documentation at `/docs`


## Usage
- setup:
```
# Dev
server=':5000'
base_url='ab-api'
# Prod
server='https://klb.wwnorton.com'
base_url='ab-api'
```
- index (no auth required): `http --body GET ${server}/${base_url}/index`
- login to get token: `http --body POST ${server}/${base_url}/login username=${user} password=${pwd}`
  - note: set the returned token to a variable named abToken for the remaining calls to work without any changes *or* see the ab_login helper below which does this for you.
- show welcome message: `http -j GET ${server}/${base_url}/ "Authorization: Bearer ${abToken}"`
- submit a build request: `http -j --body POST ${server}/${base_url}/build/submit "Authorization: Bearer ${abToken}" app_name=html-app src_branch=master vc_tag_enabled=False tag_image_latest=True push_image_enabled=True requester='dr' note='example call from api'`
- submit a deploy request: `http -j --body POST ${server}/${base_url}/deploy/submit "Authorization: Bearer ${abToken}" app_name=html-app namespace=kdo1 approver='dr' note='example call from api' image_url=pcr2.wwnorton.com/html-app:a2018-06-13.1119`
- submit build and deploy request: `http -j --body POST ${server}/${base_url}/build-and-deploy/submit "Authorization: Bearer ${abToken}" app_name=html-app run_build=True src_branch=master vc_tag='' vc_tag_enabled=False tag_image_latest=True push_image_enabled=True src_repo='https://gitlab.com/drad/sqlpad.git' requester='dr' namespace='kdo1'`


### Helpers

- ab_login function: add the following function to your ~/.bash_aliases or ~/.bashrc file, source it (e.g. `source ~/.bashrc`), and you can then simply use `ab_login` to get and set the abToken variable:
```
ab_login() {
    #http -j POST :5000/login username=test-service password=testservice@ab
    server=':5000'
    user='Your-Username'
    pwd='Your-Password'
    export abToken=$(http --body POST ${server}/login username=${user} password=${pwd} | python -c 'import json,sys;obj=json.load(sys.stdin);print obj["token"]')
    echo "- ab-api token set as abToken"
}
```
