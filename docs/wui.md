# wui

## Linting/Security
- flake8: is a multi-linter - it is a wrapper around the PyFlakes, pycodestyle, and Ned Batchelder's McCabe scripts and does a little more
  - connect to container
  - install flake8: `pip install flake8`
  - run flake8: `flake8 /opt/app`
    - NOTE: see the `.flake8` config file for ignored items and config of flake8
- bandit: `a tool designed to find common security issues in Python code`)
  - connect to container
  - install bandit: `pip install bandit`
  - run bandit: `bandit -r /opt/app`


## Database
Notice: as of 2018-05-01 we use flask-migrate.

- create migration
  - make some changes to model
  - create migration: `docker-compose run wui flask db migrate -m 'revision message'`
  - create manual migration: `docker-compose run wui flask db revision -m 'revision message'`
  - review created migration and make changes if necessary (should not be necessary)
- apply migration: `docker-compose run wui flask db upgrade`
- other things:
  - show migration history: `flask db history`
  - revert/rollback: `flask db downgrade`


## build scripts

```

NOTICE: the following configs are now in the 'Component Run Defaults' table in the ab db.
        this info is the 'source' for the tables.

```

* classroom-app:
```
{
  "APP_NAME": "classroom-app",
  "PRJ_NAME": "classroom-app",
  "SRC_NAME": "classroom",
  "SRC_REPO": "git@bitbucket.org:nortondev/classroom.git",
  "REPO_BRANCH": "master",
  "CLEAN_SOURCE": "false",
  "TAG_IMAGE_LATEST": "false",
  "IMAGE_REPO": "pcr.wwnorton.com:5000",
  "PUSH_IMAGE_ENABLED": "true",
  "VC_TAG_ENABLED": "true"
}
```

* dlp-app:
```
{
  "APP_NAME": "dlp-app",
  "PRJ_NAME": "dlp-app",
  "SRC_NAME": "ncia",
  "SRC_REPO": "ssh://git@stash.wwnorton.com:7999/ncia/ncia.git",
  "REPO_BRANCH": "Summer_2018_DLPUpdates",
  "CLEAN_SOURCE": "false",
  "TAG_IMAGE_LATEST": "false",
  "IMAGE_REPO": "pcr.wwnorton.com:5000",
  "PUSH_IMAGE_ENABLED": "false",
  "VC_TAG_ENABLED": "false"
}
```

* sw5-app:
```
{
  "APP_NAME": "sw5-app",
  "PRJ_NAME": "sw5-app",
  "SRC_NAME_CLIENT": "sw5-client",
  "SRC_NAME_SERVER": "sw5-server",
  "SRC_REPO_CLIENT": "git@bitbucket.org:nortondev/sw5-client.git",
  "SRC_REPO_SERVER": "git@bitbucket.org:nortondev/sw5-server.git",
  "RUN_BUILD": "true",
  "VC_CLONE_OPTS": "",
  "IMAGE_REPO": "pcr.wwnorton.com:5000",
  "PUSH_IMAGE_ENABLED": "false",
  "REPO_BRANCH_CLIENT": "master",
  "REPO_BRANCH_SERVER": "master",
  "SW5_APP_MODULE_DEPLOYS_ENABLED": "true",
  "SW5_APP_MODULE_DEPLOYS_ENV": "kuat1",
  "SW5_APP_PATCH_SERVER_URL": "34.193.76.59",
  "TAG_IMAGE_LATEST": "false",
  "VC_TAG_ENABLED": "false",
  "CLEAN_SOURCE": "false"
}
```

* sw5-ws:
```
{
  "APP_NAME": "sw5-ws",
  "PRJ_NAME": "sw5-ws",
  "SRC_NAME": "ncia-docker",
  "SRC_REPO": "ssh://git@bitbucket.org/nortondev/ncia-docker.git",
  "REPO_BRANCH": "feature/ibd",
  "CLEAN_SOURCE": "false",
  "TAG_IMAGE_LATEST": "false",
  "IMAGE_REPO": "pcr.wwnorton.com:5000",
  "PUSH_IMAGE_ENABLED": "false",
  "VC_TAG_ENABLED": "false"
}
```


* sqlpad:
```
{
  "APP_NAME": "sqlpad",
  "PRJ_NAME": "sqlpad",
  "SRC_NAME": "sqlpad",
  "SRC_REPO": "https://github.com/dradux/sqlpad.git",
  "REPO_BRANCH": "master",
  "CLEAN_SOURCE": "false",
  "TAG_IMAGE_LATEST": "false",
  "IMAGE_REPO": "pcr.wwnorton.com:5000",
  "PUSH_IMAGE_ENABLED": "false",
  "VC_TAG_ENABLED": "false"
}
```

## GlyphIcons
- [glyphicons.com](https://glyphicons.com/)
  - works: icon-info-sign, icon-more-items, icon-upload, icon-film, icon-book, icon-tag, icon-tags, icon-warning-sign


## Links
- [PyYAML](https://pypi.org/project/PyYAML/)
- [flask-migrate](https://github.com/miguelgrinberg/flask-migrate/)
- [SQLAlchemy Postgresql ORM](https://www.compose.com/articles/using-postgresql-through-sqlalchemy/)
- [kubernetes.client.CoreV1Api](https://github.com/kubernetes-client/python/blob/master/kubernetes/docs/CoreV1Api.md#list_namespaced_pod)
- [redis](https://pypi.org/project/redis/)
- [bandit](https://github.com/PyCQA/bandit)
- [flake8](https://gitlab.com/pycqa/flake8)
