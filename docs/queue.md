# Queue

The Queue is a database (postgres) which contains the queue table. Each row of the queue table corresponds to a build request. As a builder picks up an item from the queue it makes the item as 'in process' so other builders do not also process the build request. Durning processing of the build request, the builder updates the record in the queue, setting its status as appropriate (request, building, failure, success, etc.). Items remain in the queue indefinitely as this provides a history of build requests with their corresponding status.


## Useful Docker Info
- dump db: `docker-compose exec queue /bin/bash -c "pg_dump --username=postgres -b postgres > /dumps/$(date +"%F_%H%M")_full.dmp"`
- restore db dump: `docker-compose exec queue /bin/bash -c "cat /dumps/2018-05-06_1752_full.dmp | psql --username=postgres postgres"`
  - note: place the dump in the `queue/dumps` directory and adjust the path/filename accordingly.


## Useful SQL

```
-- list queue_status
select id,item_name from queue_status;

-- view queue
select q.id,qs.item_name as status from queue q join queue_status qs on q.queue_status_id=qs.id;
-- view queue showing app_name.
select q.id,qs.item_name as status,q_data ->> 'APP_NAME' as app_name from queue q join queue_status qs on q.queue_status_id=qs.id order by id desc limit 4;

```



pg_dump -Fc -b postgres | xz -9 --stdout - > "$(date +"%F_%H%M")_full.dmp.xz"
