# New User

To set up a new user in ab:

1. create a new user in kubernetes
  - Global > Users > Add User
    * Username: create a username such as 'drader'
    * Password: select Generate (keep password temporarily in order to login and get access token)
    * Display Name: enter the user's full name such as 'David Rader'
    * Under Global Permissions, select Custom and then make sure there is a check for "Login Access" only
2. get user's bearer token
  - login to rancher as the new user
  - go to User Account > API & Keys
  - click "Add Key" to create a new key
  - give the key a description such as "key used by ab"
  - select "Never" under Automatically Expire
  - click "Create" to create the key
  - copy the "Bearer Token" information
3. create user in ab
  - Config > Users > Create
  - Roles: user
  - Name: user's full name (e.g. David Rader)
  - Username: username (e.g. drader)
  - Nickname: short (usually 2-3 letter) nickname for user (e.g. dr)
  - Email: user's email address (e.g. drader@adercon.com)
  - Active: checked
  - Note: add a note about the user if appropriate
  - New Password:
  - Kube Bearer Token: use "Bearer Token" information from previous step
4. assign needed KUN roles
  - go to the Project that the user needs to deploy to
  - add the following Roles for the user by clicking Add Member
    * BaseWWNUser
    * ab_deploy
