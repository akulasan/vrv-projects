# todo

Todo items for ab.


# Next Release
- change wui such that it grabs AppConfig for app on queue create request, lets user review/change it, then submits it
  - NOTE: this is a bigger change that should come with additional rework (e.g. the 'enrichment' flow likely needs to change/go away)
- add the 'clean' logic back in (sunil seems to need this on sw5 code every once in a while - need to check if it is 'safe')
- for some reason nothing is going to graylog now...
  - interesting: startup of app goes to graylog but nothing else. I do see other log info in the container's logs...


## Critical
- deployer deployments do not work
  - symptom: works locally, does not work when I deploy it
  - current wui has deploy logic added back in so we will have to use the api deployment to test this logic in prd
  - this appears to be fixed now, need to revert ab:wui changes, deploy and test. WE MAY want to roll this out with the pubsub change
- any poller (builder, deployer) needs to be changed to a notify, pub/sub, etc. what we want is for the db (a trigger likely) to use something (nanomsg, zeromq, etc.) to send a msg to a service (builder/deployer), this service then 'wakes up' and gets the data. the service may also need a poll to ensure we dont miss something but the poll can be longer-lived (say every 5m vs every 15s)
  - this item is a necissity because as it is now it is near impossible to deploy a db (queue) change as one of the services or components which talk to the queue keeps a lock on it due to the poll frequency.
  - pgpubsub package seems to handle this very well, see the following for the basis:
- we should add something to ab deployment to write the previous image of a container to graylog in case wui crashes!


## Standard Ordered List
- deployer does not run/work in kubernetes, works fine locally
- add logic so we can deploy updates to non-service based apps (e.g. gs_issues_monitor service)


## Later Bucket
- add maint into service_status
  - also likely want to add pygal into this mix, not sure what or how we can use it at this point though; pygal works well for nice charts, easy enough to use but as of right now we dont have a lot of need for it. For the time-being I think a simple table will work.
- live updates of build status in queue (this is livern's request); looks like we have the following options:
  - websockets (see: https://www.shanelynn.ie/asynchronous-updates-to-a-webpage-with-flask-and-socket-io/)
    - notoriously difficlut at the firewall and corporate network level
  - ajax
  - NOTE: examples of both: https://stackoverflow.com/questions/15721679/update-and-render-a-value-from-flask-periodically
- wui/general
  - add logic to see if a build of the same app is already running, if so notify user
  - add logic to check and make sure the ComponentRunDefault record exists.
  - add logic for user to add 'custom' data to queue request (e.g. sw5 fields)
  - need to handle 'edit' logic on Queue
  - add ability to cancel a build
- wui/deployment
  - add rollback logic (not sure if we can use a real rollback or we need something like a patch with last image (e.g. `kubectl patch pod authentication-service-app --type='json' -p='[{"op": "replace", "path": "/spec/containers/0/image", "value":"pcr2.wwnorton.com/authentication-service-app:a2018-06-05.1512"}]'`
  - handle deployment of new apps (e.g. apps that have not already been setup `kubectl deploy`)
    - design and build out how we will do new deploys
      - use specs we already have or should they provide it? I like providing it, we dont have to capture/track it as it'll be in kubernetes if needed
  - handle if image does not exist (e.g. if image to deploy is not in PCR...currently the deploy will fail as kubernetes will throw an ImageBackoff error)
  - may want logic that copies the service_spec from the namespace_app into the deployment request, user can then edit it (change ports, change vars such as db url or port, etc.) then on submit we send this spec. This also keeps an exact history of the spec sent.
  - fix the '500: Path Error' (see utils.py 'Path Error') on Deployment > View existing deployment
- wui/queue
  - add deployment request # field
    - also need a config item of path to use for this so we can build a hyperlink to it
    - also add all the logic to update a jira ticket with the results after a build
  - add role logic for submitting builds
  - add logic to send image version (IMAGE_VERSION) in build request - this is the ability to make the image version something like ab-wui:v1.0.0
  - request by subhash: add a 'rebuild' option (select a previous queue item and submit it for rebuild)
  - make "Tag Source" (checkbox) required or default to true on build so sunil does not forget to do it
    - NOTICE: there is an issue with doing things, things like sqlpad (which have an external git repo that is  not set up for writes from ab) will have issues as you cannot tag without write access to the repo - maybe we turn it around and make it default unless a "Skip VC Tagging" flag is set
- builder
  - we have added some basic error handling in app.py but need to do more
  - clean up logging, right now the builder is sending a msg per line of log write to graylog which makes a lot of messages, I would rather have things in larger chunks so each msg is multi-line and generally has everything together vs chunked into separate log lines
    - compact logging - some things we can put in one message as opposed to a log message for each 'new line'
- deployer
  - the watch on patch deployment is disabled as it causes issues (chunked encoding issue) - wonder if this could be docker version related??
- build-ab script
  - if you change build-ab the change does not take effect until after the first run (this is due to build-ab updating itself yet it does not 'reload' itself after update so it finishes run with 'old' instance)
    - NOTICE: this is partially handled by the addition of 'build templates' as it is not an issue there
- api
  - issue on build and deploy, if build fails the api poller just keeps polling, waiting for it to finish...indefinitely
    - can we add logic to the poller to see if the item failed? that's about as good as we can do, can't do much with the scenario where a build fails but status is left as 'building' as we dont know how long each build will take
      - something complicated would be to track the id of the builder that picked the item up, if that id is gone from service_status it means something happened - this could be a separater watcher service that does this???
- sunil caused issues:
  - add something to ab help that explains they need to have a build-ab in the branch they are building.
  - we should add a 'version' to build-ab script and also a version in builder that checks this version.
  - need to add something in builder that checks validity of build-ab script.


## Other / Misc
- investigate the [python client library](https://kubernetes.io/docs/tasks/access-application-cluster/access-cluster/#python-client)
  - also see [linux.com article](https://www.linux.com/learn/kubernetes/enjoy-kubernetes-python)
