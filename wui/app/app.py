import uuid
import logging
import graypy
import random
import string
from threading import Thread
from flask import Flask, render_template, request
from flask_admin import Admin
from flask_security.datastore import SQLAlchemyUserDatastore

from flask_security import login_required, Security, logout_user
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
from werkzeug.contrib.fixers import ProxyFix
from flask_debugtoolbar import DebugToolbarExtension

from flask_ldap3_login import LDAP3LoginManager
from flask_login import LoginManager, login_user, current_user
from flask import render_template_string, redirect
from flask_ldap3_login.forms import LDAPLoginForm

# NOTE: ldap3 used for secondary (readonly) bind to get user's groups.
from ldap3 import Server, Connection, ALL

import config as Config
from utils import (
    ping_service_status,
    PrefixMiddleware,
    get_kube_bearer_token,
    get_instance_logs,
    PodLogRequest,
    PodRequest,
    get_instance_specs,
)

about = {
    "name": "ab-wui",
    "version": "3.5.0",
    "modified": "2019-01-08",
    "created": "2018-04-26",
}
service_id = str(uuid.uuid4())[:8]

logger_base = logging.getLogger("main_logger")
logger_base.setLevel(logging.getLevelName(Config.DefaultConfig.APP_LOGLEVEL))
graylog_handler = graypy.GELFHandler(
    host=Config.DefaultConfig.GRAYLOG_HOST, port=Config.DefaultConfig.GRAYLOG_PORT
)
console_handler = logging.StreamHandler()
if "graylog" in Config.DefaultConfig.LOG_TO:
    logger_base.addHandler(graylog_handler)
if "console" in Config.DefaultConfig.LOG_TO:
    logger_base.addHandler(console_handler)

logger = logging.LoggerAdapter(
    logging.getLogger("main_logger"),
    {
        "application_name": Config.DefaultConfig.GRAYLOG_APPNAME,
        "application_env": Config.DefaultConfig.GRAYLOG_APPENV,
    },
)


logger.info(
    "{0} - v.{1} ({2}) - {3}".format(
        about["name"], about["version"], about["modified"], service_id
    )
)
logger.info(
    "logs are being sent to: {0}".format(
        ", ".join(str(i) for i in Config.DefaultConfig.LOG_TO)
    )
)

# Create application
app = Flask(__name__)
app.config.from_object(Config.BaseConfig)
if Config.DefaultConfig.DEBUG:
    toolbar = DebugToolbarExtension(app)
db = SQLAlchemy(app)
migrate = Migrate(app, db)
from models import *
from views import *

login_manager = LoginManager(app)  # Setup a Flask-Login Manager
ldap_manager = LDAP3LoginManager(app)  # Setup a LDAP3 Login Manager.

# Create a dictionary to store the users in when they authenticate
# This example stores users in memory.
users = {}


@login_manager.user_loader
def load_user(user_id):
    """User Loader for Flask-Login: simply returns the User if it exists
    in our 'database', otherwise returns None.
    """

    logger.debug("load_user by id={0}".format(user_id))
    # if id in users:
    #    return users[id]
    # return None
    return User.get(user_id)


@ldap_manager.save_user
def save_user(dn, username, data, memberships):
    """User Saver for Flask-Ldap3-Login: this method is called whenever
    a LDAPLoginForm() successfully validates. Here you have to save the
    user, and return it so it can be used in the login controller.
    """
    logger.debug("saving user as: dn={0}, username={1}".format(dn, username))
    upw = data["userPassword"][0].decode()
    # logger.debug("NOTICE: user's pwd is=[{0}]".format(upw))
    # connect to ldap as 'readonly' user to get user's groups.
    server = Server(Config.DefaultConfig.LDAP_HOST, use_ssl=True, get_info=ALL)
    conn = Connection(
        server,
        Config.DefaultConfig.LDAP_BIND_USER_DN,
        Config.DefaultConfig.LDAP_BIND_USER_PASSWORD,
        auto_bind=True,
    )
    # get all of the groups the user is a member of
    ldap_groups = []
    groups_filter = "(&(objectClass=*)(member=cn={0},dc=wwnorton,dc=com))".format(
        username
    )
    conn.search("ou=groups,dc=wwnorton,dc=com", groups_filter, attributes=["cn"])
    for entry in conn.entries:
        for role in entry:
            print("adding ldap role of: {0}".format(role))
            ldap_groups.append(role)

    # dont allow users with no roles.
    if not ldap_groups:
        logger.error("No Roles found for user: {0}".format(username))
        return None

    # setup user.
    name = "{0} {1}".format(data["givenName"][0], data["sn"][0])
    username = username
    nickname = data["displayName"]
    roles = []

    # logger.debug("user: name={0}, nickname={1}".format(name,nickname))
    # roles/groups.
    for r in ldap_groups:
        role = str(r)
        logger.debug("user is member of: [{0}]".format(role))
        # roles.append(role)
        # check if roles exist, if not add them
        app_role = Role.query.filter_by(name=role).first()
        if not app_role:
            logger.info("adding new role: {0}".format(role))
            app_role = Role(name=role, description="added from ldap login")
        logger.debug("- adding app_role: {0}".format(app_role))
        roles.append(app_role)

    # check if user exists
    user = User.query.filter_by(username=username).first()
    if user:
        # existing user - update
        for role in user.roles:
            logger.debug("app user role: {0}".format(role.name))
        user.name = name
        # user["username"] = username   # we dont update this
        user.nickname = nickname
        user.roles = roles
    else:
        # new user - create
        password = "".join(
            [
                random.choice(string.ascii_letters + string.digits + string.punctuation)
                for n in range(33)
            ]
        )
        user = User(
            name=name,
            username=username,
            nickname=nickname,
            password=password,
            roles=roles,
        )
    # get user's kbt.
    user.kube_bearer_token = get_kube_bearer_token(user=user, upw=upw)
    db.session.add(user)
    db.session.commit()

    logger.debug("user is: {0}".format(user))
    users[username] = user
    return user


# apply any/all pending migrations.
# ~ 2018-06-07:drad:disabling because currently this is ran twice per start of app (dont know why)
# ~   and this can cause deadlock issues on applying migrations.
# ~ if Config.DefaultConfig.DEPLOY_ENV != 'dev':
# ~ with app.app_context():
# ~ from flask_migrate import upgrade as _upgrade
# ~ logger.info('Applying pending DB Migrations...')
# ~ _upgrade()
# ~ logger.info('DB Migrations finished.')

# Setup Flask-Security
user_datastore = SQLAlchemyUserDatastore(db, User, Role)
security = Security(app, user_datastore)


@app.route("/")
@login_required
def index():
    return render_template("index.html")


@app.route("/admin/logout")
@login_required
def logout_view():
    logout_user()
    return redirect(url_for("admin.index", next=url_for("admin.index")))


@app.route("/admin/login")
def login_view():
    return redirect(url_for("security.login", next=url_for("admin.index")))


@app.route("/admin/login_ldap", methods=["GET", "POST"])
def ldap_login_view():
    template = (
        """
    <style>
        #loginbox {
            margin-left: auto;
            margin-right: auto;
            margin-top: 66px !important;
            width: 400px;
            border: 2px solid #BFBFBF;
            border-radius: 13px;
            padding: 7px 7px 7px 7px;
            margin-top: 10px;
            display: block;
            margin-left: auto;
            margin-right: auto;
            box-shadow: 5px 10px #888888;
        }
        .login-header {
            margin-left: auto;
            margin-right: auto;
            margin-top: 10px !important;
            padding: 13px 0px 23px 0px;
            font-family: "Times New Roman", Times, serif;
            font-size: 28px;
            font-weight: bold;
            width: 100%;
            text-align: center;
        }
        .ldap-url {
            margin-left: auto;
            margin-right: auto;
            /* margin-top: 5px !important; */
            padding: 3px 0px 3px 0px;
            font-family: "Times New Roman", Times, serif;
            font-size: 10px;
            font-weight: normal;
            width: 100%;
            text-align: right;
        }
        label {
            width: 100px;
            display: inline-block;
        }
        input {
            width: 225px;
            display: inline-block;
        }
        #submit {
            width: 100px;
        }
        .actions {
            padding: 13px 13px 13px 13px;
            margin-top: 13px;
            display: block
            margin-left: auto;
            margin-right: auto;
        }
        .flashes {
            background: yellow;
            font-weight: bolder;
            font-size: 13px;
            line-height: 33px;
        }
        .flashes li.info {
            list-style-type: none;
            text-align: center;
            align: center;
        }
    </style>
    <div id="loginbox">
      <div class="login-header">ab login</div>
      <div class="top-pad">
        {% if get_flashed_messages() %}
        <div class="flashes">{{ get_flashed_messages() }}</div>
        {% endif %}
        {% if form.errors %}
        <div class="flashes">{{ form.errors }}</div>
        {% endif %}
        <form method="POST">
            <div class="form-group"><label>Username</label>{{ form.username() }}</div>
            <div class="form-group"><label>Password</label>{{ form.password() }}</div>
            <div class="actions">
              {{ form.submit() }}
              {{ form.hidden_tag() }}
            </div>
        </form>
      </div>
      <div class="ldap-url" title="LDAP URL: """
        + Config.DefaultConfig.LDAP_HOST
        + """:"""
        + str(Config.DefaultConfig.LDAP_PORT)
        + """" style="cursor: hand;">(?)</div>
    </div>
"""
    )

    # Instantiate a LDAPLoginForm which has a validator to check if the user
    # exists in LDAP.
    form = LDAPLoginForm()
    if form.validate_on_submit():
        # Successfully logged in, We can now access the saved user object
        # via form.user.
        # check if user is set (note: it wont be if user has no roles).
        # logger.debug("You are logging in as user=[{0}] pwd=[{1}]".format(form.user, form.password.data))
        # put_user_session(user_session={'username': form.user, 'password': form.password.data})
        if form.user:
            # cache user pwd to redis so we can use it to login to rancher to get kbt.
            login_user(form.user)  # Tell flask-login to log them in.
            return redirect("/autobuild/admin/")  # Send them home
        else:
            flash("User has no Roles")
            return redirect("/autobuild/admin/login_ldap")

    return render_template_string(template, form=form)


@app.route("/admin/custom_detail_viewer", methods=["GET"])
@login_required
def custom_detail_viewer():
    item_type = request.args.get("item-type")
    data = "Item Type [{0}] not found, viewer not properly initialized.".format(
        item_type
    )
    item_namespace = request.args.get("item-namespace")
    item_name = request.args.get("item-name")
    if item_type == "Log":
        data = get_instance_logs(
            current_user=current_user,
            data=PodLogRequest(item_namespace, item_name, "", False, True, False),
        )
    elif item_type == "Spec":
        data = get_instance_specs(
            current_user=current_user, data=PodRequest(item_namespace, item_name, True)
        )

    template = """
<h3>{{ title }} Viewer: {{ name }}</h3>
<form>
<textarea name="custom-detail-data" style="width: 100%; height: 90%;" readonly="readonly">
{{ data }}
</textarea>
</form>
<div>* close window when finished</div>
        """
    return render_template_string(template, title=item_type, data=data, name=item_name)


# ~ @app.route('/change_password')
# ~ def change_password():
# ~ return render_template('security/change_password.html')

# ~ @app.route('/admin/queue/deploy/<qid>')
# ~ @login_required
# ~ def queue_deploy(qid):
# ~ return render_template('admin/queue/deploy.html')

# ~ @app.route('/register', methods=['GET', 'POST'])
# ~ def register():
# ~ form = RegistrationForm(request.form)
# ~ if request.method == 'POST' and form.validate():
# ~ user = User(form.username.data, form.email.data,
# ~ form.password.data)
# ~ db_session.add(user)
# ~ flash('Thanks for registering')
# ~ return redirect(url_for('login'))
# ~ return render_template('register.html', form=form)

# Create admin
admin = Admin(
    app,
    name="ab",
    base_template="layout.html",
    index_view=HomeView(name="Home"),
    category_icon_classes={"Config": "glyphicon glyphicon-wrench"},
)

# Primary items.
admin.add_view(QueueView(Queue, db.session, name="Builds"))
admin.add_view(DeploymentView(Deployment, db.session, name="Deployments"))

# Misc.
admin.add_view(
    AppListView(
        name="App List",
        menu_icon_type="glyph",
        menu_icon_value="glyphicon-tags",
        endpoint="applist",
    )
)

# Stats.
admin.add_view(
    StatsBuildsSevenView(
        name="Builds 7", endpoint="stats-builds-seven", category="Stats"
    )
)
admin.add_view(
    StatsBuildsView(name="Builds All", endpoint="stats-builds", category="Stats")
)
admin.add_view(
    StatsBuildSizeSevenView(
        name="Build Sizes 7", endpoint="stats-build-sizes-seven", category="Stats"
    )
)
admin.add_view(
    StatsBuildTimesSevenView(
        name="Build Times 7", endpoint="stats-build-times-seven", category="Stats"
    )
)
admin.add_view(
    StatsDeploysView(name="Deploys All", endpoint="stats-deploys", category="Stats")
)
admin.add_view(StatusView(name="System Status", endpoint="status", category="Stats"))

# Config items
admin.add_view(
    ApiAccountView(
        ApiAccount,
        db.session,
        name="API Accounts",
        category="Config",
        endpoint="api_account",
    )
)
admin.add_view(
    ComponentRunDefaultView(
        ComponentRunDefault,
        db.session,
        name="App Config",
        category="Config",
        endpoint="app_config",
    )
)
admin.add_view(
    BuildTemplateView(
        BuildTemplate,
        db.session,
        name="Build Templates",
        category="Config",
        endpoint="build_templates",
    )
)

# ~ admin.add_view(
# ~ DeployClusterView(
# ~ DeployCluster, db.session, name="Deploy Clusters", category="Config"
# ~ )
# ~ )
# ~ admin.add_view(
# ~ ClusterNamespaceView(
# ~ ClusterNamespace, db.session, name="Cluster Namespaces/Apps", category="Config"
# ~ )
# ~ )

admin.add_view(
    QueueStatusView(QueueStatus, db.session, name="Queue Statuses", category="Config")
)
admin.add_view(
    DeploymentStatusView(
        DeploymentStatus, db.session, name="Deployment Statuses", category="Config"
    )
)
admin.add_view(
    RoleAdmin(Role, db.session, name="Roles", category="Config", endpoint="role")
)
admin.add_view(TagView(Tag, db.session, name="Tags", category="Config"))
# ~ 2019-01-10:drad no longer needed as LDAP has roles piece, use UserAdminUser instead.
# ~ admin.add_view(
# ~ UserAdmin(User, db.session, name="Users", category="Config", endpoint="user")
# ~ )
admin.add_view(
    UserAdminUser(
        User, db.session, name="Users", category="Config", endpoint="user_user"
    )
)
admin.add_view(
    SystemConfigView(
        User, db.session, name="System", category="Config", endpoint="system-config"
    )
)
admin.add_view(
    AccountView(
        User, db.session, name="Account", endpoint="account_user", category="Config"
    )
)

admin.add_link(LogoutMenuLink(name="Logout", category="", endpoint="logout_view"))
admin.add_link(LoginMenuLink(name="Login", category="", endpoint="ldap_login_view"))
# admin.add_view(AccountView(User, db.session, name='Account', endpoint='account_user', category='User'))

admin.add_view(
    OnlineHelpView(name="Online Help", endpoint="online_help", category="Help")
)
admin.add_view(AboutView(name="About", endpoint="help_about", category="Help"))

# proxyfix in case you are running your site on a non-standard port/proxying.
app.wsgi_app = PrefixMiddleware(
    ProxyFix(app.wsgi_app), prefix=Config.DefaultConfig.WUI_BASE_URL
)
# app.wsgi_app = DispatcherMiddleware(simple, {'/autobuild': app.wsgi_app})

# start a background thread that keeps updating the service status.
#   note: thread runs indefinitely.
thread = Thread(target=ping_service_status, args=(service_id, about))
thread.start()

# ~ if __name__ == '__main__':
# ~ app_dir = op.realpath(os.path.dirname(__file__))
# ~ app.run()   # start app
