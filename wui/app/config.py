import os
from distutils.util import strtobool


def env_strtobool(val):
    if val in ["true", "'true'"]:
        return True
    return False


class BaseConfig(object):
    APP_LIST_DEFAULT_ITEM = "-- Select App --"

    APP_LOGLEVEL = os.environ["APP_LOGLEVEL"]

    CACHE_HOST = os.environ["CACHE_HOST"]
    CACHE_PORT = os.environ["CACHE_PORT"]
    CACHE_DB = os.environ["CACHE_DB"]
    CACHE_NS_LIST_TTL = os.environ["CACHE_NS_LIST_TTL"]
    CACHE_APP_LIST_TTL = os.environ["CACHE_APP_LIST_TTL"]
    CACHE_USER_SESSION_TTL = os.environ["CACHE_USER_SESSION_TTL"]
    CACHE_WUI_SERVICE_STATS_TTL = os.environ["CACHE_WUI_SERVICE_STATS_TTL"]

    CLS_URL = os.environ["CLS_URL"]
    # note: do not change this or all of the KBT keys will be unencryptable!
    # the IV a 16 character byte value from: Random.new().read(16)
    CRYPTO_KBT_IV = b"D\xca\xc9\xa8\x1a\xcb\xdb\xf1\x89H\xf9\x85\xb6\xe4\xb0\xdd"
    CRYPTO_KBT_KEY = "abcdefghijklmnop"

    DB_NAME = os.environ["DB_NAME"]
    DB_USER = os.environ["DB_USER"]
    DB_PASS = os.environ["DB_PASS"]
    DB_SERVICE = os.environ["DB_SERVICE"]
    DB_PORT = os.environ["DB_PORT"]

    DEBUG = env_strtobool(os.environ["DEBUG"])
    #   flask debug toolbar.
    DEBUG_TB_INTERCEPT_REDIRECTS = env_strtobool(
        os.environ["DEBUG_TB_INTERCEPT_REDIRECTS"]
    )

    DEFAULT_ADMIN_USER = os.environ["DEFAULT_ADMIN_USER"]
    DEFAULT_ADMIN_PASSWORD = os.environ["DEFAULT_ADMIN_PASSWORD"]

    DEPLOY_ENV = os.environ["DEPLOY_ENV"]

    EXCLUDED_NAMESPACES = os.environ["EXCLUDED_NAMESPACES"]

    LDAP_APP_ADMIN_KBT_USERNAME = os.environ["LDAP_APP_ADMIN_KBT_USERNAME"]
    LDAP_APP_ADMIN_KBT_PASSWORD = os.environ["LDAP_APP_ADMIN_KBT_PASSWORD"]
    LDAP_HOST = os.environ["LDAP_HOST"]
    LDAP_PORT = int(os.environ["LDAP_PORT"])
    LDAP_USE_SSL = env_strtobool(os.environ["LDAP_USE_SSL"])
    # The RDN attribute for your user schema on LDAP
    LDAP_USER_RDN_ATTR = "cn"
    # The Attribute you want users to authenticate to LDAP with.
    LDAP_USER_LOGIN_ATTR = "cn"
    # user attributes to get.
    # LDAP_GET_USER_ATTRIBUTES = ldap3.ALL_ATTRIBUTES
    # search with bind user.
    # ~ LDAP_ALWAYS_SEARCH_BIND = True
    LDAP_ALWAYS_SEARCH_BIND = False
    # The Username to bind to LDAP with
    LDAP_BIND_USER_DN = os.environ["LDAP_BIND_USER_DN"]
    # The Password to bind to LDAP with
    LDAP_BIND_USER_PASSWORD = os.environ["LDAP_BIND_USER_PASSWORD"]
    # Base DN of your directory
    LDAP_BASE_DN = "dc=wwnorton,dc=com"
    # Users DN to be prepended to the Base DN (ou=users)
    # ~ LDAP_USER_DN = 'ou=users'
    LDAP_USER_DN = ""
    # Group settings
    # NOTICE: keep the following to False; we do not use Flask-Ldap3-Login's group search as something dont work with it, rather use use ldap3 directly.
    LDAP_SEARCH_FOR_GROUPS = False
    # Groups DN to be prepended to the Base DN
    # LDAP_GROUP_DN = 'ou=groups'
    # LDAP_GROUP_OBJECT_FILTER = '(objectClass=groupOfNames)'
    # LDAP_GROUP_OBJECT_FILTER = None
    # LDAP_GROUP_MEMBERS_ATTR = 'member'
    # Group scope ([LEVEL], SUBTREE) there are other levels (base/object, subordinates
    # LDAP_GROUP_SEARCH_SCOPE = 'LEVEL'

    LOG_TO = os.environ["LOG_TO"].split(",")

    KUBE_SERVER_URL = os.environ["KUBE_SERVER_URL"]
    KUBE_CLUSTER_ID = os.environ["KUBE_CLUSTER_ID"]

    GRAYLOG_HOST = os.environ["GRAYLOG_HOST"]
    GRAYLOG_PORT = int(os.environ["GRAYLOG_PORT"])
    GRAYLOG_APPNAME = "ab-wui"
    GRAYLOG_APPENV = os.environ["DEPLOY_ENV"]

    NS_LIST_DEFAULT_ITEM = "-- Select Namespace --"

    SECRET_KEY = os.environ["SECRET_KEY"]
    SECURITY_PASSWORD_HASH = os.environ["SECURITY_PASSWORD_HASH"]
    SECURITY_PASSWORD_SALT = os.environ["SECURITY_PASSWORD_SALT"]
    SECURITY_RECOVERABLE = os.environ["SECURITY_RECOVERABLE"]
    SECURITY_CHANGEABLE = os.environ["SECURITY_CHANGEABLE"]

    SQLALCHEMY_DATABASE_URI = "postgresql://{0}:{1}@{2}:{3}/{4}".format(
        DB_USER, DB_PASS, DB_SERVICE, DB_PORT, DB_NAME
    )
    SQLALCHEMY_ECHO = env_strtobool(os.environ["SQLALCHEMY_ECHO"])
    SQLALCHEMY_TRACK_MODIFICATIONS = False

    WUI_BASE_URL = os.environ["WUI_BASE_URL"]


class DefaultConfig(BaseConfig):

    # Statement for enabling the development environment
    # DEBUG = True
    pass
