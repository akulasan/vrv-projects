import arrow
import logging
import textwrap

from flask_admin.contrib import sqla
from flask_admin import BaseView, expose, AdminIndexView
from flask_security import current_user, utils
from wtforms.validators import ValidationError
from wtforms.fields import (
    PasswordField,
    TextField,
    BooleanField,
    TextAreaField,
    SelectField,
    HiddenField,
)
from flask_admin.menu import MenuLink
from flask_admin.model.form import InlineFormAdmin

from jinja2 import Markup
from flask import url_for, request, redirect, flash

from sqlalchemy import text

# pygal
import pygal
from pygal.style import (
    DefaultStyle,
    LightStyle,
    CleanStyle,
    RedBlueStyle,
    DarkGreenStyle,
    BlueStyle,
    LightGreenStyle,
    LightColorizedStyle,
    LightSolarizedStyle,
)
from pygal import Config as pygalConfig

# kubernetes
from kubernetes import client, watch
from kubernetes.client.rest import ApiException

# local
from models import *
from app import about, service_id
from utils import *
import config as Config

logger = logging.getLogger("main_logger")


class NamespaceAppForm(InlineFormAdmin):
    def is_accessible(self):
        is_allowed = False
        if current_user.is_authenticated:
            if current_user.has_role("admin") or current_user.has_role("user"):
                is_allowed = True
        return is_allowed

    def inaccessible_callback(self, name, **kwargs):
        # redirect to login page if user does not have access.
        # return redirect(url_for("security.login"))
        return redirect("admin/login_ldap")

    def _handle_view(self, name, **kwargs):
        if not self.is_accessible():
            return self.inaccessible_callback(name, **kwargs)
        if current_user.has_role("admin"):
            self.can_delete = True
        else:
            self.can_delete = False

    form_columns = ("id", "name", "description", "active", "service_spec")
    form_label = "App"
    column_labels = {"service_spec": "Service Specification"}
    form_widget_args = {
        "name": {
            "placeholder": "sqlpad, dlp-app, sw5-app, classroom-app, sw5-ws, etc.",
            "title": "enter the name of the app to add",
        },
        "description": {
            "placeholder": "the dlp-application, the sw5-application, etc.",
            "title": "enter a description of the app",
        },
        "service_spec": {
            "placeholder": "enter the service specification (yaml) here",
            "title": "enter the service specification (yaml) info",
            "rows": 8,
        },
        "active": {"title": "is the application active?"},
    }


#
# NOTICE: custom forms must come above views.
#


class AboutView(BaseView):
    @expose("/")
    def index(self):
        help_about = about
        return self.render("help-about.html", help_about=help_about)


class AccountView(sqla.ModelView):
    def is_accessible(self):
        return current_user.has_role("admin") or current_user.has_role("user")

    @expose("/")
    def index(self):
        kbt = (
            "{0}...".format(pyco_decrypt(current_user.kube_bearer_token)[0:17])
            if hasattr(current_user, "kube_bearer_token")
            and current_user.kube_bearer_token
            and len(current_user.kube_bearer_token) > 0
            else "N/A"
        )
        return self.render("account.html", kbt=kbt)


class ApiAccountView(sqla.ModelView):
    def is_accessible(self):
        is_allowed = False
        if current_user.is_authenticated:
            if current_user.has_role("admin"):
                is_allowed = True
        return is_allowed

    def inaccessible_callback(self, name, **kwargs):
        # redirect to login page if user does not have access.
        # return redirect(url_for("security.login"))
        return redirect("admin/login_ldap")

    def _handle_view(self, name, **kwargs):
        if not self.is_accessible():
            return self.inaccessible_callback(name, **kwargs)
        if current_user.has_role("admin"):
            self.can_delete = True
        else:
            self.can_delete = False

    # can_view_details = True
    # details_modal = True

    column_filters = ["username", "associated_user.name", "active", "note"]

    # default sort by name
    column_default_sort = ("username", False)

    # Don't display the password on the list of Users
    column_exclude_list = "password"

    column_searchable_list = ["username"]

    column_list = ["username", "associated_user", "active", "note"]

    # Don't include the standard password field when creating or editing a User (but see below)
    form_excluded_columns = "password"

    page_size = 20

    # Automatically display human-readable names for the current and available Roles when creating or editing a User
    column_auto_select_related = True

    def _note_formatter(view, context, model, name):
        return Markup("%s" % (model.note)) if model.note else ""

    column_formatters = {"note": _note_formatter}

    form_create_rules = ["username", "password2", "associated_user", "active", "note"]

    # On the form for creating or editing a User, don't display a field corresponding to the model's password field.
    # There are two reasons for this. First, we want to encrypt the password before storing in the database. Second,
    # we want to use a password field (with the input masked) rather than a regular text field.
    def scaffold_form(self):

        # Start with the standard form as provided by Flask-Admin. We've already told Flask-Admin to exclude the
        # password field from this form.
        form_class = super(ApiAccountView, self).scaffold_form()

        # Add a password field, naming it "password2" and labeling it "New Password".
        form_class.password2 = PasswordField("Password")
        return form_class

    # This callback executes when the user saves changes to a newly-created or edited User -- before the changes are
    # committed to the database.
    def on_model_change(self, form, model, is_created):
        logger.debug("Check to set password...")
        if len(form.password2.data):
            logger.debug("Password is present: [{0}]".format(form.password2.data))
            model.password = ApiAccount.hash_password(form.password2.data)

    # ~ def on_model_change(self, form, model, is_created):
    # ~ # If the password field isn't blank...
    # ~ if len(model.password2):
    # ~ model.password = ApiAccount.hash_password(model.password2)


class AppListView(BaseView):
    def is_accessible(self):
        return current_user.has_role("admin") or current_user.has_role("user")

    @expose("/")
    def index(self):
        return self.render(
            "app_list.html", app_list=get_applist(current_user=current_user)
        )


class BuildTemplateView(sqla.ModelView):
    def is_accessible(self):
        is_allowed = False
        if current_user.is_authenticated:
            if current_user.has_role("admin"):
                is_allowed = True
        return is_allowed

    def inaccessible_callback(self, name, **kwargs):
        # redirect to login page if user does not have access.
        # return redirect(url_for("security.login"))
        return redirect("admin/login_ldap")

    def _handle_view(self, name, **kwargs):
        if not self.is_accessible():
            return self.inaccessible_callback(name, **kwargs)
        if current_user.has_role("admin"):
            self.can_delete = True
        else:
            self.can_delete = False

    page_size = 20
    can_view_details = True
    create_modal = False
    edit_modal = False
    details_modal = False
    can_export = True

    form_label = "Namespace"

    column_searchable_list = ["name", "description"]
    column_filters = ["name", "description", "template"]
    column_editable_list = ["name", "description"]
    column_list = ["name", "description"]
    column_exclude_list = ["template"]
    # ~ # sort by name, descending
    column_default_sort = ("name", False)

    form_widget_args = {
        "name": {
            "placeholder": "standard, MZK #1, advanced #1, etc.",
            "title": "the name of the template",
        },
        "description": {
            "placeholder": "standard build script which performs (update source, source_tag, build_image, push_image, generate_response), etc.",
            "title": "description of cluster",
        },
        "template": {
            "placeholder": "add bash script here",
            "title": "the build template (bash script)",
            "rows": 10,
        },
    }


class ClusterNamespaceView(sqla.ModelView):
    def is_accessible(self):
        is_allowed = False
        if current_user.is_authenticated:
            if current_user.has_role("admin"):
                is_allowed = True
        return is_allowed

    def inaccessible_callback(self, name, **kwargs):
        # redirect to login page if user does not have access.
        # return redirect(url_for("security.login"))
        return redirect("admin/login_ldap")

    def _handle_view(self, name, **kwargs):
        if not self.is_accessible():
            return self.inaccessible_callback(name, **kwargs)
        if current_user.has_role("admin"):
            self.can_delete = True
        else:
            self.can_delete = False

    page_size = 20
    can_view_details = False
    create_modal = False
    edit_modal = False
    details_modal = False
    can_export = True

    form_label = "Namespace"

    inline_models = (NamespaceAppForm(NamespaceApp),)
    # inline_models = (NamespaceApp, )

    column_searchable_list = ["name", "description"]
    column_filters = ["name", "description", "active"]
    column_editable_list = ["name", "description", "active"]
    column_list = ["name", "description", "active"]
    column_exclude_list = ["creator"]
    # ~ # sort by name, descending
    column_default_sort = ("name", False)

    form_excluded_columns = "deploy_cluster"

    form_widget_args = {
        "name": {"placeholder": "wwn-kc, MyCluster, etc.", "title": "the cluster name"},
        "description": {
            "placeholder": "WW Norton Kubernetes Cluster, My Cluster for Testing, etc.",
            "title": "description of cluster",
        },
        "active": {"title": "is the cluster active?"},
    }


class ComponentRunDefaultView(sqla.ModelView):
    def is_accessible(self):
        is_allowed = False
        if current_user.is_authenticated and current_user.has_role("admin"):
            is_allowed = True
        return is_allowed

    def inaccessible_callback(self, name, **kwargs):
        # redirect to login page if user does not have access.
        # return redirect(url_for("security.login"))
        return redirect("admin/login_ldap")

    def _handle_view(self, name, **kwargs):
        if not self.is_accessible():
            return self.inaccessible_callback(name, **kwargs)
        if current_user.has_role("admin"):
            self.can_delete = True
        else:
            self.can_delete = False

    page_size = 10
    can_set_page_size = True
    can_view_details = True
    create_modal = True
    edit_modal = True
    details_modal = True
    can_export = True

    column_searchable_list = ["component", "data"]
    column_filters = ["component"]
    column_editable_list = ["component"]
    column_list = ["component", "data"]
    column_default_sort = ("component", False)

    form_excluded_columns = "id"
    form_columns = ["component", "data"]

    column_labels = dict(component="App", data="Data")

    form_create_rules = ["component", "data"]

    form_widget_args = {
        "component": {"placeholder": "enter app name", "title": "name of app"},
        "data": {
            "placeholder": "enter default data (json format) for the app",
            "title": "default data",
        },
    }


class ClusterNamespaceForm(InlineFormAdmin):
    def is_accessible(self):
        is_allowed = False
        if current_user.is_authenticated:
            if current_user.has_role("admin") or current_user.has_role("user"):
                is_allowed = True
        return is_allowed

    def inaccessible_callback(self, name, **kwargs):
        # redirect to login page if user does not have access.
        # return redirect(url_for("security.login"))
        return redirect("admin/login_ldap")

    def _handle_view(self, name, **kwargs):
        if not self.is_accessible():
            return self.inaccessible_callback(name, **kwargs)
        if current_user.has_role("admin"):
            self.can_delete = True
        else:
            self.can_delete = False

    inline_models = (NamespaceAppForm(NamespaceApp),)

    form_columns = ("id", "name", "description", "active")
    form_label = "Namespace"
    column_labels = {"run_metric": "Metric"}

    form_widget_args = {
        "name": {
            "placeholder": "ncia-kdo1, ncia-kdev1, etc.",
            "title": "enter the namespace name",
        },
        "description": {
            "placeholder": "kdo1 environment, kdev1 environment, etc.",
            "title": "enter a description of the environment",
        },
        "active": {"title": "is the namespace active?"},
    }


class StatusView(BaseView):
    def is_accessible(self):
        return current_user.has_role("admin")

    @expose("/")
    def index(self):
        return self.render(
            "status.html",
            builders=get_service_status_group("service_status:builder:*"),
            queue_depth=get_queue_depth(),
            apis=get_service_status_group("service_status:api:*"),
            deployers=get_service_status_group("service_status:deployer:*"),
            wuis=get_service_status_group("service_status:wui:*"),
            maintenances=get_service_status(
                "service_status:maintenance:{0}".format(service_id)
            ),
        )


class StatsBuildsView(BaseView):
    def is_accessible(self):
        return current_user.has_role("admin") or current_user.has_role("user")

    @expose("/")
    def index(self):

        # ~ config = pygalConfig()
        # ~ config.show_legend = True
        # ~ config.human_readable = True
        # ~ config.fill = True
        # ~ config.interpolate = 'cubic'
        # ~ # config.interpolate = 'hermite'
        # ~ # config.interpolation_parameters = {'type': 'cardinal', 'c': .75}
        # ~ config.style = LightGreenStyle
        # ~ #config.width = 300
        # ~ #config.height = 500
        # ~ config.title = 'Hello Chart!'
        # ~ config.x_title = 'time'
        # ~ config.y_title = '# of builds'
        # ~ config.x_labels = ('Item 1', 'Item 2', 'Item 3', 'Item 4', 'Item 5', 'Item 6', 'Item 7')
        # ~ config.x_labels_major = ['This is the first point !', 'This is the fourth point !']
        # ~ config.value_formatter = lambda x: "%.0f" % x
        # ~ config.print_values = False
        # ~ config.dynamic_print_values = True
        # ~ config.value_font_family='googlefont:Raleway'
        # ~ config.value_font_size=30
        # ~ config.value_colors=('white',)
        # ~ #config.tooltip_border_radius = 10
        # ~ config.disable_xml_declaration = True  # if you dont omit xml decl  you will need to decode the rendered data.
        # ~ #config.x_labels_major_every = 3
        # ~ #config.show_minor_x_labels = False
        # ~ #config.logarithmic = True

        # ~ # Examples

        # ~ bchart = pygal.Bar()
        # ~ bchart.add('Fibonacci', [0, 1, 1, 2, 3, 5, 8, 13, 21, 34, 55])
        # ~ # logger.debug('the bar_chart: {0}'.format(bar_chart.render().decode("utf-8")))

        # ~ # chart = pygal.StackedLine(fill=True, interpolate='cubic', style=DefaultStyle) # Setting style here is not necessary
        # ~ # chart = pygal.StackedLine(fill=True, interpolate='cubic', style=LightStyle)
        # ~ # chart = pygal.StackedLine(fill=True, interpolate='cubic', style=CleanStyle)
        # ~ # chart = pygal.StackedLine(fill=True, interpolate='cubic', style=DarkGreenStyle)
        # ~ # chart = pygal.StackedLine(fill=True, interpolate='cubic', style=BlueStyle)
        # ~ # chart = pygal.StackedLine(fill=True, interpolate='cubic', style=LightGreenStyle)
        # ~ # chart = pygal.StackedLine(fill=True, interpolate='cubic', style=LightColorizedStyle)
        # ~ # chart = pygal.StackedLine(fill=True, interpolate='cubic', style=LightSolarizedStyle)
        # ~ # chart = pygal.StackedLine(config)
        # ~ lchart = pygal.Line(config)
        # ~ lchart.add('A', [1, 3,  5, 16, 13, 3,  7])
        # ~ lchart.add('B', [5, 2,  3,  2,  5, 7, 17])
        # ~ lchart.add('C', [6, 10, 9,  7,  3, 1,  0])
        # ~ lchart.add('D', [2,  3, 5,  9, 12, 9,  5])
        # ~ lchart.add('E', [7,  4, 2,  1,  2, 10, 0])

        # ~ # Line date based graph, good for builds over last month???
        # ~ from datetime import datetime, timedelta
        # ~ config.x_label_rotation=20
        # ~ chart = pygal.Line(config)
        # ~ chart.x_labels = map(lambda d: d.strftime('%Y-%m-%d'), [
        # ~ datetime(2013, 1, 2),
        # ~ datetime(2013, 1, 12),
        # ~ datetime(2013, 2, 2),
        # ~ datetime(2013, 2, 22)])
        # ~ chart.add("Visits", [300, 412, 823, 672])
        # ~ # chart.render()

        # ~ # Guage: for showing # of builders
        # ~ gauge = pygal.SolidGauge(inner_radius=0.70)
        # ~ percent_formatter = lambda x: '{:.10g}%'.format(x)
        # ~ dollar_formatter = lambda x: '{:.10g}$'.format(x)
        # ~ gauge.value_formatter = percent_formatter

        # ~ gauge.add('Series 1', [{'value': 225000, 'max_value': 1275000}],
        # ~ formatter=dollar_formatter)
        # ~ gauge.add('Series 2', [{'value': 110, 'max_value': 100}])
        # ~ gauge.add('Series 3', [{'value': 3}])
        # ~ gauge.add(
        # ~ 'Series 4', [
        # ~ {'value': 51, 'max_value': 100},
        # ~ {'value': 12, 'max_value': 100}])
        # ~ gauge.add('Series 5', [{'value': 79, 'max_value': 100}])
        # ~ gauge.add('Series 6', 99)
        # ~ gauge.add('Series 7', [{'value': 100, 'max_value': 100}])
        # ~ #gauge.render().decode("utf-8")

        from app import db

        # builds chart (line)
        chart = pygal.Line(
            fill=True,
            interpolate="cubic",
            style=DefaultStyle,
            human_readable=True,
            print_values=True,
            dynamic_print_values=True,
            tooltip_border_radius=10,
            disable_xml_declaration=True,
        )
        chart.x_label_rotation = 45
        chart.title = "Builds Over Time"
        # query to get build data.
        chart_sql = text(
            """
SELECT to_char(date_trunc('month', created_at), 'YYYY-MM') "dts", count(CASE WHEN queue_status_id=4 THEN 1 END) success, count(CASE WHEN queue_status_id=5 THEN 1 END) fail
FROM queue
group by 1
ORDER BY 1
"""
        )

        chart_result = db.engine.execute(chart_sql)
        chart_labels = []
        chart_success = []
        chart_failure = []
        for row in chart_result:
            # logger.debug('* row: {0}'.format(row))
            chart_labels.append(row[0])
            chart_success.append(row[1])
            chart_failure.append(row[2])
        chart.x_labels = chart_labels
        chart.add("Success", chart_success)
        chart.add("Failure", chart_failure)

        # return self.render('ab-stats.html', chart=chart.render(), gauge=gauge.render().decode("utf-8"), bchart=bchart.render().decode("utf-8"), lchart=lchart.render(), builds=builds.render(), deploys=deploys.render())
        return self.render("ab-stats.html", chart=chart.render())


class StatsBuildsSevenView(BaseView):
    def is_accessible(self):
        return current_user.has_role("admin") or current_user.has_role("user")

    @expose("/")
    def index(self):
        from app import db

        chart = pygal.Line()
        chart.title = "Builds 7"
        chart.fill = True
        chart.interpolate = "cubic"
        chart.style = (
            DefaultStyle
        )  # DefaultStyle, DarkStyle, NeonStyle, DarkSolarizedStyle, LightSolarizedStyle, LightStyle, CleanStyle, RedBlueStyle, DarkColorizedStyle, LightColorizedStyle, TurquoiseStyle, LightGreenStyle, DarkGreenStyle, DarkGreenBlueStyle, BlueStyle
        chart.human_readable = True
        chart.print_values = True
        chart.dynamic_print_values = True
        chart.tooltip_border_radius = 10
        chart.disable_xml_declaration = True
        chart.x_label_rotation = 45

        # query to get deploy data.
        chart_sql = text(
            """
select to_char(date_trunc('day', created_at), 'YYYY-MM-dd') "dts",
    count(CASE WHEN queue_status_id=4 THEN 1 END) success,
    count(CASE WHEN queue_status_id=5 THEN 1 END) fail,
    count(*) total
from queue
where
    queue_status_id in (4,5)
    and created_at > NOW() - interval '7' day
group by 1
order by 1
"""
        )
        chart_result = db.engine.execute(chart_sql)
        chart_labels = []
        chart_success = []
        chart_failure = []
        chart_total = []
        for row in chart_result:
            # logger.debug('* row: {0}'.format(row))
            chart_labels.append(row[0])
            chart_success.append(row[1])
            chart_failure.append(row[2])
            chart_total.append(row[3])
        chart.x_labels = chart_labels
        chart.add("Total", chart_total)
        chart.add("Success", chart_success)
        chart.add("Failure", chart_failure)

        return self.render("ab-stats.html", chart=chart.render())


class StatsBuildSizeSevenView(BaseView):
    def is_accessible(self):
        return current_user.has_role("admin") or current_user.has_role("user")

    @expose("/")
    def index(self):
        from app import db

        chart = pygal.DateTimeLine()
        chart.title = "Build Sizes 7 (in MB)"
        chart.x_label_rotation = 45
        chart.truncate_label = -1
        chart.x_value_formatter = lambda dt: dt.strftime("%Y-%m-%d %H:%M")
        chart.fill = False
        # chart.interpolate = "cubic"
        chart.style = (
            CleanStyle
        )  # DefaultStyle, DarkStyle, NeonStyle, DarkSolarizedStyle, LightSolarizedStyle, LightStyle, CleanStyle, RedBlueStyle, DarkColorizedStyle, LightColorizedStyle, TurquoiseStyle, LightGreenStyle, DarkGreenStyle, DarkGreenBlueStyle, BlueStyle
        chart.human_readable = True
        chart.print_values = True
        chart.dynamic_print_values = True
        chart.tooltip_border_radius = 10

        # query to get deploy data.
        chart_sql = text(
            """
SELECT created_at,
    result->>'component' as app,
    case
        when RIGHT(result->>'image_size', 2) = 'MB'
            then CAST(SUBSTRING(result->>'image_size' from 1 for char_length(result->>'image_size') - 2) as float)
        when RIGHT(result->>'image_size', 2) = 'GB'
            then CAST(SUBSTRING(result->>'image_size' from 1 for char_length(result->>'image_size') - 2) as float) * 1024
        else
            null
    end as build_time
FROM queue
WHERE queue_status_id = 4   --4=success
    and coalesce(result->>'image_size', '') != ''
    AND created_at > NOW() - interval '7' day
ORDER BY 1;
"""
        )
        chart_result = db.engine.execute(chart_sql)
        all_rows = []
        for row in chart_result:
            all_rows.append(row)
        # get the list of apps (e.g. strip out unique 'app' from results)
        app_list = list({v[1]: v[1] for v in all_rows}.values())
        # loop over all apps and add (created_at, build_time) to the chart.
        for app in app_list:
            chart.add(app, [(i[0], i[2]) for i in all_rows if i[1] == app])

        return self.render("ab-stats.html", chart=chart.render().decode("utf-8"))


class StatsBuildTimesSevenView(BaseView):
    def is_accessible(self):
        return current_user.has_role("admin") or current_user.has_role("user")

    @expose("/")
    def index(self):
        from app import db

        chart = pygal.DateTimeLine()
        chart.title = "Build Times 7"
        chart.x_label_rotation = 45
        chart.truncate_label = -1
        chart.x_value_formatter = lambda dt: dt.strftime("%Y-%m-%d %H:%M")
        chart.fill = True
        # chart.interpolate = "cubic"
        chart.style = (
            CleanStyle
        )  # DefaultStyle, DarkStyle, NeonStyle, DarkSolarizedStyle, LightSolarizedStyle, LightStyle, CleanStyle, RedBlueStyle, DarkColorizedStyle, LightColorizedStyle, TurquoiseStyle, LightGreenStyle, DarkGreenStyle, DarkGreenBlueStyle, BlueStyle
        chart.human_readable = True
        chart.print_values = True
        chart.dynamic_print_values = True
        chart.tooltip_border_radius = 10

        # query to get deploy data.
        chart_sql = text(
            """
SELECT created_at,
    result->>'component' as app,
    CAST(coalesce(result->>'build_time', '0') AS integer) as build_time
FROM queue
WHERE queue_status_id = 4
    AND created_at > NOW() - interval '7' day
ORDER BY 1;
"""
        )
        chart_result = db.engine.execute(chart_sql)
        all_rows = []
        for row in chart_result:
            all_rows.append(row)
        # get the list of apps (e.g. strip out unique 'app' from results)
        app_list = list({v[1]: v[1] for v in all_rows}.values())
        # loop over all apps and add (created_at, build_time) to the chart.
        for app in app_list:
            chart.add(app, [(i[0], i[2]) for i in all_rows if i[1] == app])

        return self.render("ab-stats.html", chart=chart.render().decode("utf-8"))


class StatsDeploysView(BaseView):
    def is_accessible(self):
        return current_user.has_role("admin") or current_user.has_role("user")

    @expose("/")
    def index(self):
        from app import db

        chart = pygal.Line(
            fill=True,
            interpolate="cubic",
            style=CleanStyle,
            human_readable=True,
            print_values=True,
            dynamic_print_values=True,
            tooltip_border_radius=10,
            disable_xml_declaration=True,
        )
        chart.x_label_rotation = 45
        chart.title = "Deploys Over Time"
        # query to get deploy data.
        chart_sql = text(
            """
SELECT to_char(date_trunc('month', created_at), 'YYYY-MM') "dts", count(CASE WHEN deployment_status_id=2 THEN 1 END) success, count(CASE WHEN deployment_status_id=3 THEN 1 END) fail
FROM deployment
group by 1
ORDER BY 1
"""
        )
        chart_result = db.engine.execute(chart_sql)
        chart_labels = []
        chart_success = []
        chart_failure = []
        for row in chart_result:
            # logger.debug('* row: {0}'.format(row))
            chart_labels.append(row[0])
            chart_success.append(row[1])
            chart_failure.append(row[2])
        chart.x_labels = chart_labels
        chart.add("Success", chart_success)
        chart.add("Failure", chart_failure)

        return self.render("ab-stats.html", chart=chart.render())


class DeployClusterView(sqla.ModelView):
    def is_accessible(self):
        is_allowed = False
        if current_user.is_authenticated:
            if current_user.has_role("admin"):
                is_allowed = True
        return is_allowed

    def inaccessible_callback(self, name, **kwargs):
        # redirect to login page if user does not have access.
        # return redirect(url_for("security.login"))
        return redirect("admin/login_ldap")

    def _handle_view(self, name, **kwargs):
        if not self.is_accessible():
            return self.inaccessible_callback(name, **kwargs)
        if current_user.has_role("admin"):
            self.can_delete = True
        else:
            self.can_delete = False

    page_size = 20
    can_view_details = False
    create_modal = False
    edit_modal = False
    details_modal = False
    can_export = True

    form_label = "Deploy Cluster"

    inline_models = (ClusterNamespaceForm(ClusterNamespace),)

    column_searchable_list = ["name", "description"]
    column_filters = ["name", "description", "active"]
    column_editable_list = ["name", "description", "active"]
    column_list = ["name", "description", "active"]
    column_exclude_list = ["creator", "created_at"]
    # ~ # sort by name, descending
    column_default_sort = ("name", False)

    form_excluded_columns = ("creator", "created_at")

    form_widget_args = {
        "name": {"placeholder": "wwn-kc, MyCluster, etc.", "title": "the cluster name"},
        "description": {
            "placeholder": "WW Norton Kubernetes Cluster, My Cluster for Testing, etc.",
            "title": "description of cluster",
        },
        "active": {"title": "is the cluster active?"},
    }

    def on_model_change(self, form, model, is_created):
        if is_created:
            model.creator = current_user


class DeploymentView(sqla.ModelView):
    def is_accessible(self):
        is_allowed = False
        if current_user.is_authenticated:
            if current_user.has_role("admin") or current_user.has_role("user"):
                is_allowed = True
        return is_allowed

    def inaccessible_callback(self, name, **kwargs):
        # redirect to login page if user does not have access.
        # return redirect(url_for("security.login"))
        return redirect("admin/login_ldap")

    def _handle_view(self, name, **kwargs):
        if not self.is_accessible():
            return self.inaccessible_callback(name, **kwargs)
        if current_user.has_role("admin"):
            self.can_delete = True
        else:
            self.can_delete = False

    page_size = 10
    can_set_page_size = True
    can_view_details = False
    create_modal = False
    edit_modal = False
    details_modal = False
    can_export = True

    column_searchable_list = ["image_url", "note", "app_name", "namespace"]
    column_filters = [
        "creator.name",
        "deployment_status.name",
        "app_name",
        "namespace",
        "created_at",
        "image_url",
        "note",
        "approver",
    ]
    column_editable_list = []
    column_list = [
        "id",
        "created_at",
        "deployment_status",
        "namespace",
        "app_name",
        "image_url",
        "note",
        "approver",
        "creator",
    ]
    # ~ column_exclude_list = [ 'creator' ]
    # sort by id, desc (latest first)
    column_default_sort = ("id", True)

    def _id_formatter(view, context, model, name):
        return Markup(
            "<a href='{0}&q=did:{1}' title='View logs (in CLS)....'>{1}</a>".format(
                Config.DefaultConfig.CLS_URL, model.id
            )
        )

    def _deployment_status_formatter(view, context, model, name):
        if model.deployment_status is not None:
            return Markup(
                '<div class="inline-tooltip" title="{1}">{0}</div>'.format(
                    model.deployment_status, model.deployment_status_message
                )
            )

    def _created_at_formatter(view, context, model, name):
        if model.created_at is not None:
            return Markup(
                '<div class="inline-tooltip" title="{1}">{0}</div>'.format(
                    arrow.get(model.created_at).to("local").humanize(),
                    arrow.get(model.created_at)
                    .to("local")
                    .format("YYYY-MM-DD HH:mm:ss"),
                )
            )

    def _creator_formatter(view, context, model, name):
        if model.creator is not None:
            return Markup(
                '<div class="inline-tooltip" title="{1}\n{2}">{0}</div>'.format(
                    model.creator.nickname, model.creator.username, model.creator.name
                )
            )

    def _note_formatter(view, context, model, name):
        if model.note is not None:
            return Markup(
                '<div class="inline-tooltip" title="{0}">{1}</div>'.format(
                    model.note,
                    textwrap.shorten(model.note, width=20, placeholder="..."),
                )
            )

    column_formatters = {
        "id": _id_formatter,
        "deployment_status": _deployment_status_formatter,
        "created_at": _created_at_formatter,
        "creator": _creator_formatter,
        "note": _note_formatter,
    }

    form_extra_fields = {
        "NAMESPACE": SelectField("Namespace"),
        "IS_PREPARED": HiddenField("is_prepared"),
    }

    # set order of display for fields in form.
    form_create_rules = [
        "NAMESPACE",
        "app_name",
        "image_url",
        "approver",
        "note",
        "IS_PREPARED",
    ]

    form_excluded_columns = (
        "creator",
        "created_at",
        "namespace",
        "deployment_status",
        "deployment_status_message",
        "is_prepared",
    )

    form_widget_args = {
        "app_name": {
            "placeholder": "application name (e.g. sqlpad, classroom-app, etc.)",
            "title": "enter the application name - this must match the name of the app to be deployed",
        },
        "image_url": {"placeholder": "image to deploy", "title": "image to deploy"},
        "approver": {
            "placeholder": "who approved deployment",
            "title": "the name of the person who approved the deployment",
        },
        "note": {
            "placeholder": "deploying swfb-1234 to kdo1",
            "title": "note/description of deployment",
        },
    }

    column_labels = dict(
        id="DID",
        deployment_status="Status",
        creator="By",
        created_at="Created",
        note="Note",
        namespace="NS",
        app_name="App",
    )

    def on_model_change(self, form, model, is_created):
        if is_created:
            model.creator = current_user
            if form.NAMESPACE.data == Config.DefaultConfig.NS_LIST_DEFAULT_ITEM:
                raise ValidationError("Please select a Namespace from the list.")
            # set the namespace.
            model.namespace = form.NAMESPACE.data
            # NOTICE: the following is temporary code as it should be performed by the deployer.
            namespace = form.NAMESPACE.data
            app_name = form.app_name.data
            image_url = form.image_url.data
            logger.info("deploying image_url: [{0}]".format(image_url))
            logger.debug(
                " image_url - form: {0} | model: {1}".format(
                    form.image_url.data, model.image_url
                )
            )
            # apply patch.
            kbt = pyco_decrypt(current_user.kube_bearer_token)
            configuration = client.Configuration()
            configuration.host = "{0}/k8s/clusters/{1}".format(
                Config.DefaultConfig.KUBE_SERVER_URL,
                Config.DefaultConfig.KUBE_CLUSTER_ID,
            )
            configuration.verify_ssl = False
            configuration.api_key = {"authorization": "Bearer " + kbt}
            client.Configuration.set_default(configuration)
            try:
                v1 = client.ExtensionsV1beta1Api()
                patch = [
                    {
                        "op": "replace",
                        "path": "/spec/template/spec/containers/0/image",
                        "value": image_url,
                    }
                ]
                logger.info(
                    "Patching deployment:\n  - namespace: {0}\n  - application: {1}\n  - patch: {2}".format(
                        namespace, app_name, patch
                    )
                )
                v1.patch_namespaced_deployment(
                    name=app_name, namespace=namespace, body=patch
                )
                v1a = client.AppsV1Api()
                w = watch.Watch()
                for event in w.stream(
                    v1a.list_namespaced_deployment, namespace, timeout_seconds=28
                ):
                    logger.debug(
                        "Deployment: {0}\n  - replicas: {1} (ready: {2})\n  - unavailable: {3}\n  - updated: {4}".format(
                            event["object"].metadata.name,
                            event["object"].status.replicas,
                            event["object"].status.ready_replicas,
                            event["object"].status.unavailable_replicas,
                            event["object"].status.updated_replicas,
                        )
                    )
                    # set deployment status to 'failure' now, if success it will be updated via the watch.
                    model.deployment_status_id = (
                        DeploymentStatus.query.filter_by(name="failure").one().id
                    )
                    model.deployment_status_message = (
                        "Timeout: deployment timed out, check logs for more info."
                    )
                    # stop if deploy all done.
                    #   note: unavailable will be None when all are available
                    if not event["object"].status.unavailable_replicas:
                        logger.debug("All replicas are available, stopping watch...")
                        w.stop()
                        model.deployment_status_id = (
                            DeploymentStatus.query.filter_by(name="success").one().id
                        )
                        model.deployment_status_message = "deployed, available instances: {0}".format(
                            event["object"].status.ready_replicas
                        )
            except ApiException as e:
                logger.debug("Exception: {0}".format(e))
                error_code = str(e)[1:4]
                if error_code == "403":
                    raise ValidationError(
                        "Unauthorized: you are not authorized to deploy '{0}' to '{1}'".format(
                            app_name, namespace
                        )
                    )
                elif error_code == "404":
                    raise ValidationError(
                        "Not Found: it appears '{0}' is not setup in the '{1}' namespace, please notify devops to have the app setup manually".format(
                            app_name, namespace
                        )
                    )
                else:
                    logger.error("Other Error Deploying Build:\n{0}".format(e))
                    flash("Error: {0}".format(e), "error")
                    model.deployment_status_id = (
                        DeploymentStatus.query.filter_by(name="failure").one().id
                    )
                    model.deployment_status_message = str(e)
            # NOTICE: above info will eventually go back to deployer
        else:
            flash(
                "Notice: deploy not performed, deploys are only performed on create requests."
            )

    def create_form(self):
        form = super(DeploymentView, self).create_form()
        ns_list = get_ns_list(current_user=current_user, include_default_item=True)
        logger.debug("ns_list {0}".format(ns_list))
        # if IS_PREPARED then the form has been loaded from queue data.
        # logger.debug('* form.app_name.data=[{0}]'.format(form.app_name.data))
        if "image_url" in request.args and form.IS_PREPARED.data != "yes":
            form.IS_PREPARED.data = "yes"
            form.image_url.data = request.args["image_url"]
            img_url = request.args["image_url"]
            app_si = img_url.find("/", 0, len(img_url)) + 1
            app_se = img_url.find(":", app_si, len(img_url))
            # logger.debug('app_si=[{0}], app_se=[{1}]'.format(app_si, app_se))
            form.app_name.data = img_url[app_si:app_se]
        form.NAMESPACE.choices = [(ns, ns) for ns in sorted(ns_list)]
        return form


class DeploymentStatusView(sqla.ModelView):
    def is_accessible(self):
        is_allowed = False
        if current_user.is_authenticated and current_user.has_role("admin"):
            is_allowed = True
        return is_allowed

    def inaccessible_callback(self, name, **kwargs):
        # redirect to login page if user does not have access.
        # return redirect(url_for("security.login"))
        return redirect("admin/login_ldap")

    def _handle_view(self, name, **kwargs):
        if not self.is_accessible():
            return self.inaccessible_callback(name, **kwargs)
        if current_user.has_role("admin"):
            self.can_delete = True
        else:
            self.can_delete = False

    page_size = 10
    can_set_page_size = True
    can_view_details = True
    create_modal = True
    edit_modal = True
    details_modal = True
    can_export = True

    column_searchable_list = ["name", "desc"]
    column_filters = ["name", "desc"]
    column_editable_list = ["name", "desc"]
    column_list = ["name", "desc"]
    column_default_sort = ("name", False)

    column_labels = dict(name="Name", desc="Desc")

    form_widget_args = {
        "name": {"placeholder": "enter name", "title": "name of the queue status item"},
        "desc": {
            "placeholder": "enter description",
            "title": "description of the queue status item",
        },
    }


class HomeView(AdminIndexView):
    @expose("/")
    def index(self):
        return self.render("home.html")


class LoginMenuLink(MenuLink):
    def is_accessible(self):
        return not current_user.is_authenticated


class LogoutMenuLink(MenuLink):
    def is_accessible(self):
        return current_user.is_authenticated


class NamespaceAppView(sqla.ModelView):
    def is_accessible(self):
        is_allowed = False
        if current_user.is_authenticated:
            if current_user.has_role("admin") or current_user.has_role("user"):
                is_allowed = True
        return is_allowed

    def inaccessible_callback(self, name, **kwargs):
        # redirect to login page if user does not have access.
        # return redirect(url_for("security.login"))
        return redirect("admin/login_ldap")

    def _handle_view(self, name, **kwargs):
        if not self.is_accessible():
            return self.inaccessible_callback(name, **kwargs)
        if current_user.has_role("admin"):
            self.can_delete = True
        else:
            self.can_delete = False

    page_size = 20
    can_view_details = False
    create_modal = False
    edit_modal = False
    details_modal = False
    can_export = True

    column_searchable_list = ["name", "description"]
    column_filters = ["name", "description", "active"]
    column_editable_list = ["name", "description", "active"]
    column_list = ["name", "description", "active"]
    column_exclude_list = ["creator"]
    # ~ # sort by name, descending
    column_default_sort = ("name", False)

    form_excluded_columns = "creator"

    form_widget_args = {
        "name": {"placeholder": "wwn-kc, MyCluster, etc.", "title": "the cluster name"},
        "description": {
            "placeholder": "WW Norton Kubernetes Cluster, My Cluster for Testing, etc.",
            "title": "description of cluster",
        },
        "active": {"title": "is the cluster active?"},
    }


class OnlineHelpView(BaseView):
    @expose("/")
    def index(self):
        return self.render("help-online.html")


class QueueStatusView(sqla.ModelView):
    def is_accessible(self):
        is_allowed = False
        if current_user.is_authenticated and current_user.has_role("admin"):
            is_allowed = True
        return is_allowed

    def inaccessible_callback(self, name, **kwargs):
        # redirect to login page if user does not have access.
        # return redirect(url_for("security.login"))
        return redirect("admin/login_ldap")

    def _handle_view(self, name, **kwargs):
        if not self.is_accessible():
            return self.inaccessible_callback(name, **kwargs)
        if current_user.has_role("admin"):
            self.can_delete = True
        else:
            self.can_delete = False

    page_size = 10
    can_set_page_size = True
    can_view_details = True
    create_modal = True
    edit_modal = True
    details_modal = True
    can_export = True

    column_searchable_list = ["name", "desc"]
    column_filters = ["name", "desc"]
    column_editable_list = ["name", "desc"]
    column_list = ["name", "desc"]
    column_default_sort = ("name", False)

    column_labels = dict(name="Name", desc="Desc")

    form_widget_args = {
        "name": {"placeholder": "enter name", "title": "name of the queue status item"},
        "desc": {
            "placeholder": "enter description",
            "title": "description of the queue status item",
        },
    }


class QueueView(sqla.ModelView):
    def is_accessible(self):
        is_allowed = False
        if current_user.is_authenticated:
            if current_user.has_role("user") or current_user.has_role("admin"):
                is_allowed = True
        return is_allowed

    def inaccessible_callback(self, name, **kwargs):
        # redirect to login page if user does not have access.
        # return redirect(url_for("security.login"))
        return redirect("admin/login_ldap")

    def _handle_view(self, name, **kwargs):
        if not self.is_accessible():
            return self.inaccessible_callback(name, **kwargs)

        if current_user.has_role("admin"):
            self.can_delete = True
        else:
            self.can_delete = False

    page_size = 10
    can_set_page_size = True
    can_view_details = True
    create_modal = False
    edit_modal = False
    details_modal = True
    can_export = True

    column_searchable_list = ["note", "data", "result"]
    column_filters = [
        "id",
        "queue_status.name",
        "creator.name",
        "created_at",
        "tags",
        "note",
        "tags.name",
        "requester",
    ]
    column_editable_list = ["tags"]
    column_list = [
        "id",
        "created_at",
        "queue_status",
        "app_branch",
        "tags",
        "fmt_response",
        "image_size",
        "note",
        "requester",
        "creator",
        "actions",
    ]
    column_default_sort = ("id", True)

    def _data_formatter(view, context, model, name):
        app_name = "n/a"
        repo_branch = ""
        if "APP_NAME" in model.data:
            app_name = model.data["APP_NAME"]
        if "REPO_BRANCH" in model.data:
            repo_branch = " / {0}".format(model.data["REPO_BRANCH"])
        return Markup(
            '<div class="inline-tooltip" title="{2}">{0}{1}</div>'.format(
                app_name, repo_branch, model.data
            )
        )

    def _id_formatter(view, context, model, name):
        return Markup(
            "<a href='{0}&q=qid:{1}' title='View logs (in CLS)....'>{1}</a>".format(
                Config.DefaultConfig.CLS_URL, model.id
            )
        )

    def _created_at_formatter(view, context, model, name):
        if model.created_at is not None:
            return Markup(
                '<div class="inline-tooltip" title="{1}">{0}</div>'.format(
                    arrow.get(model.created_at).to("local").humanize(),
                    arrow.get(model.created_at)
                    .to("local")
                    .format("YYYY-MM-DD HH:mm:ss"),
                )
            )

    def _app_branch_formatter(view, context, model, name):
        app_name = "n/a"
        repo_branch = ""
        data = ""
        if "APP_NAME" in model.data:
            app_name = model.data["APP_NAME"]
        if "REPO_BRANCH" in model.data:
            repo_branch = " / {0}".format(model.data["REPO_BRANCH"])
        if model.data:
            data = model.data
        return Markup(
            '<div class="inline-tooltip" title="{2}">{0}{1}</div>'.format(
                app_name, repo_branch, data
            )
        )

    def _fmt_response_formatter(view, context, model, name):
        if model.result is not None:
            build_success = (
                "success"
                if "build_success" in model.result
                and model.result["build_success"] == "true"
                else ""
            )
            build_msg = (
                "{0}".format(model.result["build_result_msg"])
                if "build_result_msg" in model.result
                else ""
            )
            image_build_performed = (
                "true"
                if "image_build_performed" in model.result
                and model.result["image_build_performed"] == "true"
                else "false"
            )
            image_push_performed = (
                "true"
                if "image_push_performed" in model.result
                and model.result["image_push_performed"] == "true"
                else "false"
            )
            image_push_latest_performed = (
                "true"
                if "image_push_latest_performed" in model.result
                and model.result["image_push_latest_performed"] == "true"
                else "false"
            )
            image_tag_performed = (
                "true"
                if "image_tag_performed" in model.result
                and model.result["image_tag_performed"] == "true"
                else "false"
            )
            image_tag_latest_performed = (
                "true"
                if "image_tag_latest_performed" in model.result
                and model.result["image_tag_latest_performed"] == "true"
                else "false"
            )
            vc_tag_performed = (
                "true"
                if "vc_tag_performed" in model.result
                and model.result["vc_tag_performed"] == "true"
                else "false"
            )
            build_time = (
                " ({0})".format(humanized_time(int(model.result["build_time"])))
                if "build_time" in model.result
                else ""
            )
            return Markup(
                '<div class="inline-tooltip" title="Message: {1}\nBuild: {2}\nPush: {3}\nPush Latest: {4}\nTag: {5}\nTag Latest: {6}\nVC Tag: {7}\n">{0}{8}</div>'.format(
                    build_success,
                    build_msg,
                    image_build_performed,
                    image_push_performed,
                    image_push_latest_performed,
                    image_tag_performed,
                    image_tag_latest_performed,
                    vc_tag_performed,
                    build_time,
                )
            )

    def _image_size_formatter(view, context, model, name):
        image_size = ""
        if model.result is not None:
            image_size = (
                model.result["image_size"] if "image_size" in model.result else ""
            )
        return image_size

    def _queue_status_formatter(view, context, model, name):
        if model.queue_status is not None:
            return Markup(
                '<div class="inline-tooltip" title="{1}">{0}</div>'.format(
                    model.queue_status, model.queue_status.desc
                )
            )

    def _creator_formatter(view, context, model, name):
        if model.creator is not None:
            return Markup(
                '<div class="inline-tooltip" title="{1}\n{2}">{0}</div>'.format(
                    model.creator.nickname, model.creator.username, model.creator.name
                )
            )

    def _note_formatter(view, context, model, name):
        if model.note is not None:
            return Markup(
                '<div class="inline-tooltip" title="{0}">{1}</div>'.format(
                    model.note,
                    textwrap.shorten(model.note, width=20, placeholder="..."),
                )
            )

    def _fmt_actions_formatter(view, context, model, name):
        # need to check to make sure item is complete and good (success)
        if (
            model.result is not None
            and len(model.result) > 0
            and "image_url" in model.result
            and len(model.result["image_url"]) > 0
            and "build_success" in model.result
            and model.result["build_success"] == "true"
        ):
            actions_html = """
<div id="queue-actions">
  <div class="popup" onclick="clipit('{0}', 'clipitPopup_{2}');" title="Copy 'Image URL' to clipboard" id="popupMaster">
    <img id="buildImage" src="../../static/img/build-image.png" height="14px" width="14px" />
    <span class="popuptext" id="clipitPopup_{2}"></span>
  </div>

  <a class="icon" href="{1}" title="Deploy Image...">
    <span class="fa fa-pencil glyphicon icon-upload"></span>
  </a>
</div>
"""
            return Markup(
                actions_html.format(
                    model.result["image_url"],
                    url_for(
                        "deployment.create_view",
                        url=url_for("deployment.index_view"),
                        image_url=model.result["image_url"],
                    ),
                    model.id,
                )
            )

    form_extra_fields = {
        "APP_NAME": SelectField("App"),
        "REPO_BRANCH": TextField("Branch", default="master"),
        # 'CLEAN_SOURCE': BooleanField('Clean Source'),
        "RUN_BUILD": BooleanField("Build", default=True),
        "TAG_IMAGE_LATEST": BooleanField("Tag Latest", default=False),
        "PUSH_IMAGE_ENABLED": BooleanField("Push Image", default=True),
        "VC_TAG_ENABLED": BooleanField("Tag Source", default=True),
        "VC_TAG": TextField("Source Tag"),
        "SW5_APP_MODULE_DEPLOYS_ENV": TextField("SW5 Modules Env"),
        # 'ADDITIONAL_PARAMS': TextAreaField('Additional Params'),
    }

    form_create_rules = [
        "APP_NAME",
        "REPO_BRANCH",
        # 'CLEAN_SOURCE',
        "RUN_BUILD",
        "TAG_IMAGE_LATEST",
        "PUSH_IMAGE_ENABLED",
        "VC_TAG_ENABLED",
        "VC_TAG",
        "SW5_APP_MODULE_DEPLOYS_ENV",
        "requester",
        "note",
        "tags",
        "data",
    ]

    form_args = {
        "created_at": {
            "format": "%Y-%m-%d %H:%M:%S"  # changes how the input is parsed by strptime (e.g. 2017-07-22 11:47:58).
        }
    }

    column_formatters = {
        "id": _id_formatter,
        "created_at": _created_at_formatter,
        "app_branch": _app_branch_formatter,
        "fmt_response": _fmt_response_formatter,
        "image_size": _image_size_formatter,
        "actions": _fmt_actions_formatter,
        "queue_status": _queue_status_formatter,
        "creator": _creator_formatter,
        "note": _note_formatter,
    }
    #
    # NOTICE: readonly does not appear to apply to combo boxes, disabled seems to Empty the values so
    #         we simply hide the columns from the form to achieve the same effect.
    form_widget_args = {
        "APP_NAME": {
            "placeholder": "classroom-app, dlp-app, sw5-app, sw5-ws",
            "title": "the application to build",
        },
        "REPO_BRANCH": {
            "placeholder": "master, sprint5, feature/swfb-1234, etc.",
            "title": "the branch to use for the build",
        },
        "note": {
            "placeholder": "build for swfb-1234, my first build, etc.",
            "title": "note(s) about the build (for documentational purposes only)",
        },
        "tags": {
            "placeholder": "select tag(s) for the build",
            "title": "a set of tags for the build (for documentational purposes only)",
        },
        "RUN_BUILD": {
            "title": "perform build; if checked, the application will be built producing an image of the application"
        },
        # ~ 'CLEAN_SOURCE': {
        # ~ 'title': 'clean build source; if checked, the build source will be cleaned (deleted) before building',
        # ~ },
        "TAG_IMAGE_LATEST": {
            "title": "tag image latest; if checked, the resulting image will have the 'latest' tag added"
        },
        "PUSH_IMAGE_ENABLED": {
            "title": "push image; if checked, the built image will be pushed to the image repository"
        },
        "VC_TAG_ENABLED": {
            "title": "source tag; if checked, a tag will be applied to the source from where the image was built (e.g. git tag added to mark where build came from)"
        },
        "VC_TAG": {
            "placeholder": "v3_build, my_tag, etc (leave blank to use auto-generated tag)",
            "title": "tag to apply to source if 'Tag Source' is checked (note: leave blank to use auto-generated tag (which is a RUNSTAMP))",
        },
        "ADDITIONAL_PARAMS": {
            "placeholder": '{\n  "SW5_APP_MODULE_DEPLOYS_ENABLED": "true",\n  "SW5_APP_MODULE_DEPLOYS_ENV": "kuat1",\n  "SW5_APP_PATCH_SERVER_URL": "34.193.76.59"\n}',
            "title": "additional parameters to send with the request",
            "rows": 5,
        },
        "SW5_APP_MODULE_DEPLOYS_ENV": {
            "placeholder": "stg, slt, uat, qa , int, mnt, ncia-docker-dev1, ncia-docker-dev2, ncia-docker-qa, kqa2, kuat1, etc. ",
            "title": "SW5 app modules deploy environment to use for the build (leave blank unless building sw5-app)",
        },
        "requester": {
            "placeholder": "who requested build",
            "title": "the name of the person who requested the build",
        },
        "creator": {
            "readonly": True,
            # 'disabled': True,
        },
        "created_at": {
            "readonly": True,
            "disabled": True,
            "data-date-format": u"YYYY-MM-DD HH:mm:ss",  # changes how the DateTimeField displays the time
        },
        "data": {
            "readonly": True,
            "disabled": True,
            "rows": 1,
            "style": "height: 18px",
        },
    }
    column_labels = dict(
        id="QID",
        queue_status="Status",
        creator="By",
        created_at="Created",
        data="Request",
        response="Response",
        app_branch="Request",
        fmt_response="Result",
        image_size="Img Size",
        note="Note",
        tags="Tags",
        actions="",
    )

    def create_form(self):
        form = super(QueueView, self).create_form()
        app_list = get_user_app_list(
            current_user=current_user, include_default_item=True
        )
        logger.debug("user_app_list is: {0}".format(app_list))

        # iterate through the namespaces and get all apps.
        # form.APP_NAME.choices = [(app['app_name'],'{0}:{1}'.format(app['namespace'],app['app_name'])) for app in app_list]
        form.APP_NAME.choices = [(app, app) for app in sorted(app_list)]
        return form

    def edit_form(self, obj):
        form = super(QueueView, self).edit_form(obj)
        app_list = get_user_app_list(
            current_user=current_user, include_default_item=True
        )
        logger.debug("user_app_list is: {0}".format(app_list))
        form.APP_NAME.choices = [(app, app) for app in sorted(app_list)]
        return form

    def validate_form(self, form):
        if form and "APP_NAME" in form:
            if (
                form.APP_NAME.data == ""
                or form.APP_NAME.data == Config.DefaultConfig.APP_LIST_DEFAULT_ITEM
            ):
                logger.debug(
                    "APP_NAME not selected (default value) or not provided (empty), value: [{0}]".format(
                        form.APP_NAME
                    )
                )
                flash(
                    "Invalid App name (default value) or not provided (empty). You must select an App from the list."
                )
                return False
        return super(QueueView, self).validate_form(form)

    def on_model_change(self, form, model, is_created):
        if is_created:
            model.creator = current_user
            # NOTE: the builder 'enrichment' process adds additional info to the request on build
            request_data = {
                "APP_NAME": form.APP_NAME.data,
                "REPO_BRANCH": form.REPO_BRANCH.data,
                "RUN_BUILD": str(form.RUN_BUILD.data).lower(),
                # "CLEAN_SOURCE": 'false',
                "TAG_IMAGE_LATEST": str(form.TAG_IMAGE_LATEST.data).lower(),
                "PUSH_IMAGE_ENABLED": str(form.PUSH_IMAGE_ENABLED.data).lower(),
                "VC_TAG_ENABLED": str(form.VC_TAG_ENABLED.data).lower(),
                "VC_TAG": form.VC_TAG.data,
            }
            if len(form.SW5_APP_MODULE_DEPLOYS_ENV.data) > 0:
                request_data[
                    "SW5_APP_MODULE_DEPLOYS_ENV"
                ] = form.SW5_APP_MODULE_DEPLOYS_ENV.data
            model.data = request_data
            logger.info("model.data is: {0}".format(model.data))

    # create of model from form.
    def create_model(self, form):
        form.data.data = {}
        super(QueueView, self).create_model(form)
        return True


# Customized Role model for SQL-Admin
class RoleAdmin(sqla.ModelView):
    def is_accessible(self):
        is_allowed = False
        if current_user.is_authenticated and current_user.has_role("admin"):
            is_allowed = True
        return is_allowed

    def inaccessible_callback(self, name, **kwargs):
        # redirect to login page if user does not have access.
        # return redirect(url_for("security.login"))
        return redirect("admin/login_ldap")

    def _handle_view(self, name, **kwargs):
        if not self.is_accessible():
            return self.inaccessible_callback(name, **kwargs)
        if current_user.has_role("admin"):
            self.can_delete = True
        else:
            self.can_delete = False


class SystemConfigView(sqla.ModelView):
    def is_accessible(self):
        return current_user.has_role("admin")

    @expose("/")
    def index(self):
        title = "System Config"
        return self.render("system-config.html", title=title)

    @expose("/flush-applist-cache")
    def flush_applist_cache(self):
        title = "System Config"
        ret = delete_all_cached_user_applists()
        if ret["success"]:
            flash("AppList Cache Flushed!")
        else:
            flash(ret["message"], "error")
        return self.render("system-config.html", title=title)

    @expose("/flush-nslist-cache")
    def flush_nslist_cache(self):
        title = "System Config"
        ret = delete_all_cached_user_nslists()
        if ret["success"]:
            flash("NamespaceList Cache Flushed!")
        else:
            flash(ret["message"], "error")
        return self.render("system-config.html", title=title)


class TagView(sqla.ModelView):
    def is_accessible(self):
        is_allowed = False
        if current_user.is_authenticated:
            if current_user.has_role("admin") or current_user.has_role("user"):
                is_allowed = True
        return is_allowed

    def inaccessible_callback(self, name, **kwargs):
        # redirect to login page if user does not have access.
        # return redirect(url_for("security.login"))
        return redirect("admin/login_ldap")

    def _handle_view(self, name, **kwargs):
        if not self.is_accessible():
            return self.inaccessible_callback(name, **kwargs)
        if current_user.has_role("admin"):
            self.can_delete = True
        else:
            self.can_delete = False

    page_size = 20
    can_view_details = False
    create_modal = True
    edit_modal = True
    # details_modal = True
    can_export = True

    column_searchable_list = ["name"]
    column_filters = ["name"]
    column_editable_list = ["name"]
    column_list = ["name"]
    column_exclude_list = ["creator", "queue", "s2id_autogen4"]
    # sort by name, descending
    column_default_sort = ("name", False)

    form_excluded_columns = ("creator", "queue")

    form_widget_args = {"name": {"placeholder": "tag name"}}

    def on_model_change(self, form, model, is_created):

        if is_created:
            # model.created_by = current_user.id
            model.creator = current_user


# Customized User model for SQL-Admin
class UserAdmin(sqla.ModelView):
    def is_accessible(self):
        # logger.debug('current user id/query_string id: {0}/{1}'.format(current_user.id, request.args.get('id')))
        is_allowed = False
        if current_user.is_authenticated:
            if current_user.has_role("admin"):
                is_allowed = True
            elif str(current_user.id) == str(request.args.get("id")):
                is_allowed = True
            else:
                is_allowed = False
        else:
            logger.debug("user not allowed (not authenticated)")
        # logger.debug('returning: {0}'.format(is_allowed))
        return is_allowed

    def inaccessible_callback(self, name, **kwargs):
        # redirect to login page if user does not have access.
        # return redirect(url_for("security.login"))
        return redirect("admin/login_ldap")

    def _handle_view(self, name, **kwargs):
        if not self.is_accessible():
            return self.inaccessible_callback(name, **kwargs)
        if current_user.has_role("admin"):
            self.can_delete = True
        else:
            self.can_delete = False

    # can_view_details = True
    # details_modal = True

    # default sort by name
    column_default_sort = ("name", False)

    # Don't display the password on the list of Users
    column_exclude_list = ("password", "kube_bearer_token")

    # Don't include the standard password field when creating or editing a User (but see below)
    form_excluded_columns = ("password", "kube_bearer_token")

    page_size = 20

    # Automatically display human-readable names for the current and available Roles when creating or editing a User
    column_auto_select_related = True

    # On the form for creating or editing a User, don't display a field corresponding to the model's password field.
    # There are two reasons for this. First, we want to encrypt the password before storing in the database. Second,
    # we want to use a password field (with the input masked) rather than a regular text field.
    def scaffold_form(self):

        # Start with the standard form as provided by Flask-Admin. We've already told Flask-Admin to exclude the
        # password field from this form.
        form_class = super(UserAdmin, self).scaffold_form()

        # Add a password field, naming it "password2" and labeling it "New Password".
        form_class.password2 = PasswordField("New Password")
        form_class.kube_bearer_token2 = PasswordField("Kube Bearer Token")
        return form_class

    # This callback executes when the user saves changes to a newly-created or edited User -- before the changes are
    # committed to the database.
    def on_model_change(self, form, model, is_created):
        # If the password field isn't blank...
        if len(model.password2):
            model.password = utils.encrypt_password(model.password2)
        if len(model.kube_bearer_token2):
            model.kube_bearer_token = pyco_encrypt(model.kube_bearer_token2)


# Customized User model for users
class UserAdminUser(sqla.ModelView):
    def is_accessible(self):
        # logger.debug('current user id/query_string id: {0}/{1}'.format(current_user.id, request.args.get('id')))
        is_allowed = False
        if current_user.is_authenticated:
            if current_user.has_role("admin"):
                is_allowed = True
            elif str(current_user.id) == str(request.args.get("id")):
                is_allowed = True
            else:
                is_allowed = False
        else:
            logger.debug("user not allowed (not authenticated)")
        # logger.debug('returning: {0}'.format(is_allowed))
        return is_allowed

    def inaccessible_callback(self, name, **kwargs):
        # redirect to login page if user does not have access.
        # return redirect(url_for("security.login"))
        return redirect("admin/login_ldap")

    def _handle_view(self, name, **kwargs):
        if not self.is_accessible():
            return self.inaccessible_callback(name, **kwargs)
        if current_user.has_role("admin"):
            self.can_delete = True
        else:
            self.can_delete = False

    # can_view_details = True
    # details_modal = True

    # default sort by name
    column_default_sort = ("name", False)

    # Don't display the password on the list of Users
    column_exclude_list = ("password", "kube_bearer_token")

    # Don't include the standard password field when creating or editing a User (but see below)
    form_excluded_columns = ("password", "kube_bearer_token", "roles")

    page_size = 20

    # Automatically display human-readable names for the current and available Roles when creating or editing a User
    column_auto_select_related = True

    form_widget_args = {
        "nickname": {
            "placeholder": "enter short (2-3 character) nickname",
            "title": "nickname (max 5 characters), should be initial or something short",
        }
    }
    column_labels = dict(nickname="Nickname")

    # On the form for creating or editing a User, don't display a field corresponding to the model's password field.
    # There are two reasons for this. First, we want to encrypt the password before storing in the database. Second,
    # we want to use a password field (with the input masked) rather than a regular text field.
    def scaffold_form(self):

        # Start with the standard form as provided by Flask-Admin. We've already told Flask-Admin to exclude the
        # password field from this form.
        form_class = super(UserAdminUser, self).scaffold_form()

        # Add a password field, naming it "password2" and labeling it "New Password".
        form_class.password2 = PasswordField("New Password")
        form_class.kube_bearer_token2 = PasswordField("Kube Bearer Token")
        return form_class

    # This callback executes when the user saves changes to a newly-created or edited User -- before the changes are
    # committed to the database.
    def on_model_change(self, form, model, is_created):
        # If the password field isn't blank...
        if len(model.password2):
            model.password = utils.encrypt_password(model.password2)
        if len(model.kube_bearer_token2):
            model.kube_bearer_token = pyco_encrypt(model.kube_bearer_token2)
