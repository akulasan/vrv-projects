import base64
import logging
import redis
import arrow
import datetime
import json

from dateutil.relativedelta import relativedelta
from time import sleep
from Crypto.Cipher import AES
from kubernetes import client
from kubernetes.client.rest import ApiException

from requests_futures.sessions import FuturesSession

# locals
import config as Config

logger = logging.getLogger("main_logger")
rc = redis.StrictRedis(
    host=Config.DefaultConfig.CACHE_HOST,
    port=Config.DefaultConfig.CACHE_PORT,
    db=Config.DefaultConfig.CACHE_DB,
    charset="utf-8",
    decode_responses=True,
)


class ServiceStatus(object):
    def __init__(self, name, checker, service_id, status, updated, message, version):
        self.name = name
        self.checker = checker
        self.service_id = service_id
        self.status = status
        self.message = message
        self.updated = updated
        self.version = version

    def to_json(self):
        return json.dumps(self, default=lambda o: o.__dict__)


class AppListApp(object):
    def __init__(
        self,
        name,
        ready,
        status,
        status_message,
        status_reason,
        restarts,
        age,
        age_creation_timestamp,
        image,
    ):
        self.name = name
        self.ready = ready
        self.status = status
        self.status_message = status_message
        self.status_reason = status_reason
        self.restarts = restarts
        self.age = age
        self.age_creation_timestamp = age_creation_timestamp
        self.image = image


class AppListNamespace(object):
    def __init__(self, namespace, applist):
        self.namespace = namespace
        self.applist = applist


class PodLogRequest(object):
    def __init__(self, namespace, name, container, follow, pretty, previous):
        self.namespace = namespace
        self.name = name
        self.container = container
        self.follow = follow
        self.pretty = pretty
        self.previous = previous


class PodRequest(object):
    def __init__(self, namespace, name, pretty):
        self.namespace = namespace
        self.name = name
        self.pretty = pretty


class PrefixMiddleware(object):
    def __init__(self, app, prefix=""):
        self.app = app
        self.prefix = prefix

    def __call__(self, environ, start_response):
        try:
            if environ["PATH_INFO"].startswith(self.prefix):
                environ["PATH_INFO"] = environ["PATH_INFO"][len(self.prefix) :]
                environ["SCRIPT_NAME"] = self.prefix
                return self.app(environ, start_response)
            else:
                start_response("404", [("Content-Type", "text/plain")])
                return ["This url does not belong to the app.".encode()]
        except Exception as e:
            logger.debug("An error occurred in adjusting the path:\n{0}".format(e))
            start_response("500", [("Content-Type", "text/plain")])
            return ["Path Error: {0}".format(e).encode()]


def get_rks_session(username=None, upw=None):
    """Log into rks as the user.
    """
    logger.debug("get rks session for user: {0}".format(username))
    # example curl command:
    # LOGINRESPONSE=`curl -s 'https://rancher-tst.wwnorton.com/v3-public/localProviders/local?action=login' -H 'content-type: application/json' --data-binary '{"username":"theUsername","password":"theUserPassword"}' --insecure`
    headers = {"content-type": "application/json"}
    data = '"username":"{0}","password":"{1}"'.format(username, upw)
    session = FuturesSession(max_workers=1)
    # localProvider route is if you are using rancher's 'internal' auth.
    # ~ loginRoute = 'v3-public/localProviders/local?action=login'
    loginRoute = "v3-public/openLdapProviders/openldap?action=login"
    postUrl = "{0}/{1}".format(Config.DefaultConfig.KUBE_SERVER_URL, loginRoute)
    logger.debug("postUrl: {0}".format(postUrl))
    future = session.post(postUrl, json=json.loads("{" + data + "}"), headers=headers)
    response = future.result()
    logger.debug(
        "login response - code: {0}, text: {1}".format(
            response.status_code, response.text
        )
    )
    respj = json.loads(response.text)
    token = None
    if respj and "token" in respj:
        token = respj["token"]
    else:
        logger.error("No Token Found, cannot continue!")

    logger.debug("Your rks session token is: {0}".format(token))
    return token


def get_kbt(rks_session_token=None):
    """Use rks_session_token to create user kbt
    """
    logger.debug("get kbt token")
    # example curl command:
    # APIRESPONSE=`curl -s 'https://rancher-tst.wwnorton.com/v3/token' -H 'content-type: application/json' -H "Authorization: Bearer $LOGINTOKEN" --data-binary '{"type":"token","description":"ab automation token"}' --insecure`
    # get api token.
    # APITOKEN=`echo $APIRESPONSE | jq -r .token`
    headers = {
        "content-type": "application/json",
        "Authorization": "Bearer {0}".format(rks_session_token),
    }
    data = (
        '"type":"token","description":"ab automation token created by ab login process"'
    )
    session = FuturesSession(max_workers=1)
    loginRoute = "v3/token"
    postUrl = "{0}/{1}".format(Config.DefaultConfig.KUBE_SERVER_URL, loginRoute)
    logger.debug("postUrl: {0}".format(postUrl))
    future = session.post(postUrl, json=json.loads("{" + data + "}"), headers=headers)
    response = future.result()
    logger.debug(
        "login response - code: {0}, text: {1}".format(
            response.status_code, response.text
        )
    )
    respj = json.loads(response.text)
    token = None
    if respj and "token" in respj:
        token = respj["token"]
    else:
        logger.error("No Token Found, cannot continue!")

    # logger.debug("Created new KBT for user of: {0}".format(token))
    return token


def get_kube_bearer_token(user=None, upw=None):
    """Get the user's kbt.
    """
    logger.debug("Getting user KBT")
    if user and user.kube_bearer_token:
        logger.debug("User already has KBT")
        kbt = user.kube_bearer_token
        kbt_raw = pyco_decrypt(kbt)
    else:
        logger.debug("User does not have KBT, creating a new one")
        # get rks session so we can create a new kbt
        rks_session_token = get_rks_session(username=user.username, upw=upw)
        # use rks_session_token to create a new kbt
        kbt_raw = get_kbt(rks_session_token=rks_session_token)
        kbt = pyco_encrypt(kbt_raw)
        # NOTE: we dont need to save it as it is returned to a process which does the saving of it.
    logger.debug("User's KBT is: [{0}/{1}]".format(kbt_raw, kbt))
    return kbt


def pyco_encrypt(message):
    """Encrypt message.
    - takes: plaintext
    - returns: cipher (encrypted message)
    """
    obj = AES.new(
        Config.DefaultConfig.CRYPTO_KBT_KEY,
        AES.MODE_CFB,
        Config.DefaultConfig.CRYPTO_KBT_IV,
    )
    return base64.urlsafe_b64encode(obj.encrypt(message)).decode()


def pyco_decrypt(cipher):
    """Decrypt an encrypted message.
    - takes: cipher (encrypted message)
    - returns: plaintext (byte decoded)
    """
    obj2 = AES.new(
        Config.DefaultConfig.CRYPTO_KBT_KEY,
        AES.MODE_CFB,
        Config.DefaultConfig.CRYPTO_KBT_IV,
    )
    return obj2.decrypt(base64.urlsafe_b64decode(cipher)).decode()


def get_ns_list(current_user=None, include_default_item=True):
    """Get the cluster Namespaces (NS) list (from redis). If the ns list
    does not exist then it is retrieve from kubernetes.
    - takes: include_default_item (optional) - flag indicating if the
      default item should be added to the list.
    - returns: list of namespaces (filtered to remove Excluded Namespaces)
"""
    # check redis for list.
    key = "ns_list_{0}".format(current_user.username)
    ns_list = rc.lrange(key, 0, -1)
    logger.debug("ns_list is: {0}".format(ns_list))
    # if no ns list then get it from cluster.
    if not ns_list:
        configuration = client.Configuration()
        configuration.host = "{0}/k8s/clusters/{1}".format(
            Config.DefaultConfig.KUBE_SERVER_URL, Config.DefaultConfig.KUBE_CLUSTER_ID
        )
        # configuration.verify_ssl = False
        configuration.api_key = {"authorization": "Bearer " + get_app_admin_kbt()}
        client.Configuration.set_default(configuration)
        ns_list = (
            [Config.DefaultConfig.NS_LIST_DEFAULT_ITEM] if include_default_item else []
        )
        try:
            v1 = client.CoreV1Api()
            ret_list = v1.list_namespace(pretty=True).to_dict()
            # logger.debug('list_namespace returned: {0}'.format(ret_list))
            for item in ret_list["items"]:
                # NOTICE: do not store 'excluded' namespaces
                if (
                    not item["metadata"]["name"]
                    in Config.DefaultConfig.EXCLUDED_NAMESPACES
                ):
                    ns = item["metadata"]["name"]
                    # logger.debug('adding item to keep list: {0}'.format(ns))
                    # check to see if user has access to this ns and also get all apps of the ns for the app_list.
                    # note: we dont care too much about the pods returned, just that the user can return some as this means
                    #       they have access to the ns.
                    try:
                        configuration.api_key = {
                            "authorization": "Bearer "
                            + pyco_decrypt(current_user.kube_bearer_token)
                        }
                        client.Configuration.set_default(configuration)
                        v1_ua = client.CoreV1Api()
                        # ~ pod_ret_list = v1_ua.list_namespaced_pod(ns)
                        # NOTE: call list_namespaced_pod, if no error we add the ns to the list, otherwise the exception will skip it.
                        v1_ua.list_namespaced_pod(ns)
                        ns_list.append(ns)
                    except ApiException as e:
                        error_code = str(e)[1:4]
                        if error_code == "403":
                            # user does not have access to this ns, dont add it to ns_list.
                            # logger.debug('user does not have access to this ns, skipping add to list: {0}'.format(ns))
                            pass
                        else:
                            logger.error("Other Error Deploying Build:\n{0}".format(e))

        except ApiException as e:
            # this will happen if the user is unauthorized; return will be an empty list.
            pass

        # logger.debug('writing ns_list of: {0}'.format(ns_list))
        # write ns_list to redis
        rc.lpush(key, *ns_list)
        # set an expire on the list.
        rc.expire(key, Config.DefaultConfig.CACHE_NS_LIST_TTL)

    return ns_list


def get_app_admin_kbt():
    """Get the app_admin's kbt: get it from redis, if it does not exist
    in redis create a new one and save it in redis for future use.
    """
    logger.debug("Getting app_admin's KBT")
    enc_kbt = rc.get("APP_ADMIN_KBT_ENCRYPTED")
    if enc_kbt:
        logger.debug("APP_ADMIN_KBT found in redis: {0} (encrypted)".format(enc_kbt))
    else:
        logger.debug("APP_ADMIN_KBT not found in redis, creating new one")
        enc_kbt = create_app_admin_kbt()
        logger.debug("APP_ADMIN_KBT created: {0} (encrypted)".format(enc_kbt))
    return pyco_decrypt(enc_kbt)


def create_app_admin_kbt():
    """Create app_admin's kbt.
    """
    logger.debug("Creating APP_ADMIN_KBT")
    rks_session_token = get_rks_session(
        Config.DefaultConfig.LDAP_APP_ADMIN_KBT_USERNAME,
        Config.DefaultConfig.LDAP_APP_ADMIN_KBT_PASSWORD,
    )
    app_admin_kbt = get_kbt(rks_session_token=rks_session_token)
    key = "APP_ADMIN_KBT_ENCRYPTED"
    val = pyco_encrypt(app_admin_kbt)
    rc.set(key, val)
    # note: we do not set a TTL on this key as we want it to stay indefinitely.
    return val


def delete_all_cached_user_applists():
    """Delete all cached user applists
    flush applist cache by deleting all keys that start with app_list_*
    """
    logger.debug("Deleting all user applists from cache")
    try:
        for key in rc.keys("app_list_*"):
            rc.delete(key)
    except Exception as e:
        err_msg = "Error occurred in delete all cached user applists: {0}".format(e)
        logger.error(err_msg)
        return {"success": False, "message": err_msg}
    return {"success": True, "message": "all user applists cleared from cache"}


def delete_all_cached_user_nslists():
    """Delete all cached user nslists
    flush nslist cache by deleting all keys that start with ns_list_*
    """
    logger.debug("Deleting all user nslists from cache")
    try:
        for key in rc.keys("ns_list_*"):
            rc.delete(key)
    except Exception as e:
        err_msg = "Error occurred in delete all cached user nslists: {0}".format(e)
        logger.error(err_msg)
        return {"success": False, "message": err_msg}
    return {"success": True, "message": "all user nslists cleared from cache"}


def get_user_app_list(current_user=None, include_default_item=True):
    """Get the user's app_list: get the current user's app_list from
    redis (if exists) else get it from kubernetes.
    - format: list of objects: { namespace: 'default', app_name: 'my app' }
    """
    key = "app_list_{0}".format(current_user.username)
    app_list = rc.lrange(key, 0, -1)
    key_ns_list = "ns_list_{0}".format(current_user.username)
    ns_list = rc.lrange(key_ns_list, 0, -1)

    # if app_list is empty then get it from kubernetes
    if not app_list:
        configuration = client.Configuration()
        configuration.host = "{0}/k8s/clusters/{1}".format(
            Config.DefaultConfig.KUBE_SERVER_URL, Config.DefaultConfig.KUBE_CLUSTER_ID
        )
        # no longer needed as we now have a valid ssl cert.
        # configuration.verify_ssl = False
        configuration.api_key = {"authorization": "Bearer " + get_app_admin_kbt()}
        logger.debug(
            "KUB Client Configuration\n- host: {0}\n- api_key: {1}".format(
                configuration.host, configuration.api_key
            )
        )
        client.Configuration.set_default(configuration)
        # app_list = [ { 'namespace': Config.DefaultConfig.APP_LIST_DEFAULT_ITEM, 'app_name': Config.DefaultConfig.APP_LIST_DEFAULT_ITEM }] if include_default_item else []
        app_list = (
            [Config.DefaultConfig.APP_LIST_DEFAULT_ITEM] if include_default_item else []
        )
        try:
            logger.debug("Getting namespaces...")
            v1 = client.CoreV1Api()
            ret_list = v1.list_namespace(pretty=True).to_dict()
            logger.debug("got namespace list...")
            ns_list_new = []
            if ret_list and "items" in ret_list:
                for item in ret_list["items"]:
                    # NOTICE: do not store 'excluded' namespaces
                    if (
                        not item["metadata"]["name"]
                        in Config.DefaultConfig.EXCLUDED_NAMESPACES
                    ):
                        ns = item["metadata"]["name"]
                        logger.debug(
                            "- checking user access to namespace: {0}".format(ns)
                        )
                        # check to see if user has access to this ns and also get all apps of the ns for the app_list.
                        try:
                            configuration.api_key = {
                                "authorization": "Bearer "
                                + pyco_decrypt(current_user.kube_bearer_token)
                            }
                            logger.debug(
                                "- using user token of: {0}".format(
                                    configuration.api_key
                                )
                            )
                            client.Configuration.set_default(configuration)
                            v1_ua = client.CoreV1Api()
                            pod_ret_list = v1_ua.list_namespaced_pod(ns)
                            for pod in pod_ret_list.items:
                                # NOTICE: we really do not need ns:app_name so we are just storing app_name for now.
                                # app_list.append( { 'namespace': ns, 'app_name': pod.spec.containers[0].name } )
                                app_list.append(pod.spec.containers[0].name)
                            # add ns to ns_list.
                            ns_list_new.append(ns)
                        except ApiException as e:
                            error_code = str(e)[1:4]
                            if error_code == "403":
                                # user does not have access to this ns, dont add it to ns_list.
                                logger.debug(
                                    "User does not have access to namespace: {0}".format(
                                        e
                                    )
                                )
                                pass
                            if error_code == "401":
                                # unauthorized!
                                logger.debug("User is not authorized: {0}".format(e))
                                pass
                            else:
                                logger.error(
                                    "Other Error in getting App List: \n{0}".format(e)
                                )
                # remove duplicates from list.
                app_list = list(set(app_list))
                # set app_list.
                rc.lpush(key, *app_list)
                rc.expire(key, Config.DefaultConfig.CACHE_APP_LIST_TTL)

                # set ns_list if it is empty.
                if not ns_list:
                    rc.lpush(key_ns_list, *ns_list_new)
                    rc.expire(key_ns_list, Config.DefaultConfig.CACHE_NS_LIST_TTL)
            else:
                # namespace list is empty.
                logger.debug(
                    "User is not authorized for any Namespaces (namespace list empty)"
                )

        except ApiException as e:
            # this will happen if the user is unauthorized; return will be an empty list.
            logger.error("ApiException happened getting namespaces: {1}".format(e))
            pass
        except Exception as e:
            logger.error("Exception happened getting namespaces: {1}".format(e))

    return app_list


def get_ns_applist_apps(current_user=None, namespace=None):
    """Get applist apps for a namespace.
    - return: list of apps
    """
    applist_apps = []

    configuration = client.Configuration()
    configuration.host = "{0}/k8s/clusters/{1}".format(
        Config.DefaultConfig.KUBE_SERVER_URL, Config.DefaultConfig.KUBE_CLUSTER_ID
    )
    try:
        configuration.api_key = {
            "authorization": "Bearer " + pyco_decrypt(current_user.kube_bearer_token)
        }
        client.Configuration.set_default(configuration)
        v1_ua = client.CoreV1Api()
        pod_ret_list = v1_ua.list_namespaced_pod(namespace)
        for pod in pod_ret_list.items:
            applist_apps.append(
                AppListApp(
                    pod.metadata.name,  # Name
                    pod.status.container_statuses[0].ready,  # Ready
                    pod.status.phase,  # Status
                    pod.status.message,  # Status (message)
                    pod.status.reason,  # Status (reason)
                    pod.status.container_statuses[0].restart_count,  # Restarts
                    arrow.get(pod.metadata.creation_timestamp)
                    .to("local")
                    .humanize(only_distance=True),  # Age
                    pod.metadata.creation_timestamp,  # Age (creation timestamp)
                    pod.status.container_statuses[0].image,  # ImageURL
                )
            )
    except ApiException as e:
        error_code = str(e)[1:4]
        if error_code == "403":
            # user does not have access to this ns, dont add it to ns_list.
            pass
        else:
            logger.error("Error Getting Applist Apps:\n{0}".format(e))

    return applist_apps


def get_applist(current_user=None):
    """Get the applist (this is the applist view app list).
    """
    applist = []
    # get ns_list first, for each ns, get app list.
    ns_list = get_ns_list(current_user=current_user, include_default_item=True)
    # ns_list = []
    # ns_list.append('ab-prd')
    for ns in ns_list:
        if ns != "-- Select Namespace --":
            # get apps (with status and other info) for ns.
            ns_applist_apps = get_ns_applist_apps(
                current_user=current_user, namespace=ns
            )
            applist.append(AppListNamespace(ns, ns_applist_apps))
    return sorted(applist, key=lambda namespace: namespace.namespace)


def get_instance_logs(current_user=None, data: PodLogRequest = None):
    """Get instance logs.
    """
    instance_logs = "getting instance logs from: {0}".format(data)

    configuration = client.Configuration()
    configuration.host = "{0}/k8s/clusters/{1}".format(
        Config.DefaultConfig.KUBE_SERVER_URL, Config.DefaultConfig.KUBE_CLUSTER_ID
    )
    try:
        configuration.api_key = {
            "authorization": "Bearer " + pyco_decrypt(current_user.kube_bearer_token)
        }
        client.Configuration.set_default(configuration)
        v1_ua = client.CoreV1Api()
        instance_logs = v1_ua.read_namespaced_pod_log(
            name=data.name,
            namespace=data.namespace,
            follow=data.follow,
            pretty=data.pretty,
            container=data.container,
            previous=data.previous,
        )
    except ApiException as e:
        error_code = str(e)[1:4]
        if error_code == "403":
            # user does not have access to this ns, dont add it to ns_list.
            pass
        else:
            logger.error("Error Getting Pod Logs:\n{0}".format(e))

    return instance_logs


def get_instance_specs(current_user=None, data: PodRequest = None):
    """Get instance specs
    """
    instance_specs = "getting instance specs from: {0}".format(data)

    configuration = client.Configuration()
    configuration.host = "{0}/k8s/clusters/{1}".format(
        Config.DefaultConfig.KUBE_SERVER_URL, Config.DefaultConfig.KUBE_CLUSTER_ID
    )
    try:
        configuration.api_key = {
            "authorization": "Bearer " + pyco_decrypt(current_user.kube_bearer_token)
        }
        client.Configuration.set_default(configuration)
        v1_ua = client.CoreV1Api()
        api_response = v1_ua.read_namespaced_pod(
            name=data.name, namespace=data.namespace, pretty=data.pretty
        )
        instance_specs = api_response.spec.containers[0]
    except ApiException as e:
        error_code = str(e)[1:4]
        if error_code == "403":
            # user does not have access to this ns, dont add it to ns_list.
            pass
        else:
            logger.error("Error Getting Instance Specs:\n{0}".format(e))

    return instance_specs


def get_service_status_group(group=None):
    ss = []
    for key in rc.scan_iter(group):
        ss.append(json.loads(rc.get(key)))
    return ss


def get_service_status(service=None):
    ss = []
    ss_query = rc.get(service)
    if ss_query:
        ss.append(json.loads(ss_query))
    return ss


def get_queue_depth():
    from app import db
    from models import Queue

    queue_depth = 0
    cursor = (
        db.session.query(Queue)
        .join(Queue.queue_status)
        .filter(Queue.queue_status.property.mapper.class_.name == "request")
    )
    queue_depth = cursor.count()
    return queue_depth


def humanized_time(in_seconds: None):
    attrs = ["days", "hours", "minutes", "seconds"]
    human_readable = lambda delta: [
        "%d %s" % (getattr(delta, attr), getattr(delta, attr) > 1 and attr or attr[:-1])
        for attr in attrs
        if getattr(delta, attr)
    ]
    return (
        "".join(human_readable(relativedelta(seconds=in_seconds)))
        .replace(" seconds", "s")
        .replace(" minutes", "m")
        .replace(" minute", "m")
        .replace(" hours", "h")
        .replace(" days", "D ")
    )


def ping_service_status(service_id, about):
    # logger.info('NOTICE: ping service status background thread started...')
    while True:
        set_service_status(service_id, about)
        # note: this loop sleeps 5 seconds less than the TTL to ensure the TTL is reset before it expires.
        sleep(int(Config.DefaultConfig.CACHE_WUI_SERVICE_STATS_TTL) - 5)


def set_service_status(service_id=None, about=None):
    dts = arrow.get(datetime.datetime.now()).format("YYYY-MM-DD HH:mm:ss")
    ss = ServiceStatus(
        "wui",
        "wui",
        service_id,
        "success",
        dts,
        "ping",
        "{0} ({1})".format(about["version"], about["modified"]),
    )
    rc.set("service_status:{0}:{1}".format(ss.name, ss.service_id), ss.to_json())
    rc.expire(
        "service_status:{0}:{1}".format(ss.name, ss.service_id),
        Config.DefaultConfig.CACHE_WUI_SERVICE_STATS_TTL,
    )
