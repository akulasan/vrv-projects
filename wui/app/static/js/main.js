// custom scripts


/*
 * Copy input text to clipboard.
 */
function clipit(in_string, obj) {
    var input = document.createElement('input')
    input.id="__copyText__";
    input.value = in_string;
    document.body.appendChild(input);
    input.select();
    document.execCommand("copy");
    var txt = input.value
    input.remove()
    console.log("Text Copied to Clipboard: '"+txt+"'");
    clipitAlert("Copied to clipboard!", obj)
}


/*
 * Clipit alert (div popup).
 */
function clipitAlert(message, obj) {
    x = document.getElementById(obj);
    x.innerHTML=message;
    x.classList.toggle("show");
    setTimeout('x.classList.toggle("show")', 3000);
}
