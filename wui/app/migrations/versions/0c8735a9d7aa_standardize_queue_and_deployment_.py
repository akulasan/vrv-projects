"""standardize queue and deployment statuses

Revision ID: 0c8735a9d7aa
Revises: 31ee695fadfe
Create Date: 2018-06-13 09:55:07.193654

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '0c8735a9d7aa'
down_revision = '31ee695fadfe'
branch_labels = None
depends_on = None


def upgrade():
    # rename 'complete' to 'success'
    op.execute("UPDATE deployment_status SET \"name\" = 'success', \"desc\" = 'deployment succeeded' WHERE \"name\" = 'complete' ")
    # rename 'created' to 'request'
    op.execute("UPDATE deployment_status SET \"name\" = 'request', \"desc\" = 'deployment requested' WHERE \"name\" = 'created' ")
    # update 'failure'
    op.execute("UPDATE deployment_status SET \"desc\" = 'deployment failed' WHERE \"name\" = 'failure' ")

    # add new 'deploying' status
    op.execute("INSERT INTO deployment_status (\"name\", \"desc\") VALUES('deploying','deployment deploying')")


def downgrade():
    # rename 'complete' to 'success'
    op.execute("UPDATE deployment_status SET \"name\" = 'complete', \"desc\" = 'deployment complete' WHERE \"name\" = 'success' ")
    # rename 'created' to 'request'
    op.execute("UPDATE deployment_status SET \"name\" = 'created', \"desc\" = 'request created' WHERE \"name\" = 'request' ")
    # update 'failure'
    op.execute("UPDATE deployment_status SET \"desc\" = 'request failed' WHERE \"name\" = 'failure' ")

    # delete new 'deploying' status
    op.execute("DELETE FROM deployment_status WHERE \"name\" = 'deploying'")
