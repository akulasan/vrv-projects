"""create api_account

Revision ID: 840ef5be11a4
Revises: 35f32626c9c8
Create Date: 2018-06-08 15:20:10.283391

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '840ef5be11a4'
down_revision = '35f32626c9c8'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.create_table('api_account',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('username', sa.String(length=33), nullable=False),
    sa.Column('password', sa.String(length=255), nullable=False),
    sa.Column('active', sa.Boolean(), nullable=False),
    sa.Column('note', sa.Text(), nullable=True),
    sa.PrimaryKeyConstraint('id'),
    sa.UniqueConstraint('username')
    )
    op.alter_column('deployment', 'approver',
               existing_type=sa.VARCHAR(length=50),
               nullable=False)
    op.alter_column('queue', 'requester',
               existing_type=sa.VARCHAR(length=50),
               nullable=False)
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.alter_column('queue', 'requester',
               existing_type=sa.VARCHAR(length=50),
               nullable=True)
    op.alter_column('deployment', 'approver',
               existing_type=sa.VARCHAR(length=50),
               nullable=True)
    op.drop_table('api_account')
    # ### end Alembic commands ###
