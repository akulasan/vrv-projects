# ~ from wtforms import Form, BooleanField, StringField, PasswordField, validators

# ~ class RegistrationForm(Form):
# ~ username = StringField('Username', [validators.Length(min=4, max=25)])
# ~ email = StringField('Email Address', [validators.Length(min=6, max=35)])
# ~ password = PasswordField('New Password', [
# ~ validators.DataRequired(),
# ~ validators.EqualTo('confirm', message='Passwords must match')
# ~ ])
# ~ confirm = PasswordField('Repeat Password')
# ~ accept_tos = BooleanField('I accept the TOS', [validators.DataRequired()])


from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, SubmitField, validators


class LoginForm(FlaskForm):
    username = StringField(
        "Username",
        [validators.Length(min=4, max=25)],
        render_kw={"placeholder": "enter username"},
    )
    password = PasswordField(
        "Password",
        [validators.Length(min=4, max=25)],
        render_kw={"placeholder": "enter password"},
    )
    submit = SubmitField("Submit")
