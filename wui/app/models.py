import datetime
from app import db
from flask_security import UserMixin, RoleMixin
from passlib.apps import custom_app_context as pwd_context


cluster_namespace_clusternamespaces = db.Table(
    "cluster_namespace_clusternamespaces",
    db.Column("deploy_cluster_id", db.Integer(), db.ForeignKey("deploy_cluster.id")),
    db.Column(
        "cluster_namespace_id", db.Integer(), db.ForeignKey("cluster_namespace.id")
    ),
)

namespaceapp_namespaceapps = db.Table(
    "namespaceapp_namespaceapps",
    db.Column(
        "cluster_namespace_id", db.Integer(), db.ForeignKey("cluster_namespace.id")
    ),
    db.Column("namespace_app_id", db.Integer(), db.ForeignKey("namespace_app.id")),
)

roles_users = db.Table(
    "roles_users",
    db.Column("user_id", db.Integer(), db.ForeignKey("user.id")),
    db.Column("role_id", db.Integer(), db.ForeignKey("role.id")),
)

tag_queue = db.Table(
    "tag_queue",
    db.Column("queue_id", db.Integer(), db.ForeignKey("queue.id")),
    db.Column("tag_id", db.Integer(), db.ForeignKey("tag.id")),
)


class ApiAccount(db.Model):
    __tablename__ = "api_account"
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(33), nullable=False, unique=True)
    password = db.Column(db.String(255), nullable=False)
    active = db.Column(db.Boolean(), default=True, nullable=False)
    note = db.Column(db.Text, nullable=True)
    associated_user_id = db.Column(db.Integer, db.ForeignKey("user.id"), nullable=False)
    associated_user = db.relationship("User", foreign_keys=[associated_user_id])

    def hash_password(password):
        return pwd_context.encrypt(password)

    def verify_password(self, password):
        return pwd_context.verify(password, self.password)


class BuildTemplate(db.Model):
    __tablename__ = "build_template"
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(50), nullable=False, unique=True)
    description = db.Column(db.String(500), nullable=False)
    template = db.Column(db.Text, nullable=False)


class ClusterNamespace(db.Model):
    __tablename__ = "cluster_namespace"
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(100), nullable=False)
    description = db.Column(db.String(255))
    active = db.Column(db.Boolean(), default=True, nullable=False)

    # multiple ocurring apps per namespace.
    namespace_apps = db.relationship(
        "NamespaceApp",
        secondary=namespaceapp_namespaceapps,
        # back_populates="cluster_namespaces"
        backref=db.backref("cluster_namespace", lazy="dynamic"),
    )

    def __str__(self):
        return "{0}".format(self.name)


class ComponentRunDefault(db.Model):
    __tablename__ = "component_run_default"
    id = db.Column(db.Integer, primary_key=True)
    component = db.Column(db.String)
    data = db.Column(db.JSON, default={}, nullable=True)


class DeploymentStatus(db.Model):
    __tablename__ = "deployment_status"
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String)
    desc = db.Column(db.String)

    def __str__(self):
        return "{0}".format(self.name)


class Deployment(db.Model):
    __tablename__ = "deployment"
    id = db.Column(db.Integer, primary_key=True)
    deployment_status_id = db.Column(
        db.Integer, db.ForeignKey("deployment_status.id"), nullable=False
    )
    deployment_status = db.relationship(
        DeploymentStatus, foreign_keys="Deployment.deployment_status_id"
    )
    deployment_status_message = db.Column(db.String(2000), nullable=True)
    namespace = db.Column(db.String(50), nullable=True)
    app_name = db.Column(db.String(50), nullable=True)
    image_url = db.Column(db.String(255), nullable=False)
    approver = db.Column(db.String(50), nullable=False)
    note = db.Column(db.Text, nullable=True)
    created_by = db.Column(db.Integer, db.ForeignKey("user.id"), nullable=False)
    creator = db.relationship("User", foreign_keys=[created_by])
    created_at = db.Column(
        db.TIMESTAMP, default=datetime.datetime.utcnow, nullable=False
    )

    # init set default values.
    def __init__(self, deployment_status_id=None, note=""):
        self.deployment_status_id = (
            DeploymentStatus.query.filter_by(name="request").one().id
        )
        self.note = note


class DeployCluster(db.Model):
    __tablename__ = "deploy_cluster"
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(100), nullable=False)
    description = db.Column(db.String(255))
    active = db.Column(db.Boolean(), default=True, nullable=False)

    # multiple ocurring namespaces per cluster.
    cluster_namespaces = db.relationship(
        "ClusterNamespace",
        secondary=cluster_namespace_clusternamespaces,
        backref=db.backref("deploy_cluster", lazy="select"),
    )

    created_by = db.Column(db.Integer, db.ForeignKey("user.id"), nullable=False)
    created_at = db.Column(
        db.TIMESTAMP, default=datetime.datetime.utcnow, nullable=False
    )

    creator = db.relationship("User", foreign_keys=[created_by])

    def __str__(self):
        return "{0}".format(self.name)


class NamespaceApp(db.Model):
    __tablename__ = "namespace_app"
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(100), nullable=False)
    description = db.Column(db.String(255))
    active = db.Column(db.Boolean(), default=True, nullable=False)
    service_spec = db.Column(db.Text, nullable=False)

    cluster_namespaces = db.relationship(
        "ClusterNamespace",
        secondary=namespaceapp_namespaceapps,
        back_populates="namespace_apps",
        # ~ backref="cluster_namespace",
        lazy="dynamic",
    )

    def __str__(self):
        return "{0} (ns: {1})".format(self.name, self.cluster_namespaces.one().name)


class QueueStatus(db.Model):
    __tablename__ = "queue_status"
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String)
    desc = db.Column(db.String)

    def __str__(self):
        return "{0}".format(self.name)


class Queue(db.Model):
    __tablename__ = "queue"
    id = db.Column(db.Integer, primary_key=True)
    queue_status_id = db.Column(
        db.Integer, db.ForeignKey("queue_status.id"), nullable=False
    )
    queue_status = db.relationship(QueueStatus, foreign_keys="Queue.queue_status_id")
    data = db.Column(db.JSON, default={}, nullable=False)
    result = db.Column(db.JSON, nullable=True)
    note = db.Column(db.Text, nullable=True)
    requester = db.Column(db.String(50), nullable=False, default="self")
    created_by = db.Column(db.Integer, db.ForeignKey("user.id"), nullable=False)
    creator = db.relationship("User", foreign_keys=[created_by])
    created_at = db.Column(
        db.TIMESTAMP, default=datetime.datetime.utcnow, nullable=False
    )
    tags = db.relationship(
        "Tag", secondary=tag_queue, backref=db.backref("queue", lazy="dynamic")
    )

    # init so QueueStatus of 'request[ed]' can be set.
    def __init__(self, queue_status_id=None, data={}, result="", note=""):
        self.queue_status_id = QueueStatus.query.filter_by(name="request").one().id
        self.data = data
        self.result = result
        self.note = note


class Role(db.Model, RoleMixin):
    id = db.Column(db.Integer(), primary_key=True)
    name = db.Column(db.String(80), unique=True)
    description = db.Column(db.String(255))

    # __str__ is required by Flask-Admin, so we can have human-readable values for the Role when editing a User.
    # If we were using Python 2.7, this would be __unicode__ instead.
    def __str__(self):
        return self.name

    # __hash__ is required to avoid the exception TypeError: unhashable type: 'Role' when saving a User
    def __hash__(self):
        return hash(self.name)

    def __init__(self, name=name, description=description):
        self.name = name
        self.description = description


class Tag(db.Model):
    __tablename__ = "tag"
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(20), nullable=False)

    created_by = db.Column(db.Integer, db.ForeignKey("user.id"), nullable=False)
    creator = db.relationship("User", foreign_keys=[created_by])

    def __str__(self):
        return "%s" % (self.name)


class User(db.Model, UserMixin):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(100), nullable=False)
    username = db.Column(db.String(33), nullable=False, unique=True)
    nickname = db.Column(db.String(5), nullable=True, unique=True)
    password = db.Column(db.String(255), nullable=False)
    roles = db.relationship(
        "Role", secondary=roles_users, backref=db.backref("users", lazy="dynamic")
    )
    kube_bearer_token = db.Column(db.String(255), nullable=True)

    def __init__(
        self,
        name=None,
        username=None,
        nickname=None,
        password=None,
        roles=[],
        kube_bearer_token=None,
    ):
        self.id = None
        self.name = name
        self.username = username
        self.nickname = nickname
        self.password = password
        self.roles = roles
        self.kube_bearer_token = kube_bearer_token

    def is_active(self):
        """True, as all users are active."""
        return True

    def get_id(self):
        """Return the id to satisfy Flask-Login's requirements."""
        return self.id

    def is_authenticated(self):
        """Return True if the user is authenticated."""
        return self.authenticated

    def is_anonymous(self):
        """False, as anonymous users aren't supported."""
        return False

    def __str__(self):
        return "%s" % (self.username)
