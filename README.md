# README

This is the readme for ab, a docker based docker build environment.

## Notices

- the source code repo must have the ab ssh public key added if the repo is not public, the public key can be found at  `app/admin/.ssh/id_rsa.pub`
- keep in mind that all tools used in the launcher or build script must be available on the build server. The build server is a very light docker based Alphine Linux instance, see the Dockerfile for packages/tools which are available. If you need additional tools please contact support.
- the 'sources' are 'cached' on the build server locally (if the build server is running in a kubernetes environment and a pod gets rebuild, the cache will be gone and the process will re-download/checkount needed code (which will increase build time))
- all logs of all apps can be available on console (container log out) and graylog, see .env file for where logs are being sent.


## Setup
- all database logic (create, load, etc.) is done with migrations from the wui app, see `docs/wui.md` for more info.


## Adding A Component to ab

- create launcher script
  - use the cron/.template (should only need to change 4 vars)
- create build (build-ab) script (use classroom-app as an example)
- test build
  - note: all runs of a build post the results to the #builds slack channel by default
- if all goes well you are done and/or you can schedule the job


## Details

### Launcher Script

The launcher script is a simple `sh` script that performs the source checkout (if the source is not already checked out) and then calls the build-ab script which handles the build. The launcher script is intended to be simple and should generally require changing 4 variables at the top of the script. This script can be ran manually or dropped into any of the cron scheduler buckets to perform automated builds.


### Build Script

The build script performs the complete build of the component in question. Most builds can follow a standard pattern (see the classroom-app build-ab as an example) and require little change; however, some components (such as SW5) require compilation and other tasks. The build script is independant from the launcher script and can be ran on its own.

### Testing Builds

For intial "smoke" testing of a new addition to ab the easiest approach is to run the ab container locally, connect to it and run your launcher script. You have full access to run and debug the build within the container.


## Deploys

Currently the ab process does not perform deploys as it is not desired/needed at this point. The ab process builds and image and uploads the image to the PCR (pcr.wwnorton.com). The image can be deployed through the typical kubernetes deploy process.


## Development

### NOTICE
- this project uses black for code formatting. there is a .pre-commit-config.yaml file to set it up as a pre-commit hook, you can do this by performing the following steps:
  - install pre-commit: `pip install pre-commit`
  - use pre-commit to install the black pre-commit config: `pre-commit install`
  ----
  - after doing the above, black will be ran on any commit, if your changes meet black formatting rules it will be committed, otherwise you will get a notice of issues and can adjust as needed
  - more information can be found at [black](https://github.com/ambv/black)



## Issues

Please log any issues to the [DEVOPS](https://wwnorton.atlassian.net/browse/DEVOPS) jira project.
