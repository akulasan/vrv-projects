#!/bin/sh
set -e

echo 'Starting crond...'
nohup crond -f -S -l 2 -L /var/log/crond.log &

echo 'Starting dockerd...'
# check if docker pid file exists (can linger from docker stop or unclean shutdown of container)
if [ -f /var/run/docker.pid ]; then
  echo '* notice: old docker pid file found, deleting...'
  rm -f /var/run/docker.pid
fi
nohup dockerd > /var/log/dockerd.log &

#
# NOTICE: the following command does not spawn execution to the background as
#         we need to leave something holding the container in run state.
#
echo "Starting builder app..."
exec python3 app.py

# tmp: just run container so we can bash in and run things manually.
#while :; do sleep 60s; done
#echo "All service started."
