# https://hub.docker.com/_/python/
FROM python:3.7.2-alpine3.8
MAINTAINER drad <drader@adercon.com>

COPY entrypoint.sh /
COPY admin/.ssh/* /root/.ssh/
COPY cron /etc/periodic/

RUN mkdir -p /opt/app
WORKDIR /opt/app/

# NOTICE: for development the following should be commented out to use a local volume for app code (this enables dynamic docker work)
COPY app /opt/app/
COPY app/requirements.txt /

RUN apk add --no-cache --virtual .build_deps                            \
    tzdata                                                              \
    # requirements to build psycopg2                                    \
    gcc                                                                 \
    musl-dev                                                            \
    libffi-dev                                                          \
  && apk add --no-cache                                                 \
    # note: bash is required for ab-build scripts.                      \
    bash                                                                \
    docker                                                              \
    git                                                                 \
    openssh-client                                                      \
    # needed at runtime for psycopg2                                    \
    postgresql-dev                                                      \
    python3-dev                                                         \
  && chmod 0700 /entrypoint.sh                                          \
  && chmod 0600 /root/.ssh/id_rsa*                                      \
  && echo "Set timezone..."                                             \
  && cp "/usr/share/zoneinfo/America/New_York" /etc/localtime           \
  # SETUP APP                                                           \
  && echo "Creating the 1min periodic runner..."                        \
  && mkdir -m 0755 -p /etc/periodic/1min                                \
  && echo "*       *       *       *       *       run-parts /etc/periodic/1min">>/etc/crontabs/root \
  && echo 'Creating docker image cleanup cron job...'                   \
  && echo "3       0       *       *       *       docker image prune --all --force">>/etc/crontabs/root \
  && echo "Set / Reset all jobs to executable..."                       \
  && chmod -R a+x /etc/periodic/*                                       \
  # SETUP APP                                                           \
  && mkdir -p /opt/sources                                              \
  && git config --global user.email "autobuild@wwnorton.com"            \
  && git config --global user.name "autobuild"                          \
  && pip install -r /requirements.txt                                   \
  # CLEANUP                                                             \
  && apk del .build_deps

ENTRYPOINT [ "/entrypoint.sh" ]
