import os, stat, sys, shutil, subprocess, arrow, datetime, uuid, logging, graypy, json

from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from sqlalchemy.ext.declarative import declarative_base
from subprocess import Popen, PIPE
from slackclient import SlackClient
from time import sleep
from pprint import pformat
from threading import Thread

# locals
from models import *
import config as Config

rundts = datetime.datetime.now()
qid = None
queue_item = None
result = None

about = {
    "name": "builder",
    "version": "1.14.1",
    "modified": "2019-01-03",
    "created": "2018-04-24",
}
service_id = str(uuid.uuid4())[:8]


class QidFilter(logging.Filter):
    # static (on init) values.
    # def __init__(self):
    # self.rid = '{0}'.format(rid)
    # on each log msg.
    def filter(self, record):
        global result
        record.qid = qid
        if record.getMessage().startswith(
            Config.DefaultConfig.BUILD_SCRIPT_RESPONSE_KEY
        ):
            # print("Build script response message received")
            # print('>>> Response Message: {0}'.format(record.getMessage()))
            try:
                # strip key off of msg and marshal to json
                result = json.loads(
                    record.getMessage()[
                        len(Config.DefaultConfig.BUILD_SCRIPT_RESPONSE_KEY) :
                    ]
                )
                update_queue_result(queue_item, result)
            except json.decoder.JSONDecodeError as e:
                logger.error(
                    "Error decoding build script response message: {0}".format(e)
                )
                update_item_status(item=queue_item, status="failure")
        return True


logger_base = logging.getLogger("main_logger")
logger_base.setLevel(logging.getLevelName(Config.DefaultConfig.APP_LOGLEVEL))
# add QidFilter which catches the Build Script Response message.
logger_base.addFilter(QidFilter())
graylog_handler = graypy.GELFHandler(
    host=Config.DefaultConfig.GRAYLOG_HOST, port=Config.DefaultConfig.GRAYLOG_PORT
)
console_handler = logging.StreamHandler()
if "graylog" in Config.DefaultConfig.LOG_TO:
    logger_base.addHandler(graylog_handler)
if "console" in Config.DefaultConfig.LOG_TO:
    logger_base.addHandler(console_handler)
logger = logging.LoggerAdapter(
    logging.getLogger("main_logger"),
    {
        "application_name": Config.DefaultConfig.GRAYLOG_APPNAME,
        "application_env": Config.DefaultConfig.GRAYLOG_APPENV,
    },
)
logger.info(
    "{0} - v.{1} ({2}) - {3}".format(
        about["name"], about["version"], about["modified"], service_id
    )
)
logger.info(
    "logs are being sent to: {0}".format(
        ", ".join(str(i) for i in Config.DefaultConfig.LOG_TO)
    )
)

db = create_engine(Config.DefaultConfig.SQLALCHEMY_DATABASE_URI)
Session = sessionmaker(db)
session = Session()
base.metadata.create_all(db)


def can_process(build_request_payload):
    if "APP_NAME" in build_request_payload:
        return True
    else:
        logger.warning("WARNING! Item does not contain app name")
        return False


def multi_component_enabled(build_request_payload):
    if (
        build_request_payload["APP_NAME"]
        in Config.DefaultConfig.multi_subcomponent_components
    ):
        logger.debug("component is multi-component enabled")
        return True
    else:
        logger.debug("component is not multi-component enabled")
        return False


# notify via slack.
def notify_via_slack(subject, message):
    slack_token = Config.DefaultConfig.slack["api-token"]
    try:
        sc = SlackClient(slack_token)
        resp = sc.api_call(
            "chat.postMessage",
            channel=Config.DefaultConfig.slack["channel"],
            text=subject,
            icon_emoji=":eyes",
            as_user="false",
            # link_name="http://adercon.com",
            attachments=[{"text": message, "mrkdwn_in": ["text"]}],
            username="buildbot",
        )
        # echo('Message sent via Slack, response was: {0}'.format(resp))
    except Exception as e:
        logger.error(
            "ERROR: Issue sending via slack, is your api key set/valid?\nDetails: {0}".format(
                e
            )
        )
        sys.exit(500)


def vc_checkout(prj_path, cmp_path, repo_branch, clone_opts, src_repo, src_name):
    # check if project is properly setup
    if os.path.exists("{0}/.git".format(cmp_path)):
        # git exists so prj has been checked out before
        logger.debug(
            "project already setup, updating to ensure we have everything needed for build..."
        )
        os.chdir(cmp_path)
        # first checkout the desired branch
        checkout_cmd = ["git", "checkout", repo_branch]
        logger.debug("- performing checkout of branch={0}".format(repo_branch))
        subprocess.call(checkout_cmd)
        # then pull (this handles if you are already on the branch to update).
        pull_cmd = ["git", "pull"]
        subprocess.call(pull_cmd)
        logger.debug("- performing pull to update")
    else:
        logger.debug("app not setup in project")
        try:
            os.makedirs(prj_path)
        except OSError:
            logger.debug("project root already exists")
        os.chdir(prj_path)
        clone_opts = ""
        clone_cmd = ["git", "clone", "-b", repo_branch, clone_opts, src_repo, src_name]
        clone_cmd = list(
            filter(None, clone_cmd)
        )  # remove empty strings (such as clone_opts if non specified).
        logger.info("cloning with: '%s'" % " ".join(map(str, clone_cmd)))
        subprocess.call(clone_cmd)


def perform_vc_checkout(item, prj_path, cmp_path, build_request_payload):
    logger.debug("perform_vc_checkout")
    update_item_status(item=item, status="vc_checkout")
    if build_request_payload:
        logger.info(
            "using APP_NAME={0} with: {1}".format(
                build_request_payload["APP_NAME"],
                pformat(build_request_payload, indent=4),
            )
        )
        # check if sources_dir exists.
        if not os.path.exists(Config.DefaultConfig.sources_dir):
            logger.error(
                "{0}\n - sources_dir = [{1}]".format(
                    "ERR-01: sources_dir not found, perhaps volume is not mounted? Cannot continue.",
                    Config.DefaultConfig.sources_dir,
                )
            )
            update_item_status(item=item, status="failure")
            sys.exit(1)
        # @TODO likely want a check on params here, PRJ_NAME, others must exist...
        if (
            "CLEAN_SOURCE" in build_request_payload
            and build_request_payload["CLEAN_SOURCE"] == "true"
        ):
            source_to_remove = ""
            if (
                not "PRJ_NAME" in build_request_payload
                or len(build_request_payload["PRJ_NAME"]) < 1
            ):
                source_to_remove = "{0}/{1}".format(
                    Config.DefaultConfig.sources_dir, build_request_payload["PRJ_NAME"]
                )
                if source_to_remove != "" and os.path.exists(source_to_remove):
                    logger.warning(
                        "WARNING! CLEAN_SOURCE flag found, cleaning source dir: [{0}] prior to build".format(
                            source_to_remove
                        )
                    )
                    shutil.rmtree(source_to_remove)
                else:
                    logger.error(
                        "{0}".format("ERR-02: PRJ_NAME not supplied, Cannot continue.")
                    )
                    sys.exit(2)
            else:
                logger.error(
                    "{0}".format("ERR-02: PRJ_NAME not supplied, Cannot continue.")
                )
                sys.exit(2)
        vc_clone_opts = ""
        if "VC_CLONE_OPTS" in build_request_payload:
            vc_clone_opts = build_request_payload["VC_CLONE_OPTS"]
        if multi_component_enabled(build_request_payload):
            logger.debug("perform multi-component processing...")
            cmp_1_path = "{0}/{1}.git".format(
                prj_path, build_request_payload["SRC_NAME_CLIENT"]
            )
            cmp_2_path = "{0}/{1}.git".format(
                prj_path, build_request_payload["SRC_NAME_SERVER"]
            )
            logger.debug("checking setup for sw5-client...")
            vc_checkout(
                prj_path,
                cmp_1_path,
                # ~ build_request_payload["REPO_BRANCH_CLIENT"],
                build_request_payload["REPO_BRANCH"],
                vc_clone_opts,
                build_request_payload["SRC_REPO_CLIENT"],
                build_request_payload["SRC_NAME_CLIENT"],
            )
            logger.debug("checking setup for sw5-server...")
            vc_checkout(
                prj_path,
                cmp_2_path,
                # ~ build_request_payload["REPO_BRANCH_SERVER"],
                build_request_payload["REPO_BRANCH"],
                vc_clone_opts,
                build_request_payload["SRC_REPO_SERVER"],
                build_request_payload["SRC_NAME_SERVER"],
            )
        else:
            logger.debug("performing non-multi-component processing...")
            vc_checkout(
                prj_path,
                cmp_path,
                build_request_payload["REPO_BRANCH"],
                vc_clone_opts,
                build_request_payload["SRC_REPO"],
                build_request_payload["SRC_NAME"],
            )
    else:
        logger.error("No build data provided, cannot process item...")
        update_item_status(item=item, status="failure")
        return False
    return True


def get_build_script_name(cmp_path=None):
    return "{0}/build-ab".format(cmp_path).replace("//", "/")


def cleanup_build_script(had_existing_build_script=None, build_script_name=None):
    if had_existing_build_script:
        # delete the build_template version of build-ab script.
        os.remove(build_script_name)
        # move original project build build-ab back to its original name.
        shutil.move("{0}_orig".format(build_script_name), build_script_name)
    else:
        # delete the build_template version of build-ab script.
        os.remove(build_script_name)


def perform_build(item, prj_path, cmp_path, build_request_payload):
    logger.debug("perform build")
    update_item_status(item=item, status="building")
    # adjust for BUILD_DIR if exists.
    if (
        "BUILD_DIR" in build_request_payload
        and len(build_request_payload["BUILD_DIR"]) > 0
        and build_request_payload["BUILD_DIR"] != "/"
    ):
        cmp_path = "{0}/{1}/".format(cmp_path, build_request_payload["BUILD_DIR"])
        logger.info(
            "NOTICE: cmp_path adjusted to include BUILD_DIR ({0}), full path: {1}".format(
                build_request_payload["BUILD_DIR"], cmp_path
            )
        )
    logger.info("cmp_path is: {0}".format(cmp_path))
    logger.debug("item data items: {0}".format(build_request_payload.items()))
    # set additional envars
    os.environ["RUNSTAMP"] = "{0}".format(arrow.get(rundts).format("YYYY-MM-DD_HHmm"))
    # set envars from request.
    for k, v in build_request_payload.items():
        logger.info("set var: {0}={1}".format(k, v))
        try:
            os.environ[k] = v
        except TypeError as e:
            os.environ[k] = str(v).lower()
        except Exception as e:
            logger.error("Error occurred in setting var:\n{0}".format(e))
    logger.debug("finished setting vars")
    if os.path.exists(cmp_path):
        os.chdir(cmp_path)
    else:
        logger.error(
            "Missing Application in Source: the application does not exist in source, this can happen if the app is a part of a project and the application has not been committed/pushed to the git repo. Please verify the app is available in git and try again."
        )
        return False
    build_script_name = get_build_script_name(cmp_path=cmp_path)
    logger.info("current working directory: {0}".format(os.getcwd()))
    logger.debug(" - build script: {0}".format(build_script_name))
    # get build_template, if not defined or is 'none' then fall back to build-ab script.
    build_template = build_request_payload["BUILD_TEMPLATE"]
    had_existing_build_script = False
    # if we have a build template, write it to fs and use it.
    if build_template and build_template != "none" and len(build_template) > 0:
        logger.info(
            "NOTICE: build template ({0}) found and will be used over app build script (if exists)".format(
                build_template
            )
        )
        qry_build_template = session.query(BuildTemplate).filter(
            BuildTemplate.name == build_template
        )
        res_build_template = qry_build_template.first()
        # ~ logger.debug(
        # ~ "build template is as follows:\n---------------------------------\n{0}\n---------------------------------".format(
        # ~ res_build_template.template
        # ~ )
        # ~ )
        if os.path.exists(build_script_name):
            had_existing_build_script = True
            # logger.debug(' - original build script details: {0}'.format(oct(os.stat(build_script_name).st_mode)))
            # create backup of project build-ab as we will use the build-ab from build_template.
            shutil.copy(build_script_name, "{0}_orig".format(build_script_name))
        # write the build_template out as a file.
        bt_template_file = open(build_script_name, "w", newline="")
        # note: must remove \r from file or you will get a FileNotFoundError (odd).
        bt_template_file.write(res_build_template.template.replace("\r", ""))
        bt_template_file.close()
        # make file executable.
        os.chmod(build_script_name, os.stat(build_script_name).st_mode | stat.S_IEXEC)
        logger.info("* build script set from build template")
    else:
        logger.info(
            "NOTICE: no build template specified/found, falling back to 'build-ab' script..."
        )
    if os.path.isfile(build_script_name):
        logger.debug(
            "build script found ({0}), executing build".format(build_script_name)
        )
        # logger.debug(' - build script details: {0}'.format(oct(os.stat(build_script_name).st_mode)))
        # ~ for root, dirs, files in os.walk(cmp_path):
        # ~ for filename in files:
        # ~ logger.debug(" - file: {0}".format(filename))
        # execute build.
        try:
            logger.debug("build script: {0}".format(build_script_name))
            # logger.debug(' - build script details: {0}'.format(oct(os.stat('/opt/sources/autobuild/autobuild/wui/build-ab').st_mode)))
            with Popen(
                build_script_name,
                stdout=PIPE,
                stderr=subprocess.STDOUT,
                bufsize=1,
                universal_newlines=True,
            ) as p:
                for line in p.stdout:
                    # logger.debug('{0}'.format(line), end='')
                    logger.info("{0}".format(line))
                    # log data here.
            # check response exit code.
            logger.debug("build script exit code was: [{0}]".format(p.returncode))
            cleanup_build_script(
                had_existing_build_script=had_existing_build_script,
                build_script_name=build_script_name,
            )
            if p.returncode != 0:
                logger.warn("build failed (exit code: {0})".format(p.returncode))
                return False
            else:
                logger.info("build succeeded (exit code: {0})".format(p.returncode))
                return True
        except PermissionError as e:
            logger.error("Permission Error: Cannot execute build script: {0}".format(e))
            cleanup_build_script(
                had_existing_build_script=had_existing_build_script,
                build_script_name=build_script_name,
            )
            return False
        except FileNotFoundError as e:
            logger.error(
                "Build File Not Found: the build script cannot be ran as the file was not found: {0}".format(
                    e
                )
            )
            cleanup_build_script(
                had_existing_build_script=had_existing_build_script,
                build_script_name=build_script_name,
            )
            return False
        except Exception as e:
            logger.error("Build Other Error: an error occurred: {0}".format(e))
            cleanup_build_script(
                had_existing_build_script=had_existing_build_script,
                build_script_name=build_script_name,
            )
            return False
    else:
        logger.error("Wrapper build script not found, cannot build!")
        return False


def update_item_status(item=None, status=None):
    global Session
    logger.debug("update queue item id={0} status={1}".format(item.id, status))
    status_id = session.query(QueueStatus).filter(QueueStatus.name == status).one().id
    item.queue_status_id = status_id
    session.commit()


def update_queue_result(item, result):
    logger.debug("update queue item id={0}".format(item.id))
    item.result = result
    session.commit()


def get_build_start_message(build_request_payload, repo_branch):
    return """
• App: {0} / {1}
• By: {8} (for: {9})
• Note: {10}
• Build: {2} (clean: {3}) / Push: {4} (latest: {5}) / VC Tag: {6} (tag: {7})
""".format(
        build_request_payload["APP_NAME"]
        if "APP_NAME" in build_request_payload
        else "",
        repo_branch,
        build_request_payload["RUN_BUILD"]
        if "RUN_BUILD" in build_request_payload
        else "",
        build_request_payload["CLEAN_SOURCE"]
        if "CLEAN_SOURCE" in build_request_payload
        else "",
        build_request_payload["PUSH_IMAGE_ENABLED"]
        if "PUSH_IMAGE_ENABLED" in build_request_payload
        else "",
        build_request_payload["TAG_IMAGE_LATEST"]
        if "TAG_IMAGE_LATEST" in build_request_payload
        else "",
        build_request_payload["VC_TAG_ENABLED"]
        if "VC_TAG_ENABLED" in build_request_payload
        else "",
        build_request_payload["VC_TAG"]
        if "VC_TAG" in build_request_payload and build_request_payload["VC_TAG"] != ""
        else "not specified",
        build_request_payload["QUEUE_BY"]
        if "QUEUE_BY" in build_request_payload
        else "",  # By
        build_request_payload["QUEUE_FOR"]
        if "QUEUE_FOR" in build_request_payload
        else "",  # For
        build_request_payload["QUEUE_NOTE"]
        if "QUEUE_NOTE" in build_request_payload
        else "",  # Note
    )


def get_build_complete_message(result):
    logger.debug("build complete message from result={0}".format(result))
    # if we dont have a 'component' defined then something went wrong with the build.
    if not "component" in result:
        logger.error(
            "ERROR: generating build complete message is missing the Component"
        )
        ret = "Build Failed, see record for more information."
    else:
        ret = """
• App: {0} / {1}
• Result: {3}
• Img: {10} (size: {11})
• Build: {2} (in {12}s)
• Push: {4} (latest: {5})
• Tag: {6} (latest: {7})
• VC Tag: {8} (tag: {9})
  """.format(
            result["component"] if "component" in result else "",
            result["vc_ref"] if "vc_ref" in result else "",
            result["build_success"] if "build_success" in result else "",
            result["build_result_msg"] if "build_result_msg" in result else "",
            result["image_push_performed"] if "image_push_performed" in result else "",
            result["image_push_latest_performed"]
            if "image_push_latest_performed" in result
            else "",
            result["image_tag_performed"] if "image_tag_performed" in result else "",
            result["image_tag_latest_performed"]
            if "image_tag_latest_performed" in result
            else "",
            result["vc_tag_performed"] if "vc_tag_performed" in result else "",
            result["image_set_id"] if "image_set_id" in result else "",
            result["image_url"] if "image_url" in result else "",
            result["image_size"] if "image_size" in result else "",
            result["build_time"] if "build_time" in result else "",
        )
    return ret


# enrich the payload by adding any (missing) data in AppConfig for a given payload APP_NAME.
#  NOTE: this function gets AppConfig data for a given APP_NAME and updates it with data from the payload
#        producing the effect of adding any "missing" info from AppConfig to the payload.
def enrich_payload(qid=None, payload=None):
    enriched = {}
    app_name = payload["APP_NAME"]
    logger.debug(
        "Enriching payload for app: [{0}] and qid: [{1}]".format(app_name, qid)
    )
    # add AppConfig data
    ac_cursor = session.query(AppConfig).filter(AppConfig.component == app_name).first()
    logger.debug("* AppConfig data: {0}".format(ac_cursor.data))
    if ac_cursor:
        enriched = ac_cursor.data
        # overlay the payload data on top of the AppConfig data.
        enriched.update(payload)
    else:
        # if no AppConfig is found, return original payload.
        enriched = payload

    # add queue data (e.g. by, for, note)
    q_cursor = session.query(Queue).filter(Queue.id == qid).first()
    logger.debug(
        "* Queue record - creator={0} created_for={1} note={2}".format(
            q_cursor.creator.nickname, q_cursor.requester, q_cursor.note
        )
    )
    if q_cursor:
        enriched["QUEUE_BY"] = q_cursor.creator.nickname
        enriched["QUEUE_FOR"] = q_cursor.requester
        enriched["QUEUE_NOTE"] = q_cursor.note
    # logger.debug("* returning enriched payload: {0}".format(enriched))
    return enriched


def process_item(item):
    global qid, queue_item
    queue_item = item
    qid = item.id
    logger.debug("build request payload: {0}".format(item.data))
    build_request_payload = enrich_payload(qid=qid, payload=item.data)
    # logger.debug("enhanced build request payload: {0}".format(build_request_payload))
    logger.info("processing qid: {0}".format(qid))
    if can_process(build_request_payload):
        prj_path = "{0}/{1}".format(
            Config.DefaultConfig.sources_dir, build_request_payload["PRJ_NAME"]
        )
        if multi_component_enabled(build_request_payload):
            # note: for multi-component builds we set cmp_path to the primary cmp (the one who does the build).
            cmp_path = "{0}/{1}".format(
                prj_path, build_request_payload["SRC_NAME_CLIENT"]
            )
            # ~ repo_branch = "{0}/{1}".format(
            # ~ build_request_payload["REPO_BRANCH_CLIENT"],
            # ~ build_request_payload["REPO_BRANCH_SERVER"],
            # ~ )
            repo_branch = build_request_payload["REPO_BRANCH"]
        else:
            cmp_path = "{0}/{1}".format(prj_path, build_request_payload["SRC_NAME"])
            repo_branch = build_request_payload["REPO_BRANCH"]
        subject = "build started with qid of: {1} (<{0}&q=qid:{1}|view logs>)".format(
            Config.DefaultConfig.CLS_URL, qid
        )
        logger.debug("Sending 'build start' slack message")
        notify_via_slack(
            subject, get_build_start_message(build_request_payload, repo_branch)
        )
        logger.debug("Sent 'build start' slack message")
        # perform vc checkout
        if perform_vc_checkout(item, prj_path, cmp_path, build_request_payload):
            # execute the build.
            if perform_build(item, prj_path, cmp_path, build_request_payload):
                update_item_status(item=item, status="success")
            else:
                update_item_status(item=item, status="failure")
            subject = "build completed with qid of: {1} (<{0}&q=qid:{1}|view logs>)".format(
                Config.DefaultConfig.CLS_URL, qid
            )
            notify_via_slack(subject, get_build_complete_message(result))
        else:
            logger.error("vc checkout failed...")
    else:
        logger.warning("cannot process item, skipped...")
    # re-init qid so logs dont erroneously have the old qid.
    qid = None


# start a background thread that keeps updating the service status.
#   note: thread runs indefinitely.
from utils import ping_service_status

thread = Thread(target=ping_service_status, args=(service_id, about))
thread.start()
# start the main app loop to check queue for build requests.
while True:
    cursor = (
        session.query(Queue)
        .join(Queue.queue_status)
        .filter(Queue.queue_status.property.mapper.class_.name == "request")
        .order_by(Queue.id)
    )
    result = cursor.first()
    if cursor.count() < 1:
        logger.debug("No queue items to process")
    else:
        while result is not None:
            # mark item as being processed
            update_item_status(item=result, status="building")
            process_item(result)
            # check for more items to process.
            result = cursor.first()
    logger.debug(
        "* sleeping for {0}s".format(Config.DefaultConfig.QUEUE_CHECK_SLEEP_AMOUNT)
    )
    sleep(Config.DefaultConfig.QUEUE_CHECK_SLEEP_AMOUNT)
