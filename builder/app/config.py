import os, logging
from distutils.util import strtobool


def env_strtobool(val):
    if val in ["true", "'true'"]:
        return True
    return False


class BaseConfig(object):
    # APPLICATION
    multi_subcomponent_components = os.environ["MULTI_SUBCOMPONENT_COMPONENTS"].split(
        ","
    )
    sources_dir = os.environ["SOURCES_DIR"]

    CACHE_HOST = os.environ["CACHE_HOST"]
    CACHE_PORT = os.environ["CACHE_PORT"]
    CACHE_DB = os.environ["CACHE_DB"]
    CACHE_BUILDER_SERVICE_STATS_TTL = os.environ["CACHE_BUILDER_SERVICE_STATS_TTL"]

    # amount of time to sleep between queue checks in seconds.
    #  notice: the queue processor will continue to run (with no sleeps) if there are more records in the queue.
    #          this sleep amount comes into play when all queue records are processed.
    QUEUE_CHECK_SLEEP_AMOUNT = int(os.environ["QUEUE_CHECK_SLEEP_AMOUNT"])
    BUILD_SCRIPT_RESPONSE_KEY = os.environ["BUILD_SCRIPT_RESPONSE_KEY"]

    # DATABASE
    DB_NAME = os.environ["DB_NAME"]
    DB_USER = os.environ["DB_USER"]
    DB_PASS = os.environ["DB_PASS"]
    DB_SERVICE = os.environ["DB_SERVICE"]
    DB_PORT = os.environ["DB_PORT"]
    SQLALCHEMY_DATABASE_URI = "postgresql://{0}:{1}@{2}:{3}/{4}".format(
        DB_USER, DB_PASS, DB_SERVICE, DB_PORT, DB_NAME
    )
    SQLALCHEMY_TRACK_MODIFICATIONS = False

    # LOGGING
    LOG_TO = os.environ["LOG_TO"].split(",")
    APP_LOGLEVEL = os.environ["APP_LOGLEVEL"]

    GRAYLOG_HOST = os.environ["GRAYLOG_HOST"]
    GRAYLOG_PORT = int(os.environ["GRAYLOG_PORT"])
    GRAYLOG_APPNAME = "ab-builder"
    GRAYLOG_APPENV = os.environ["DEPLOY_ENV"]

    slack = {
        "channel": os.environ["SLACK_CHANNEL"],  # channel to post to
        "msg_subject": os.environ["SLACK_MSG_SUBJECT"],  # message subject
        "api-token": os.environ["SLACK_API_TOKEN"],  # api token for auth
    }

    CLS_URL = os.environ["CLS_URL"]


class DefaultConfig(BaseConfig):
    pass
