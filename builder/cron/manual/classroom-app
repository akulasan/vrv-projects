#!/bin/sh
# ======================================================================
# NOTICES
#  - this script must use /bin/sh (the build script can use bash)
#  - keep it simple, this script must only perform source checkout and
#    call the build script (./build-ab), the build script should take
#    care of everything else.
# ======================================================================

: ${PRJ_NAME:='classroom-app'}                                          # name of prj (this will be the name of the dir in the SOURCE_DIR)
: ${SRC_NAME:='classroom'}                                              # name of 'source' dir within app (must match the location of source for image's Dockerfile)
: ${SRC_REPO:='git@bitbucket.org:nortondev/classroom.git'}              # repo must have the 'auto-deploy' ssh key added (read only access)
: ${REPO_BRANCH:='master'}                                              # leave empty to use master.
# ---- below here should not need change ---
: ${SOURCE_DIR:='/opt/sources'}                                         # location of sources.
: ${RUN_BUILD:='true'}                                                  # kick off build or not.
: ${CLEAN_SOURCE:='false'}                                              # perform clean of source dir before run.
: ${TAG_IMAGE_LATEST:='true'}                                           # add the 'latest' tag to the image.
: ${VC_TAG:='isi-swfb2085'}                                             # vc tag.
: ${SLACK_WEBHOOK_URL:='https://hooks.slack.com/services/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'}   # slack webhook url.
: ${IMAGE_REPO:='pcr.wwnorton.com:5000'}                                # image repo url.
: ${PUSH_IMAGE_ENABLED:='true'}                                         # push image to image registry
: ${VC_TAG_ENABLED:='true'}                                             # tag vc (false will skip vc tagging)

ERR_Missing_SOURCE_DIR="ERR-01: Sources directory not available, cannot continue."
ERR_Git_Checkout_Failed="ERR-02: Git Checkout failed, cannot continue."

# startup checks.
if [ ! -f "${SOURCE_DIR}/.id" ]; then
  error_msg="${ERR_Missing_SOURCE_DIR}"; echo ${error_msg}; exit 1
fi
if [ "${CLEAN_SOURCE}" == "true" ]; then
  rm -rf "${SOURCE_DIR}/${PRJ_NAME}"
fi

# check if source dir exists.
if [ -d "${SOURCE_DIR}/${PRJ_NAME}/${SRC_NAME}/.git" ]; then
    echo "* source dir already exists..."
else
    echo "* source dir does not exist, checking out..."
    mkdir -p "${SOURCE_DIR}/${PRJ_NAME}"
    cd "${SOURCE_DIR}/${PRJ_NAME}"
    echo "* checking out from branch:  ${REPO_BRANCH}..."
    git clone -b ${REPO_BRANCH} ${VC_CLONE_OPTS} ${SRC_REPO} ${SRC_NAME}
    # check result.
    if [ "$?" -ne 0 ]; then
      error_msg="${ERR_Git_Checkout_Failed}"; echo ${error_msg}
    fi
fi

# initiate build
echo "* initiating build..."
cd "${SOURCE_DIR}/${PRJ_NAME}/${SRC_NAME}/"
if [ "${RUN_BUILD}" == "true" ]; then
  result="✓ success"
  if [ "${error_msg}" != "" ]; then
    result="✗ failure: ${error_msg}"
    echo "${result}"
    exit 1
  else
    SOURCE_DIR="${SRC_DIR}" \
      REPO_BRANCH="${REPO_BRANCH}" \
      APP_NAME="${PRJ_NAME}" \
      VC_TAG_ENABLED="${VC_TAG_ENABLED}" \
      TAG_IMAGE_LATEST="${TAG_IMAGE_LATEST}" \
      VC_TAG="${VC_TAG}" \
      SLACK_WEBHOOK_URL="${SLACK_WEBHOOK_URL}" \
      IMAGE_REPO="${IMAGE_REPO}" \
      PUSH_IMAGE_ENABLED="${PUSH_IMAGE_ENABLED}" \
      ./build-ab
  fi
else
  echo "* RUN_BUILD is not enabled, stopped at: $(pwd)"
fi
